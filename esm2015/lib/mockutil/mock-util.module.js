/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AppCoreInterceptor } from './app.core.interceptor';
import { AppInterceptorMock } from './app.interceptor';
import { MockDataService } from './mock-data.service';
export class MockUtilModule {
    constructor() {
        console.log('enter mockkkk module');
    }
}
MockUtilModule.decorators = [
    { type: NgModule, args: [{
                imports: [],
                declarations: [],
                providers: [
                    MockDataService,
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: AppCoreInterceptor,
                        multi: true
                    },
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: AppInterceptorMock,
                        multi: true
                    }
                ]
            },] },
];
/** @nocollapse */
MockUtilModule.ctorParameters = () => [];

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9jay11dGlsLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215Y29tcHMvIiwic291cmNlcyI6WyJsaWIvbW9ja3V0aWwvbW9jay11dGlsLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDekQsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV6QyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFtQnRELE1BQU07SUFDSjtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQTtLQUNwQzs7O1lBcEJGLFFBQVEsU0FBQztnQkFDUixPQUFPLEVBQUUsRUFBRTtnQkFDWCxZQUFZLEVBQUUsRUFBRTtnQkFDaEIsU0FBUyxFQUFFO29CQUNULGVBQWU7b0JBQ2Y7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsUUFBUSxFQUFFLGtCQUFrQjt3QkFDNUIsS0FBSyxFQUFFLElBQUk7cUJBQ1o7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsUUFBUSxFQUFFLGtCQUFrQjt3QkFDNUIsS0FBSyxFQUFFLElBQUk7cUJBQ1o7aUJBQ0Y7YUFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEhUVFBfSU5URVJDRVBUT1JTIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgQXBwQ29yZUludGVyY2VwdG9yIH0gZnJvbSAnLi9hcHAuY29yZS5pbnRlcmNlcHRvcic7XHJcbmltcG9ydCB7IEFwcEludGVyY2VwdG9yTW9jayB9IGZyb20gJy4vYXBwLmludGVyY2VwdG9yJztcclxuaW1wb3J0IHsgTW9ja0RhdGFTZXJ2aWNlIH0gZnJvbSAnLi9tb2NrLWRhdGEuc2VydmljZSc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6IFtdLFxyXG4gIGRlY2xhcmF0aW9uczogW10sXHJcbiAgcHJvdmlkZXJzOiBbXHJcbiAgICBNb2NrRGF0YVNlcnZpY2UsXHJcbiAgICB7XHJcbiAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxyXG4gICAgICB1c2VDbGFzczogQXBwQ29yZUludGVyY2VwdG9yLFxyXG4gICAgICBtdWx0aTogdHJ1ZVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXHJcbiAgICAgIHVzZUNsYXNzOiBBcHBJbnRlcmNlcHRvck1vY2ssXHJcbiAgICAgIG11bHRpOiB0cnVlXHJcbiAgICB9XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTW9ja1V0aWxNb2R1bGUge1xyXG4gIGNvbnN0cnVjdG9yKCl7XHJcbiAgICBjb25zb2xlLmxvZygnZW50ZXIgbW9ja2trayBtb2R1bGUnKVxyXG4gIH1cclxufVxyXG4iXX0=