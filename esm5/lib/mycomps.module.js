/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { NgModule } from '@angular/core';
import { MycompsComponent } from './mycomps.component';
import { MytestcompComponent } from './mytestcomp/mytestcomp.component';
import { MockUtilModule } from './mockutil/mock-util.module';
var MycompsModule = /** @class */ (function () {
    function MycompsModule() {
    }
    MycompsModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        MockUtilModule
                    ],
                    declarations: [MycompsComponent, MytestcompComponent],
                    exports: [MycompsComponent, MytestcompComponent]
                },] },
    ];
    return MycompsModule;
}());
export { MycompsModule };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXljb21wcy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teWNvbXBzLyIsInNvdXJjZXMiOlsibGliL215Y29tcHMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQzs7Ozs7Z0JBRTVELFFBQVEsU0FBQztvQkFDUixPQUFPLEVBQUU7d0JBQ1AsY0FBYztxQkFDZjtvQkFDRCxZQUFZLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxtQkFBbUIsQ0FBQztvQkFDckQsT0FBTyxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsbUJBQW1CLENBQUM7aUJBQ2pEOzt3QkFYRDs7U0FZYSxhQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE15Y29tcHNDb21wb25lbnQgfSBmcm9tICcuL215Y29tcHMuY29tcG9uZW50JztcbmltcG9ydCB7IE15dGVzdGNvbXBDb21wb25lbnQgfSBmcm9tICcuL215dGVzdGNvbXAvbXl0ZXN0Y29tcC5jb21wb25lbnQnO1xuaW1wb3J0IHsgTW9ja1V0aWxNb2R1bGUgfSBmcm9tICcuL21vY2t1dGlsL21vY2stdXRpbC5tb2R1bGUnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgTW9ja1V0aWxNb2R1bGVcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbTXljb21wc0NvbXBvbmVudCwgTXl0ZXN0Y29tcENvbXBvbmVudF0sXG4gIGV4cG9ydHM6IFtNeWNvbXBzQ29tcG9uZW50LCBNeXRlc3Rjb21wQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBNeWNvbXBzTW9kdWxlIHsgfVxuIl19