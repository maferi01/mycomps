/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @record
 */
export function IMock() { }
function IMock_tsickle_Closure_declarations() {
    /** @type {?} */
    IMock.prototype.mocks;
}
/**
 * @record
 */
export function IMockData() { }
function IMockData_tsickle_Closure_declarations() {
    /** @type {?|undefined} */
    IMockData.prototype.name;
    /** @type {?|undefined} */
    IMockData.prototype.type;
    /** @type {?|undefined} */
    IMockData.prototype.url;
    /** @type {?|undefined} */
    IMockData.prototype.body;
    /** @type {?|undefined} */
    IMockData.prototype.params;
    /** @type {?|undefined} */
    IMockData.prototype.headersRequest;
    /** @type {?|undefined} */
    IMockData.prototype.response;
    /** @type {?|undefined} */
    IMockData.prototype.responseFile;
    /** @type {?|undefined} */
    IMockData.prototype.resposeStatus;
    /** @type {?|undefined} */
    IMockData.prototype.headersResponse;
    /** @type {?|undefined} */
    IMockData.prototype.statusText;
    /** @type {?|undefined} */
    IMockData.prototype.error;
}
/**
 * @record
 */
export function IParam() { }
function IParam_tsickle_Closure_declarations() {
    /** @type {?} */
    IParam.prototype.key;
    /** @type {?} */
    IParam.prototype.value;
}
/**
 * @record
 */
export function IHeader() { }
function IHeader_tsickle_Closure_declarations() {
    /** @type {?} */
    IHeader.prototype.key;
    /** @type {?} */
    IHeader.prototype.value;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1vY2stZGF0YS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215Y29tcHMvIiwic291cmNlcyI6WyJsaWIvbW9ja3V0aWwvaW1vY2stZGF0YS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBJTW9jayB7XHJcbiAgbW9ja3M6IElNb2NrRGF0YVtdO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIElNb2NrRGF0YSB7XHJcbiAgbmFtZT86IHN0cmluZztcclxuICAvLyByZXF1ZXN0IGRhdGFcclxuICB0eXBlPzogc3RyaW5nO1xyXG4gIHVybD86IHN0cmluZztcclxuICBib2R5PzogYW55O1xyXG4gIHBhcmFtcz86IElQYXJhbVtdO1xyXG4gIGhlYWRlcnNSZXF1ZXN0PzogSUhlYWRlcltdO1xyXG4gIC8vIHJlc3BvbnNlIGRhdGFcclxuICByZXNwb25zZT86IGFueTtcclxuICByZXNwb25zZUZpbGU/OiBzdHJpbmc7XHJcbiAgcmVzcG9zZVN0YXR1cz86IG51bWJlcjtcclxuICBoZWFkZXJzUmVzcG9uc2U/OiBJSGVhZGVyW107XHJcbiAgc3RhdHVzVGV4dD86IHN0cmluZztcclxuICBlcnJvcj86IGFueTtcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBJUGFyYW0ge1xyXG4gIGtleTogc3RyaW5nO1xyXG4gIHZhbHVlOiBzdHJpbmc7XHJcbn1cclxuZXhwb3J0IGludGVyZmFjZSBJSGVhZGVyIHtcclxuICBrZXk6IHN0cmluZztcclxuICB2YWx1ZTogc3RyaW5nO1xyXG59XHJcbiJdfQ==