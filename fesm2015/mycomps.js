import { HttpErrorResponse, HttpHeaders, HttpResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Inject, Injectable, InjectionToken, Component, NgModule, defineInjectable } from '@angular/core';
import { of, throwError } from 'rxjs';
import { catchError, tap, delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
class BaseUtil {
    constructor() {
        this.log = {
            /**
             * @param {?} msg
             * @return {?}
             */
            info(msg) {
                console.log(msg);
            }
        };
        this.log.info('Class ' + this.constructor.name + ' created');
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const /** @type {?} */ TOKEN_MOCK = new InjectionToken('mockdata');
class MockDataService extends BaseUtil {
    /**
     * @param {?} mockData
     */
    constructor(mockData) {
        super();
        this.mockData = mockData;
    }
    /**
     * @param {?} req
     * @return {?}
     */
    getRequestMock(req) {
        return this.mockData.mocks.find((data) => {
            // check if the method and url and params and match params
            if (req.method === data.type &&
                req.url.includes(data.url) &&
                this.matchHeaders(data, req) &&
                this.matchParams(data, req) &&
                this.matchBody(data, req)) {
                if (data.name) {
                    this.log.info('---mock data matched ' + data.name);
                }
                return true;
            }
            return false;
        });
    }
    /**
     * @param {?} req
     * @param {?} mockData
     * @return {?}
     */
    getRequestMockData(req, mockData) {
        if (!mockData) {
            return null;
        }
        return mockData.mocks.find((data) => {
            // check if the method and url and params and match params
            if (this.matchMethod(data, req) &&
                this.matchUrl(data, req) &&
                this.matchHeaders(data, req) &&
                this.matchParams(data, req) &&
                this.matchBody(data, req)) {
                if (data.name) {
                    this.log.info('---mock data matched ' + data.name);
                }
                return true;
            }
            return false;
        });
    }
    /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    matchMethod(data, req) {
        if (data.type) {
            return req.method === data.type;
        }
        return true;
    }
    /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    matchUrl(data, req) {
        if (data.url) {
            return req.url.includes(data.url);
        }
        return true;
    }
    /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    matchParams(data, req) {
        if (data.params) {
            return data.params.every((param) => req.params[param.key] === param.value || (req.params.get && req.params.get(param.key) === param.value));
        }
        return true;
    }
    /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    matchHeaders(data, req) {
        if (data.headersRequest) {
            return data.headersRequest.every((header) => req.headers.get(header.key) === header.value);
        }
        return true;
    }
    /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    matchBody(data, req) {
        if (data.body) {
            const /** @type {?} */ res = objectEquals(data.body, req.body);
            this.log.info(` ${data.name} -compare res=${res} body req ${JSON.stringify(req.body)}`);
            // return JSON.stringify(data.body)==JSON.stringify(req.body);
            return res;
        }
        return true;
    }
    /**
     * @param {?} data
     * @return {?}
     */
    getResponseMock(data) {
        const /** @type {?} */ resp = this.getResponseMockObj(data);
        if (resp instanceof HttpErrorResponse) {
            return throwError(resp);
        }
        else {
            return of(resp);
        }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    getResponseMockObj(data) {
        if (!data.resposeStatus) {
            data.resposeStatus = 200;
        }
        let /** @type {?} */ headers = new HttpHeaders();
        if (data.headersResponse) {
            data.headersResponse.forEach(header => (headers = headers.append(header.key, header.value)));
        }
        if (data.resposeStatus !== 200) {
            return new HttpErrorResponse({
                status: data.resposeStatus,
                headers: headers,
                error: data.error,
                statusText: data.statusText
            });
        }
        return (new HttpResponse({
            status: data.resposeStatus,
            body: data.response ? data.response : null,
            headers: headers
        }));
    }
}
MockDataService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
MockDataService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [TOKEN_MOCK,] }] }
];
/**
 * @param {?} x
 * @param {?} y
 * @return {?}
 */
function objectEquals(x, y) {
    // if both are function
    if (x instanceof Function) {
        if (y instanceof Function) {
            return x.toString() === y.toString();
        }
        return false;
    }
    if (x === null || x === undefined || y === null || y === undefined) {
        return x === y;
    }
    if (x === y || x.valueOf() === y.valueOf()) {
        return true;
    }
    // if one of them is date, they must had equal valueOf
    if (x instanceof Date) {
        return false;
    }
    if (y instanceof Date) {
        return false;
    }
    // if they are not function or strictly equal, they both need to be Objects
    if (!(x instanceof Object)) {
        return false;
    }
    if (!(y instanceof Object)) {
        return false;
    }
    const /** @type {?} */ p = Object.keys(x);
    return p.every(function (i) {
        return objectEquals(x[i], y[i]);
    });
    /* return Object.keys(y).every(function (i) { return p.indexOf(i) !== -1; }) ?
              p.every(function (i) { return objectEquals(x[i], y[i]); }) : false; */
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MycompsService {
    constructor() { }
}
MycompsService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
MycompsService.ctorParameters = () => [];
/** @nocollapse */ MycompsService.ngInjectableDef = defineInjectable({ factory: function MycompsService_Factory() { return new MycompsService(); }, token: MycompsService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MycompsComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
MycompsComponent.decorators = [
    { type: Component, args: [{
                selector: 'my-mycomps',
                template: `
    <p>
      mycomps works artur!
    </p>
  `,
                styles: [`p{background:red}`]
            },] },
];
/** @nocollapse */
MycompsComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MytestcompComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
MytestcompComponent.decorators = [
    { type: Component, args: [{
                selector: 'my-mytestcomp',
                template: `
<div>
   <p>
  mytestcomp works***** from library!
</p>
    
</div>


`,
                styles: [`div p{background:pink}`]
            },] },
];
/** @nocollapse */
MytestcompComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class AppCoreInterceptor extends BaseUtil {
    constructor() {
        super();
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        const /** @type {?} */ changedReq = req.clone({
            headers: req.headers.set('My-Header', 'MyHeaderValue')
        });
        this.log.info('pass intercept coreee');
        return next.handle(changedReq)
            .pipe(tap((event) => {
            if (event instanceof HttpResponse) {
                const /** @type {?} */ r = /** @type {?} */ (event);
                this.log.info('url resp**===' + r.url);
                this.log.info('status resp**===' + r.status);
                this.log.info('headers resp**===' + JSON.stringify(r.headers));
            }
        }, (error) => {
            this.log.info('headers error resp**==' + JSON.stringify(error.headers));
            this.log.info('error resp**==' + JSON.stringify(error.error));
            this.log.info('error core status***' + error.status);
        }), catchError((err) => {
            this.log.info('error cath core***' + err);
            return throwError(err);
        }));
        /*   catch((err)=>{
                    console.log('error cath core***'+err);
                    return Observable.throw(err);
                  }); */
    }
}
AppCoreInterceptor.decorators = [
    { type: Injectable },
];
/** @nocollapse */
AppCoreInterceptor.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class AppInterceptorMock extends BaseUtil {
    /**
     * @param {?} mockSrv
     */
    constructor(mockSrv) {
        super();
        this.mockSrv = mockSrv;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        const /** @type {?} */ mock = this.mockSrv.getRequestMock(req);
        this.log.info('enter app interceptor MOCK***********');
        return of(null).pipe(mergeMap(() => {
            if (mock) {
                return this.mockSrv.getResponseMock(mock);
            }
            const /** @type {?} */ changedReq = req.clone({
                headers: req.headers.set('My-Header', 'MyHeaderValue')
            });
            return next.handle(changedReq);
        }), materialize(), delay(500), dematerialize());
    }
}
AppInterceptorMock.decorators = [
    { type: Injectable },
];
/** @nocollapse */
AppInterceptorMock.ctorParameters = () => [
    { type: MockDataService }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MockUtilModule {
    constructor() {
        console.log('enter mockkkk module');
    }
}
MockUtilModule.decorators = [
    { type: NgModule, args: [{
                imports: [],
                declarations: [],
                providers: [
                    MockDataService,
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: AppCoreInterceptor,
                        multi: true
                    },
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: AppInterceptorMock,
                        multi: true
                    }
                ]
            },] },
];
/** @nocollapse */
MockUtilModule.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MycompsModule {
}
MycompsModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    MockUtilModule
                ],
                declarations: [MycompsComponent, MytestcompComponent],
                exports: [MycompsComponent, MytestcompComponent]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { TOKEN_MOCK, MockDataService, MycompsService, MycompsComponent, MycompsModule, AppCoreInterceptor as ɵc, AppInterceptorMock as ɵd, BaseUtil as ɵa, MockUtilModule as ɵb, MytestcompComponent as ɵe };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXljb21wcy5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vbXljb21wcy9saWIvbW9ja3V0aWwvYmFzZS11dGlsLnRzIiwibmc6Ly9teWNvbXBzL2xpYi9tb2NrdXRpbC9tb2NrLWRhdGEuc2VydmljZS50cyIsIm5nOi8vbXljb21wcy9saWIvbXljb21wcy5zZXJ2aWNlLnRzIiwibmc6Ly9teWNvbXBzL2xpYi9teWNvbXBzLmNvbXBvbmVudC50cyIsIm5nOi8vbXljb21wcy9saWIvbXl0ZXN0Y29tcC9teXRlc3Rjb21wLmNvbXBvbmVudC50cyIsIm5nOi8vbXljb21wcy9saWIvbW9ja3V0aWwvYXBwLmNvcmUuaW50ZXJjZXB0b3IudHMiLCJuZzovL215Y29tcHMvbGliL21vY2t1dGlsL2FwcC5pbnRlcmNlcHRvci50cyIsIm5nOi8vbXljb21wcy9saWIvbW9ja3V0aWwvbW9jay11dGlsLm1vZHVsZS50cyIsIm5nOi8vbXljb21wcy9saWIvbXljb21wcy5tb2R1bGUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBCYXNlVXRpbCB7XHJcbiAgcHJvdGVjdGVkIGxvZztcclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHRoaXMubG9nID0ge1xyXG4gICAgICBpbmZvKG1zZykge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKG1zZyk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcbiAgICB0aGlzLmxvZy5pbmZvKCdDbGFzcyAnICsgdGhpcy5jb25zdHJ1Y3Rvci5uYW1lICsgJyBjcmVhdGVkJyk7XHJcbiAgfVxyXG59XHJcbiIsImltcG9ydCB7IEh0dHBFcnJvclJlc3BvbnNlLCBIdHRwSGVhZGVycywgSHR0cFJlcXVlc3QsIEh0dHBSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlLCBJbmplY3Rpb25Ub2tlbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBvZiwgdGhyb3dFcnJvciwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQmFzZVV0aWwgfSBmcm9tICcuL2Jhc2UtdXRpbCc7XHJcbmltcG9ydCB7IElIZWFkZXIsIElNb2NrLCBJTW9ja0RhdGEsIElQYXJhbSB9IGZyb20gJy4vaW1vY2stZGF0YS5tb2RlbCc7XHJcblxyXG5leHBvcnQgY29uc3QgVE9LRU5fTU9DSyA9IG5ldyBJbmplY3Rpb25Ub2tlbignbW9ja2RhdGEnKTtcclxuLy8gaW1wb3J0ICdyeGpzL2FkZC9vYnNlcnZhYmxlL3Rocm93JztcclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTW9ja0RhdGFTZXJ2aWNlIGV4dGVuZHMgQmFzZVV0aWwge1xyXG5cclxuICBjb25zdHJ1Y3RvcihASW5qZWN0KFRPS0VOX01PQ0spIHByaXZhdGUgbW9ja0RhdGE6IElNb2NrKSB7XHJcbiAgICBzdXBlcigpO1xyXG4gIH1cclxuXHJcblxyXG4gIGdldFJlcXVlc3RNb2NrKHJlcTogSHR0cFJlcXVlc3Q8YW55Pik6IElNb2NrRGF0YSB7XHJcbiAgICByZXR1cm4gdGhpcy5tb2NrRGF0YS5tb2Nrcy5maW5kKChkYXRhOiBJTW9ja0RhdGEpID0+IHtcclxuICAgICAgLy8gY2hlY2sgaWYgdGhlIG1ldGhvZCBhbmQgdXJsIGFuZCBwYXJhbXMgYW5kIG1hdGNoIHBhcmFtc1xyXG4gICAgICBpZiAoXHJcbiAgICAgICAgcmVxLm1ldGhvZCA9PT0gZGF0YS50eXBlICYmXHJcbiAgICAgICAgcmVxLnVybC5pbmNsdWRlcyhkYXRhLnVybCkgJiZcclxuICAgICAgICB0aGlzLm1hdGNoSGVhZGVycyhkYXRhLCByZXEpICYmXHJcbiAgICAgICAgdGhpcy5tYXRjaFBhcmFtcyhkYXRhLCByZXEpICYmXHJcbiAgICAgICAgdGhpcy5tYXRjaEJvZHkoZGF0YSwgcmVxKVxyXG4gICAgICApIHtcclxuICAgICAgICBpZiAoZGF0YS5uYW1lKSB7XHJcbiAgICAgICAgICB0aGlzLmxvZy5pbmZvKCctLS1tb2NrIGRhdGEgbWF0Y2hlZCAnICsgZGF0YS5uYW1lKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBnZXRSZXF1ZXN0TW9ja0RhdGEocmVxOiBIdHRwUmVxdWVzdDxhbnk+LCBtb2NrRGF0YTogSU1vY2spOiBJTW9ja0RhdGEge1xyXG4gICAgaWYgKCFtb2NrRGF0YSkge1xyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gbW9ja0RhdGEubW9ja3MuZmluZCgoZGF0YTogSU1vY2tEYXRhKSA9PiB7XHJcbiAgICAgIC8vIGNoZWNrIGlmIHRoZSBtZXRob2QgYW5kIHVybCBhbmQgcGFyYW1zIGFuZCBtYXRjaCBwYXJhbXNcclxuICAgICAgaWYgKFxyXG4gICAgICAgIHRoaXMubWF0Y2hNZXRob2QoZGF0YSwgcmVxKSAmJlxyXG4gICAgICAgIHRoaXMubWF0Y2hVcmwoZGF0YSwgcmVxKSAmJlxyXG4gICAgICAgIHRoaXMubWF0Y2hIZWFkZXJzKGRhdGEsIHJlcSkgJiZcclxuICAgICAgICB0aGlzLm1hdGNoUGFyYW1zKGRhdGEsIHJlcSkgJiZcclxuICAgICAgICB0aGlzLm1hdGNoQm9keShkYXRhLCByZXEpXHJcbiAgICAgICkge1xyXG4gICAgICAgIGlmIChkYXRhLm5hbWUpIHtcclxuICAgICAgICAgIHRoaXMubG9nLmluZm8oJy0tLW1vY2sgZGF0YSBtYXRjaGVkICcgKyBkYXRhLm5hbWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgbWF0Y2hNZXRob2QoZGF0YTogSU1vY2tEYXRhLCByZXE6IEh0dHBSZXF1ZXN0PGFueT4pOiBib29sZWFuIHtcclxuICAgIGlmIChkYXRhLnR5cGUpIHtcclxuICAgICAgcmV0dXJuIHJlcS5tZXRob2QgPT09IGRhdGEudHlwZTtcclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBtYXRjaFVybChkYXRhOiBJTW9ja0RhdGEsIHJlcTogSHR0cFJlcXVlc3Q8YW55Pik6IGJvb2xlYW4ge1xyXG4gICAgaWYgKGRhdGEudXJsKSB7XHJcbiAgICAgIHJldHVybiByZXEudXJsLmluY2x1ZGVzKGRhdGEudXJsKTtcclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBtYXRjaFBhcmFtcyhkYXRhOiBJTW9ja0RhdGEsIHJlcTogSHR0cFJlcXVlc3Q8YW55Pik6IGJvb2xlYW4ge1xyXG4gICAgaWYgKGRhdGEucGFyYW1zKSB7XHJcbiAgICAgIHJldHVybiBkYXRhLnBhcmFtcy5ldmVyeShcclxuICAgICAgICAocGFyYW06IElQYXJhbSkgPT4gcmVxLnBhcmFtc1twYXJhbS5rZXldID09PSBwYXJhbS52YWx1ZSB8fCAocmVxLnBhcmFtcy5nZXQgJiYgcmVxLnBhcmFtcy5nZXQocGFyYW0ua2V5KSA9PT0gcGFyYW0udmFsdWUpXHJcbiAgICAgICk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcbiAgcHJpdmF0ZSBtYXRjaEhlYWRlcnMoZGF0YTogSU1vY2tEYXRhLCByZXE6IEh0dHBSZXF1ZXN0PGFueT4pOiBib29sZWFuIHtcclxuICAgIGlmIChkYXRhLmhlYWRlcnNSZXF1ZXN0KSB7XHJcbiAgICAgIHJldHVybiBkYXRhLmhlYWRlcnNSZXF1ZXN0LmV2ZXJ5KChoZWFkZXI6IElIZWFkZXIpID0+IHJlcS5oZWFkZXJzLmdldChoZWFkZXIua2V5KSA9PT0gaGVhZGVyLnZhbHVlKTtcclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBtYXRjaEJvZHkoZGF0YTogSU1vY2tEYXRhLCByZXE6IEh0dHBSZXF1ZXN0PGFueT4pOiBib29sZWFuIHtcclxuICAgIGlmIChkYXRhLmJvZHkpIHtcclxuICAgICAgY29uc3QgcmVzID0gb2JqZWN0RXF1YWxzKGRhdGEuYm9keSwgcmVxLmJvZHkpO1xyXG4gICAgICB0aGlzLmxvZy5pbmZvKGAgJHtkYXRhLm5hbWV9IC1jb21wYXJlIHJlcz0ke3Jlc30gYm9keSByZXEgJHtKU09OLnN0cmluZ2lmeShyZXEuYm9keSl9YCk7XHJcbiAgICAgIC8vIHJldHVybiBKU09OLnN0cmluZ2lmeShkYXRhLmJvZHkpPT1KU09OLnN0cmluZ2lmeShyZXEuYm9keSk7XHJcbiAgICAgIHJldHVybiByZXM7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIGdldFJlc3BvbnNlTW9jayhkYXRhOiBJTW9ja0RhdGEpOk9ic2VydmFibGU8YW55PiB7XHJcbiAgICBjb25zdCByZXNwID0gdGhpcy5nZXRSZXNwb25zZU1vY2tPYmooZGF0YSk7XHJcbiAgICBpZiAocmVzcCBpbnN0YW5jZW9mIEh0dHBFcnJvclJlc3BvbnNlKSB7XHJcbiAgICAgIHJldHVybiB0aHJvd0Vycm9yKHJlc3ApO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIG9mKHJlc3ApO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0UmVzcG9uc2VNb2NrT2JqKGRhdGE6IElNb2NrRGF0YSk6IEh0dHBFcnJvclJlc3BvbnNlIHwgSHR0cFJlc3BvbnNlPG9iamVjdD4ge1xyXG4gICAgaWYgKCFkYXRhLnJlc3Bvc2VTdGF0dXMpIHtcclxuICAgICAgZGF0YS5yZXNwb3NlU3RhdHVzID0gMjAwO1xyXG4gICAgfVxyXG4gICAgbGV0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoKTtcclxuICAgIGlmIChkYXRhLmhlYWRlcnNSZXNwb25zZSkge1xyXG4gICAgICBkYXRhLmhlYWRlcnNSZXNwb25zZS5mb3JFYWNoKGhlYWRlciA9PiAoaGVhZGVycyA9IGhlYWRlcnMuYXBwZW5kKGhlYWRlci5rZXksIGhlYWRlci52YWx1ZSkpKTtcclxuICAgIH1cclxuICAgIGlmIChkYXRhLnJlc3Bvc2VTdGF0dXMgIT09IDIwMCkge1xyXG4gICAgICByZXR1cm4gbmV3IEh0dHBFcnJvclJlc3BvbnNlKHtcclxuICAgICAgICBzdGF0dXM6IGRhdGEucmVzcG9zZVN0YXR1cyxcclxuICAgICAgICBoZWFkZXJzOiBoZWFkZXJzLFxyXG4gICAgICAgIGVycm9yOiBkYXRhLmVycm9yLFxyXG4gICAgICAgIHN0YXR1c1RleHQ6IGRhdGEuc3RhdHVzVGV4dFxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiAoXHJcbiAgICAgIG5ldyBIdHRwUmVzcG9uc2Uoe1xyXG4gICAgICAgIHN0YXR1czogZGF0YS5yZXNwb3NlU3RhdHVzLFxyXG4gICAgICAgIGJvZHk6IGRhdGEucmVzcG9uc2UgPyBkYXRhLnJlc3BvbnNlIDogbnVsbCxcclxuICAgICAgICBoZWFkZXJzOiBoZWFkZXJzXHJcbiAgICAgIH0pXHJcbiAgICApO1xyXG4gIH1cclxufVxyXG5cclxuZnVuY3Rpb24gb2JqZWN0RXF1YWxzKHgsIHkpIHtcclxuICAvLyBpZiBib3RoIGFyZSBmdW5jdGlvblxyXG4gIGlmICh4IGluc3RhbmNlb2YgRnVuY3Rpb24pIHtcclxuICAgIGlmICh5IGluc3RhbmNlb2YgRnVuY3Rpb24pIHtcclxuICAgICAgcmV0dXJuIHgudG9TdHJpbmcoKSA9PT0geS50b1N0cmluZygpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuICBpZiAoeCA9PT0gbnVsbCB8fCB4ID09PSB1bmRlZmluZWQgfHwgeSA9PT0gbnVsbCB8fCB5ID09PSB1bmRlZmluZWQpIHtcclxuICAgIHJldHVybiB4ID09PSB5O1xyXG4gIH1cclxuICBpZiAoeCA9PT0geSB8fCB4LnZhbHVlT2YoKSA9PT0geS52YWx1ZU9mKCkpIHtcclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgLy8gaWYgb25lIG9mIHRoZW0gaXMgZGF0ZSwgdGhleSBtdXN0IGhhZCBlcXVhbCB2YWx1ZU9mXHJcbiAgaWYgKHggaW5zdGFuY2VvZiBEYXRlKSB7XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG4gIGlmICh5IGluc3RhbmNlb2YgRGF0ZSkge1xyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgLy8gaWYgdGhleSBhcmUgbm90IGZ1bmN0aW9uIG9yIHN0cmljdGx5IGVxdWFsLCB0aGV5IGJvdGggbmVlZCB0byBiZSBPYmplY3RzXHJcbiAgaWYgKCEoeCBpbnN0YW5jZW9mIE9iamVjdCkpIHtcclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcbiAgaWYgKCEoeSBpbnN0YW5jZW9mIE9iamVjdCkpIHtcclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcblxyXG4gIGNvbnN0IHAgPSBPYmplY3Qua2V5cyh4KTtcclxuICByZXR1cm4gcC5ldmVyeShmdW5jdGlvbihpKSB7XHJcbiAgICByZXR1cm4gb2JqZWN0RXF1YWxzKHhbaV0sIHlbaV0pO1xyXG4gIH0pO1xyXG5cclxuICAvKiByZXR1cm4gT2JqZWN0LmtleXMoeSkuZXZlcnkoZnVuY3Rpb24gKGkpIHsgcmV0dXJuIHAuaW5kZXhPZihpKSAhPT0gLTE7IH0pID9cclxuICAgICAgICAgIHAuZXZlcnkoZnVuY3Rpb24gKGkpIHsgcmV0dXJuIG9iamVjdEVxdWFscyh4W2ldLCB5W2ldKTsgfSkgOiBmYWxzZTsgKi9cclxufVxyXG4iLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNeWNvbXBzU2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbXktbXljb21wcycsXG4gIHRlbXBsYXRlOiBgXG4gICAgPHA+XG4gICAgICBteWNvbXBzIHdvcmtzIGFydHVyIVxuICAgIDwvcD5cbiAgYFxuICAsICBzdHlsZXM6IFtgcHtiYWNrZ3JvdW5kOnJlZH1gXVxufSlcbmV4cG9ydCBjbGFzcyBNeWNvbXBzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ215LW15dGVzdGNvbXAnLFxuICB0ZW1wbGF0ZTogYFxuPGRpdj5cbiAgIDxwPlxuICBteXRlc3Rjb21wIHdvcmtzKioqKiogZnJvbSBsaWJyYXJ5IVxuPC9wPlxuICAgIFxuPC9kaXY+XG5cblxuYCxcbiAgc3R5bGVzOiBbYGRpdiBwe2JhY2tncm91bmQ6cGlua31gXVxufSlcbmV4cG9ydCBjbGFzcyBNeXRlc3Rjb21wQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbn1cbiIsImltcG9ydCB7IEh0dHBFcnJvclJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7XHJcbiAgSHR0cEV2ZW50LFxyXG4gIEh0dHBJbnRlcmNlcHRvcixcclxuICBIdHRwSGFuZGxlcixcclxuICBIdHRwUmVxdWVzdCxcclxuICBIdHRwUmVzcG9uc2VcclxufSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgLCAgb2YsIHRocm93RXJyb3J9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwLCB0YXAsIHJlZkNvdW50LCBwdWJsaXNoUmVwbGF5LCBkZWxheSwgcmV0cnkgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IE1vY2tEYXRhU2VydmljZSB9IGZyb20gJy4vbW9jay1kYXRhLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBCYXNlVXRpbCB9IGZyb20gJy4vYmFzZS11dGlsJztcclxuXHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBBcHBDb3JlSW50ZXJjZXB0b3IgZXh0ZW5kcyBCYXNlVXRpbCBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICBzdXBlcigpO1xyXG4gIH1cclxuXHJcbiAgaW50ZXJjZXB0KHJlcTogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XHJcbiAgICAgICAgIGNvbnN0IGNoYW5nZWRSZXEgPSByZXEuY2xvbmUoe1xyXG4gICAgICAgICAgICBoZWFkZXJzOiByZXEuaGVhZGVycy5zZXQoJ015LUhlYWRlcicsICdNeUhlYWRlclZhbHVlJylcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgdGhpcy5sb2cuaW5mbygncGFzcyBpbnRlcmNlcHQgY29yZWVlJyk7XHJcbiAgICAgICAgICByZXR1cm4gbmV4dC5oYW5kbGUoY2hhbmdlZFJlcSlcclxuICAgICAgICAgIC5waXBlKHRhcCgoZXZlbnQ6IEh0dHBFdmVudDxhbnk+KSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChldmVudCBpbnN0YW5jZW9mIEh0dHBSZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgIGNvbnN0IHIgPSBldmVudCBhcyBIdHRwUmVzcG9uc2U8YW55PjtcclxuICAgICAgICAgICAgICB0aGlzLmxvZy5pbmZvKCd1cmwgcmVzcCoqPT09JyArIHIudXJsKTtcclxuICAgICAgICAgICAgICB0aGlzLmxvZy5pbmZvKCdzdGF0dXMgcmVzcCoqPT09JyArIHIuc3RhdHVzKTtcclxuICAgICAgICAgICAgICB0aGlzLmxvZy5pbmZvKCdoZWFkZXJzIHJlc3AqKj09PScgKyBKU09OLnN0cmluZ2lmeShyLmhlYWRlcnMpKTtcclxuICAgICAgICAgICB9XHJcbiAgICAgICAgICB9LCAoZXJyb3I6IEh0dHBFcnJvclJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ2hlYWRlcnMgZXJyb3IgcmVzcCoqPT0nICsgSlNPTi5zdHJpbmdpZnkoZXJyb3IuaGVhZGVycykpO1xyXG4gICAgICAgICAgICB0aGlzLmxvZy5pbmZvKCdlcnJvciByZXNwKio9PScgKyBKU09OLnN0cmluZ2lmeShlcnJvci5lcnJvcikpO1xyXG4gICAgICAgICAgICB0aGlzLmxvZy5pbmZvKCdlcnJvciBjb3JlIHN0YXR1cyoqKicgKyBlcnJvci5zdGF0dXMpO1xyXG4gICAgICAgICAgfSlcclxuICAgICAgICAgICwgY2F0Y2hFcnJvcigoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ2Vycm9yIGNhdGggY29yZSoqKicgKyBlcnIpO1xyXG4gICAgICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihlcnIpO1xyXG4gICAgICAgICAgfSlcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICAvKiAgIGNhdGNoKChlcnIpPT57XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdlcnJvciBjYXRoIGNvcmUqKionK2Vycik7XHJcbiAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLnRocm93KGVycik7XHJcbiAgICAgICAgICB9KTsgKi9cclxuXHJcbiAgICAgICAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQgeyBIdHRwRXJyb3JSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge1xyXG4gIEh0dHBFdmVudCxcclxuICBIdHRwSW50ZXJjZXB0b3IsXHJcbiAgSHR0cEhhbmRsZXIsXHJcbiAgSHR0cFJlcXVlc3QsXHJcbiAgSHR0cFJlc3BvbnNlXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5cclxuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwLCB0YXAsIHJlZkNvdW50LCBwdWJsaXNoUmVwbGF5LCBkZWxheSwgcmV0cnksIG1lcmdlTWFwLCBtYXRlcmlhbGl6ZSwgZGVtYXRlcmlhbGl6ZSB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgTW9ja0RhdGFTZXJ2aWNlIH0gZnJvbSAnLi9tb2NrLWRhdGEuc2VydmljZSc7XHJcbmltcG9ydCB7IElNb2NrRGF0YSB9IGZyb20gJy4vaW1vY2stZGF0YS5tb2RlbCc7XHJcblxyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBCYXNlVXRpbCB9IGZyb20gJy4vYmFzZS11dGlsJztcclxuXHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBBcHBJbnRlcmNlcHRvck1vY2sgZXh0ZW5kcyBCYXNlVXRpbCBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBtb2NrU3J2OiBNb2NrRGF0YVNlcnZpY2UpIHtcclxuICAgIHN1cGVyKCk7XHJcbiAgfVxyXG5cclxuICBpbnRlcmNlcHQocmVxOiBIdHRwUmVxdWVzdDxhbnk+LCBuZXh0OiBIdHRwSGFuZGxlcik6IE9ic2VydmFibGU8SHR0cEV2ZW50PGFueT4+IHtcclxuICAgY29uc3QgbW9jazogSU1vY2tEYXRhID0gdGhpcy5tb2NrU3J2LmdldFJlcXVlc3RNb2NrKHJlcSk7XHJcblxyXG4gICB0aGlzLmxvZy5pbmZvKCdlbnRlciBhcHAgaW50ZXJjZXB0b3IgTU9DSyoqKioqKioqKioqJyk7XHJcblxyXG4gICByZXR1cm4gb2YobnVsbCkucGlwZShcclxuICAgICAgbWVyZ2VNYXAoKCkgPT4ge1xyXG4gICAgICAgIGlmIChtb2NrKSB7XHJcbiAgICAgICAgICByZXR1cm4gdGhpcy5tb2NrU3J2LmdldFJlc3BvbnNlTW9jayhtb2NrKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgY2hhbmdlZFJlcSA9IHJlcS5jbG9uZSh7XHJcbiAgICAgICAgICBoZWFkZXJzOiByZXEuaGVhZGVycy5zZXQoJ015LUhlYWRlcicsICdNeUhlYWRlclZhbHVlJylcclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gbmV4dC5oYW5kbGUoY2hhbmdlZFJlcSk7XHJcbiAgICAgIH0pXHJcbiAgICAgICwgbWF0ZXJpYWxpemUoKVxyXG4gICAgICAsIGRlbGF5KDUwMClcclxuICAgICAgLCBkZW1hdGVyaWFsaXplKClcclxuICAgICk7XHJcblxyXG5cclxuICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgSFRUUF9JTlRFUkNFUFRPUlMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBBcHBDb3JlSW50ZXJjZXB0b3IgfSBmcm9tICcuL2FwcC5jb3JlLmludGVyY2VwdG9yJztcclxuaW1wb3J0IHsgQXBwSW50ZXJjZXB0b3JNb2NrIH0gZnJvbSAnLi9hcHAuaW50ZXJjZXB0b3InO1xyXG5pbXBvcnQgeyBNb2NrRGF0YVNlcnZpY2UgfSBmcm9tICcuL21vY2stZGF0YS5zZXJ2aWNlJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgaW1wb3J0czogW10sXHJcbiAgZGVjbGFyYXRpb25zOiBbXSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIE1vY2tEYXRhU2VydmljZSxcclxuICAgIHtcclxuICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXHJcbiAgICAgIHVzZUNsYXNzOiBBcHBDb3JlSW50ZXJjZXB0b3IsXHJcbiAgICAgIG11bHRpOiB0cnVlXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBIVFRQX0lOVEVSQ0VQVE9SUyxcclxuICAgICAgdXNlQ2xhc3M6IEFwcEludGVyY2VwdG9yTW9jayxcclxuICAgICAgbXVsdGk6IHRydWVcclxuICAgIH1cclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNb2NrVXRpbE1vZHVsZSB7XHJcbiAgY29uc3RydWN0b3IoKXtcclxuICAgIGNvbnNvbGUubG9nKCdlbnRlciBtb2Nra2trIG1vZHVsZScpXHJcbiAgfVxyXG59XHJcbiIsImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNeWNvbXBzQ29tcG9uZW50IH0gZnJvbSAnLi9teWNvbXBzLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBNeXRlc3Rjb21wQ29tcG9uZW50IH0gZnJvbSAnLi9teXRlc3Rjb21wL215dGVzdGNvbXAuY29tcG9uZW50JztcbmltcG9ydCB7IE1vY2tVdGlsTW9kdWxlIH0gZnJvbSAnLi9tb2NrdXRpbC9tb2NrLXV0aWwubW9kdWxlJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIE1vY2tVdGlsTW9kdWxlXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW015Y29tcHNDb21wb25lbnQsIE15dGVzdGNvbXBDb21wb25lbnRdLFxuICBleHBvcnRzOiBbTXljb21wc0NvbXBvbmVudCwgTXl0ZXN0Y29tcENvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgTXljb21wc01vZHVsZSB7IH1cbiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFDQTtJQUVFO1FBQ0UsSUFBSSxDQUFDLEdBQUcsR0FBRzs7Ozs7WUFDVCxJQUFJLENBQUMsR0FBRztnQkFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ2xCO1NBQ0YsQ0FBQztRQUNGLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxVQUFVLENBQUMsQ0FBQztLQUM5RDtDQUNGOzs7Ozs7QUNYRCx1QkFPYSxVQUFVLEdBQUcsSUFBSSxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7QUFHekQscUJBQTZCLFNBQVEsUUFBUTs7OztJQUUzQyxZQUF3QyxRQUFlO1FBQ3JELEtBQUssRUFBRSxDQUFDO1FBRDhCLGFBQVEsR0FBUixRQUFRLENBQU87S0FFdEQ7Ozs7O0lBR0QsY0FBYyxDQUFDLEdBQXFCO1FBQ2xDLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBZTs7WUFFOUMsSUFDRSxHQUFHLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxJQUFJO2dCQUN4QixHQUFHLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO2dCQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxHQUFHLENBQUM7Z0JBQzVCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUMxQixFQUFFO2dCQUNBLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDYixJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ3BEO2dCQUNELE9BQU8sSUFBSSxDQUFDO2FBQ2I7WUFDRCxPQUFPLEtBQUssQ0FBQztTQUNkLENBQUMsQ0FBQztLQUNKOzs7Ozs7SUFFRCxrQkFBa0IsQ0FBQyxHQUFxQixFQUFFLFFBQWU7UUFDdkQsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNiLE9BQU8sSUFBSSxDQUFDO1NBQ2I7UUFFRCxPQUFPLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBZTs7WUFFekMsSUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQztnQkFDeEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDO2dCQUM1QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FDMUIsRUFBRTtnQkFDQSxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7b0JBQ2IsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNwRDtnQkFDRCxPQUFPLElBQUksQ0FBQzthQUNiO1lBQ0QsT0FBTyxLQUFLLENBQUM7U0FDZCxDQUFDLENBQUM7S0FDSjs7Ozs7O0lBRU8sV0FBVyxDQUFDLElBQWUsRUFBRSxHQUFxQjtRQUN4RCxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDYixPQUFPLEdBQUcsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQztTQUNqQztRQUNELE9BQU8sSUFBSSxDQUFDOzs7Ozs7O0lBR04sUUFBUSxDQUFDLElBQWUsRUFBRSxHQUFxQjtRQUNyRCxJQUFJLElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDWixPQUFPLEdBQUcsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNuQztRQUNELE9BQU8sSUFBSSxDQUFDOzs7Ozs7O0lBR04sV0FBVyxDQUFDLElBQWUsRUFBRSxHQUFxQjtRQUN4RCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDZixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUN0QixDQUFDLEtBQWEsS0FBSyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxLQUFLLENBQUMsS0FBSyxLQUFLLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQzFILENBQUM7U0FDSDtRQUNELE9BQU8sSUFBSSxDQUFDOzs7Ozs7O0lBRU4sWUFBWSxDQUFDLElBQWUsRUFBRSxHQUFxQjtRQUN6RCxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDdkIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQWUsS0FBSyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3JHO1FBQ0QsT0FBTyxJQUFJLENBQUM7Ozs7Ozs7SUFHTixTQUFTLENBQUMsSUFBZSxFQUFFLEdBQXFCO1FBQ3RELElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtZQUNiLHVCQUFNLEdBQUcsR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDOUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxpQkFBaUIsR0FBRyxhQUFhLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzs7WUFFeEYsT0FBTyxHQUFHLENBQUM7U0FDWjtRQUNELE9BQU8sSUFBSSxDQUFDOzs7Ozs7SUFHZCxlQUFlLENBQUMsSUFBZTtRQUM3Qix1QkFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNDLElBQUksSUFBSSxZQUFZLGlCQUFpQixFQUFFO1lBQ3JDLE9BQU8sVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3pCO2FBQU07WUFDTCxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNqQjtLQUNGOzs7OztJQUVELGtCQUFrQixDQUFDLElBQWU7UUFDaEMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGFBQWEsR0FBRyxHQUFHLENBQUM7U0FDMUI7UUFDRCxxQkFBSSxPQUFPLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQztRQUNoQyxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDeEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsTUFBTSxLQUFLLE9BQU8sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUM5RjtRQUNELElBQUksSUFBSSxDQUFDLGFBQWEsS0FBSyxHQUFHLEVBQUU7WUFDOUIsT0FBTyxJQUFJLGlCQUFpQixDQUFDO2dCQUMzQixNQUFNLEVBQUUsSUFBSSxDQUFDLGFBQWE7Z0JBQzFCLE9BQU8sRUFBRSxPQUFPO2dCQUNoQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7Z0JBQ2pCLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVTthQUM1QixDQUFDLENBQUM7U0FDSjtRQUNELFFBQ0UsSUFBSSxZQUFZLENBQUM7WUFDZixNQUFNLEVBQUUsSUFBSSxDQUFDLGFBQWE7WUFDMUIsSUFBSSxFQUFFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJO1lBQzFDLE9BQU8sRUFBRSxPQUFPO1NBQ2pCLENBQUMsRUFDRjtLQUNIOzs7WUF6SEYsVUFBVTs7Ozs0Q0FHSSxNQUFNLFNBQUMsVUFBVTs7Ozs7OztBQXlIaEMsc0JBQXNCLENBQUMsRUFBRSxDQUFDOztJQUV4QixJQUFJLENBQUMsWUFBWSxRQUFRLEVBQUU7UUFDekIsSUFBSSxDQUFDLFlBQVksUUFBUSxFQUFFO1lBQ3pCLE9BQU8sQ0FBQyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUN0QztRQUNELE9BQU8sS0FBSyxDQUFDO0tBQ2Q7SUFDRCxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLFNBQVMsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxTQUFTLEVBQUU7UUFDbEUsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ2hCO0lBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7UUFDMUMsT0FBTyxJQUFJLENBQUM7S0FDYjs7SUFHRCxJQUFJLENBQUMsWUFBWSxJQUFJLEVBQUU7UUFDckIsT0FBTyxLQUFLLENBQUM7S0FDZDtJQUNELElBQUksQ0FBQyxZQUFZLElBQUksRUFBRTtRQUNyQixPQUFPLEtBQUssQ0FBQztLQUNkOztJQUdELElBQUksRUFBRSxDQUFDLFlBQVksTUFBTSxDQUFDLEVBQUU7UUFDMUIsT0FBTyxLQUFLLENBQUM7S0FDZDtJQUNELElBQUksRUFBRSxDQUFDLFlBQVksTUFBTSxDQUFDLEVBQUU7UUFDMUIsT0FBTyxLQUFLLENBQUM7S0FDZDtJQUVELHVCQUFNLENBQUMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3pCLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFTLENBQUM7UUFDdkIsT0FBTyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQ2pDLENBQUMsQ0FBQzs7O0NBSUo7Ozs7OztBQzNLRDtJQU9FLGlCQUFpQjs7O1lBTGxCLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7Ozs7Ozs7OztBQ0pEO0lBYUUsaUJBQWlCOzs7O0lBRWpCLFFBQVE7S0FDUDs7O1lBZEYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxZQUFZO2dCQUN0QixRQUFRLEVBQUU7Ozs7R0FJVDtnQkFDRSxNQUFNLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQzthQUNqQzs7Ozs7Ozs7O0FDVkQ7SUFrQkUsaUJBQWlCOzs7O0lBRWpCLFFBQVE7S0FDUDs7O1lBbkJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZUFBZTtnQkFDekIsUUFBUSxFQUFFOzs7Ozs7Ozs7Q0FTWDtnQkFDQyxNQUFNLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQzthQUNuQzs7Ozs7Ozs7O0FDZEQsd0JBZ0JnQyxTQUFRLFFBQVE7SUFDOUM7UUFDRSxLQUFLLEVBQUUsQ0FBQztLQUNUOzs7Ozs7SUFFRCxTQUFTLENBQUMsR0FBcUIsRUFBRSxJQUFpQjtRQUMzQyx1QkFBTSxVQUFVLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQztZQUMxQixPQUFPLEVBQUUsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLGVBQWUsQ0FBQztTQUN2RCxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1FBQ3ZDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUM7YUFDN0IsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQXFCO1lBQzlCLElBQUksS0FBSyxZQUFZLFlBQVksRUFBRTtnQkFDakMsdUJBQU0sQ0FBQyxxQkFBRyxLQUEwQixDQUFBLENBQUM7Z0JBQ3JDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3ZDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDN0MsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzthQUNqRTtTQUNELEVBQUUsQ0FBQyxLQUF3QjtZQUMxQixJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3hFLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDOUQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQ3RELENBQUMsRUFDQSxVQUFVLENBQUMsQ0FBQyxHQUFHO1lBQ2YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDMUMsT0FBTyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDeEIsQ0FBQyxDQUNILENBQUM7Ozs7O0tBT0Q7OztZQW5DUixVQUFVOzs7Ozs7Ozs7QUNmWCx3QkFrQmdDLFNBQVEsUUFBUTs7OztJQUM5QyxZQUFvQixPQUF3QjtRQUMxQyxLQUFLLEVBQUUsQ0FBQztRQURVLFlBQU8sR0FBUCxPQUFPLENBQWlCO0tBRTNDOzs7Ozs7SUFFRCxTQUFTLENBQUMsR0FBcUIsRUFBRSxJQUFpQjtRQUNqRCx1QkFBTSxJQUFJLEdBQWMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFekQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsdUNBQXVDLENBQUMsQ0FBQztRQUV2RCxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQ2pCLFFBQVEsQ0FBQztZQUNQLElBQUksSUFBSSxFQUFFO2dCQUNSLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDM0M7WUFDRCx1QkFBTSxVQUFVLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQztnQkFDM0IsT0FBTyxFQUFFLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxlQUFlLENBQUM7YUFDdkQsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQ2hDLENBQUMsRUFDQSxXQUFXLEVBQUUsRUFDYixLQUFLLENBQUMsR0FBRyxDQUFDLEVBQ1YsYUFBYSxFQUFFLENBQ2xCLENBQUM7S0FHSDs7O1lBM0JGLFVBQVU7Ozs7WUFQRixlQUFlOzs7Ozs7O0FDWHhCO0lBeUJFO1FBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFBO0tBQ3BDOzs7WUFwQkYsUUFBUSxTQUFDO2dCQUNSLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFlBQVksRUFBRSxFQUFFO2dCQUNoQixTQUFTLEVBQUU7b0JBQ1QsZUFBZTtvQkFDZjt3QkFDRSxPQUFPLEVBQUUsaUJBQWlCO3dCQUMxQixRQUFRLEVBQUUsa0JBQWtCO3dCQUM1QixLQUFLLEVBQUUsSUFBSTtxQkFDWjtvQkFDRDt3QkFDRSxPQUFPLEVBQUUsaUJBQWlCO3dCQUMxQixRQUFRLEVBQUUsa0JBQWtCO3dCQUM1QixLQUFLLEVBQUUsSUFBSTtxQkFDWjtpQkFDRjthQUNGOzs7Ozs7Ozs7QUN2QkQ7OztZQUtDLFFBQVEsU0FBQztnQkFDUixPQUFPLEVBQUU7b0JBQ1AsY0FBYztpQkFDZjtnQkFDRCxZQUFZLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxtQkFBbUIsQ0FBQztnQkFDckQsT0FBTyxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsbUJBQW1CLENBQUM7YUFDakQ7Ozs7Ozs7Ozs7Ozs7OzsifQ==