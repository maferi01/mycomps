import { HttpErrorResponse, HttpRequest, HttpResponse } from '@angular/common/http';
import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUtil } from './base-util';
import { IMock, IMockData } from './imock-data.model';
export declare const TOKEN_MOCK: InjectionToken<{}>;
export declare class MockDataService extends BaseUtil {
    private mockData;
    constructor(mockData: IMock);
    getRequestMock(req: HttpRequest<any>): IMockData;
    getRequestMockData(req: HttpRequest<any>, mockData: IMock): IMockData;
    private matchMethod(data, req);
    private matchUrl(data, req);
    private matchParams(data, req);
    private matchHeaders(data, req);
    private matchBody(data, req);
    getResponseMock(data: IMockData): Observable<any>;
    getResponseMockObj(data: IMockData): HttpErrorResponse | HttpResponse<object>;
}
