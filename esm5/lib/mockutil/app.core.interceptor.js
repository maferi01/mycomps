/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseUtil } from './base-util';
var AppCoreInterceptor = /** @class */ (function (_super) {
    tslib_1.__extends(AppCoreInterceptor, _super);
    function AppCoreInterceptor() {
        return _super.call(this) || this;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    AppCoreInterceptor.prototype.intercept = /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    function (req, next) {
        var _this = this;
        var /** @type {?} */ changedReq = req.clone({
            headers: req.headers.set('My-Header', 'MyHeaderValue')
        });
        this.log.info('pass intercept coreee');
        return next.handle(changedReq)
            .pipe(tap(function (event) {
            if (event instanceof HttpResponse) {
                var /** @type {?} */ r = /** @type {?} */ (event);
                _this.log.info('url resp**===' + r.url);
                _this.log.info('status resp**===' + r.status);
                _this.log.info('headers resp**===' + JSON.stringify(r.headers));
            }
        }, function (error) {
            _this.log.info('headers error resp**==' + JSON.stringify(error.headers));
            _this.log.info('error resp**==' + JSON.stringify(error.error));
            _this.log.info('error core status***' + error.status);
        }), catchError(function (err) {
            _this.log.info('error cath core***' + err);
            return throwError(err);
        }));
        /*   catch((err)=>{
                    console.log('error cath core***'+err);
                    return Observable.throw(err);
                  }); */
    };
    AppCoreInterceptor.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AppCoreInterceptor.ctorParameters = function () { return []; };
    return AppCoreInterceptor;
}(BaseUtil));
export { AppCoreInterceptor };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvcmUuaW50ZXJjZXB0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teWNvbXBzLyIsInNvdXJjZXMiOlsibGliL21vY2t1dGlsL2FwcC5jb3JlLmludGVyY2VwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQ0EsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBS0wsWUFBWSxFQUNiLE1BQU0sc0JBQXNCLENBQUM7QUFDOUIsT0FBTyxFQUFvQixVQUFVLEVBQUMsTUFBTSxNQUFNLENBQUM7QUFFbkQsT0FBTyxFQUFFLFVBQVUsRUFBTyxHQUFHLEVBQXlDLE1BQU0sZ0JBQWdCLENBQUM7QUFFN0YsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGFBQWEsQ0FBQzs7SUFJQyw4Q0FBUTtJQUM5QztlQUNFLGlCQUFPO0tBQ1I7Ozs7OztJQUVELHNDQUFTOzs7OztJQUFULFVBQVUsR0FBcUIsRUFBRSxJQUFpQjtRQUFsRCxpQkE2Qk87UUE1QkEscUJBQU0sVUFBVSxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUM7WUFDMUIsT0FBTyxFQUFFLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxlQUFlLENBQUM7U0FDdkQsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUN2QyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUM7YUFDN0IsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLEtBQXFCO1lBQzlCLEVBQUUsQ0FBQyxDQUFDLEtBQUssWUFBWSxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUNsQyxxQkFBTSxDQUFDLHFCQUFHLEtBQTBCLENBQUEsQ0FBQztnQkFDckMsS0FBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDdkMsS0FBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUM3QyxLQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2FBQ2pFO1NBQ0QsRUFBRSxVQUFDLEtBQXdCO1lBQzFCLEtBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDeEUsS0FBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUM5RCxLQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDdEQsQ0FBQyxFQUNBLFVBQVUsQ0FBQyxVQUFDLEdBQUc7WUFDZixLQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxHQUFHLENBQUMsQ0FBQztZQUMxQyxNQUFNLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ3hCLENBQUMsQ0FDSCxDQUFDOzs7OztLQU9EOztnQkFuQ1IsVUFBVTs7Ozs2QkFoQlg7RUFpQndDLFFBQVE7U0FBbkMsa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSHR0cEVycm9yUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtcclxuICBIdHRwRXZlbnQsXHJcbiAgSHR0cEludGVyY2VwdG9yLFxyXG4gIEh0dHBIYW5kbGVyLFxyXG4gIEh0dHBSZXF1ZXN0LFxyXG4gIEh0dHBSZXNwb25zZVxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSAsICBvZiwgdGhyb3dFcnJvcn0gZnJvbSAncnhqcyc7XHJcblxyXG5pbXBvcnQgeyBjYXRjaEVycm9yLCBtYXAsIHRhcCwgcmVmQ291bnQsIHB1Ymxpc2hSZXBsYXksIGRlbGF5LCByZXRyeSB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgTW9ja0RhdGFTZXJ2aWNlIH0gZnJvbSAnLi9tb2NrLWRhdGEuc2VydmljZSc7XHJcbmltcG9ydCB7IEJhc2VVdGlsIH0gZnJvbSAnLi9iYXNlLXV0aWwnO1xyXG5cclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEFwcENvcmVJbnRlcmNlcHRvciBleHRlbmRzIEJhc2VVdGlsIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHN1cGVyKCk7XHJcbiAgfVxyXG5cclxuICBpbnRlcmNlcHQocmVxOiBIdHRwUmVxdWVzdDxhbnk+LCBuZXh0OiBIdHRwSGFuZGxlcik6IE9ic2VydmFibGU8SHR0cEV2ZW50PGFueT4+IHtcclxuICAgICAgICAgY29uc3QgY2hhbmdlZFJlcSA9IHJlcS5jbG9uZSh7XHJcbiAgICAgICAgICAgIGhlYWRlcnM6IHJlcS5oZWFkZXJzLnNldCgnTXktSGVhZGVyJywgJ015SGVhZGVyVmFsdWUnKVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICB0aGlzLmxvZy5pbmZvKCdwYXNzIGludGVyY2VwdCBjb3JlZWUnKTtcclxuICAgICAgICAgIHJldHVybiBuZXh0LmhhbmRsZShjaGFuZ2VkUmVxKVxyXG4gICAgICAgICAgLnBpcGUodGFwKChldmVudDogSHR0cEV2ZW50PGFueT4pID0+IHtcclxuICAgICAgICAgICAgaWYgKGV2ZW50IGluc3RhbmNlb2YgSHR0cFJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgciA9IGV2ZW50IGFzIEh0dHBSZXNwb25zZTxhbnk+O1xyXG4gICAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ3VybCByZXNwKio9PT0nICsgci51cmwpO1xyXG4gICAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ3N0YXR1cyByZXNwKio9PT0nICsgci5zdGF0dXMpO1xyXG4gICAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ2hlYWRlcnMgcmVzcCoqPT09JyArIEpTT04uc3RyaW5naWZ5KHIuaGVhZGVycykpO1xyXG4gICAgICAgICAgIH1cclxuICAgICAgICAgIH0sIChlcnJvcjogSHR0cEVycm9yUmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5sb2cuaW5mbygnaGVhZGVycyBlcnJvciByZXNwKio9PScgKyBKU09OLnN0cmluZ2lmeShlcnJvci5oZWFkZXJzKSk7XHJcbiAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ2Vycm9yIHJlc3AqKj09JyArIEpTT04uc3RyaW5naWZ5KGVycm9yLmVycm9yKSk7XHJcbiAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ2Vycm9yIGNvcmUgc3RhdHVzKioqJyArIGVycm9yLnN0YXR1cyk7XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICAgLCBjYXRjaEVycm9yKChlcnIpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5sb2cuaW5mbygnZXJyb3IgY2F0aCBjb3JlKioqJyArIGVycik7XHJcbiAgICAgICAgICAgIHJldHVybiB0aHJvd0Vycm9yKGVycik7XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIC8qICAgY2F0Y2goKGVycik9PntcclxuICAgICAgICAgICAgY29uc29sZS5sb2coJ2Vycm9yIGNhdGggY29yZSoqKicrZXJyKTtcclxuICAgICAgICAgICAgcmV0dXJuIE9ic2VydmFibGUudGhyb3coZXJyKTtcclxuICAgICAgICAgIH0pOyAqL1xyXG5cclxuICAgICAgICB9XHJcblxyXG59XHJcbiJdfQ==