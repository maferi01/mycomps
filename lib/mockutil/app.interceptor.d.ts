import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { MockDataService } from './mock-data.service';
import { Observable } from 'rxjs';
import { BaseUtil } from './base-util';
export declare class AppInterceptorMock extends BaseUtil implements HttpInterceptor {
    private mockSrv;
    constructor(mockSrv: MockDataService);
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
