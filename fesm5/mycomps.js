import { __extends } from 'tslib';
import { HttpErrorResponse, HttpHeaders, HttpResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Inject, Injectable, InjectionToken, Component, NgModule, defineInjectable } from '@angular/core';
import { of, throwError } from 'rxjs';
import { catchError, tap, delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
var  /**
 * @abstract
 */
BaseUtil = /** @class */ (function () {
    function BaseUtil() {
        this.log = {
            info: /**
             * @param {?} msg
             * @return {?}
             */
            function (msg) {
                console.log(msg);
            }
        };
        this.log.info('Class ' + this.constructor.name + ' created');
    }
    return BaseUtil;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var /** @type {?} */ TOKEN_MOCK = new InjectionToken('mockdata');
var MockDataService = /** @class */ (function (_super) {
    __extends(MockDataService, _super);
    function MockDataService(mockData) {
        var _this = _super.call(this) || this;
        _this.mockData = mockData;
        return _this;
    }
    /**
     * @param {?} req
     * @return {?}
     */
    MockDataService.prototype.getRequestMock = /**
     * @param {?} req
     * @return {?}
     */
    function (req) {
        var _this = this;
        return this.mockData.mocks.find(function (data) {
            // check if the method and url and params and match params
            if (req.method === data.type &&
                req.url.includes(data.url) &&
                _this.matchHeaders(data, req) &&
                _this.matchParams(data, req) &&
                _this.matchBody(data, req)) {
                if (data.name) {
                    _this.log.info('---mock data matched ' + data.name);
                }
                return true;
            }
            return false;
        });
    };
    /**
     * @param {?} req
     * @param {?} mockData
     * @return {?}
     */
    MockDataService.prototype.getRequestMockData = /**
     * @param {?} req
     * @param {?} mockData
     * @return {?}
     */
    function (req, mockData) {
        var _this = this;
        if (!mockData) {
            return null;
        }
        return mockData.mocks.find(function (data) {
            // check if the method and url and params and match params
            if (_this.matchMethod(data, req) &&
                _this.matchUrl(data, req) &&
                _this.matchHeaders(data, req) &&
                _this.matchParams(data, req) &&
                _this.matchBody(data, req)) {
                if (data.name) {
                    _this.log.info('---mock data matched ' + data.name);
                }
                return true;
            }
            return false;
        });
    };
    /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    MockDataService.prototype.matchMethod = /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    function (data, req) {
        if (data.type) {
            return req.method === data.type;
        }
        return true;
    };
    /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    MockDataService.prototype.matchUrl = /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    function (data, req) {
        if (data.url) {
            return req.url.includes(data.url);
        }
        return true;
    };
    /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    MockDataService.prototype.matchParams = /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    function (data, req) {
        if (data.params) {
            return data.params.every(function (param) { return req.params[param.key] === param.value || (req.params.get && req.params.get(param.key) === param.value); });
        }
        return true;
    };
    /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    MockDataService.prototype.matchHeaders = /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    function (data, req) {
        if (data.headersRequest) {
            return data.headersRequest.every(function (header) { return req.headers.get(header.key) === header.value; });
        }
        return true;
    };
    /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    MockDataService.prototype.matchBody = /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    function (data, req) {
        if (data.body) {
            var /** @type {?} */ res = objectEquals(data.body, req.body);
            this.log.info(" " + data.name + " -compare res=" + res + " body req " + JSON.stringify(req.body));
            // return JSON.stringify(data.body)==JSON.stringify(req.body);
            return res;
        }
        return true;
    };
    /**
     * @param {?} data
     * @return {?}
     */
    MockDataService.prototype.getResponseMock = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        var /** @type {?} */ resp = this.getResponseMockObj(data);
        if (resp instanceof HttpErrorResponse) {
            return throwError(resp);
        }
        else {
            return of(resp);
        }
    };
    /**
     * @param {?} data
     * @return {?}
     */
    MockDataService.prototype.getResponseMockObj = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        if (!data.resposeStatus) {
            data.resposeStatus = 200;
        }
        var /** @type {?} */ headers = new HttpHeaders();
        if (data.headersResponse) {
            data.headersResponse.forEach(function (header) { return (headers = headers.append(header.key, header.value)); });
        }
        if (data.resposeStatus !== 200) {
            return new HttpErrorResponse({
                status: data.resposeStatus,
                headers: headers,
                error: data.error,
                statusText: data.statusText
            });
        }
        return (new HttpResponse({
            status: data.resposeStatus,
            body: data.response ? data.response : null,
            headers: headers
        }));
    };
    MockDataService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    MockDataService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [TOKEN_MOCK,] }] }
    ]; };
    return MockDataService;
}(BaseUtil));
/**
 * @param {?} x
 * @param {?} y
 * @return {?}
 */
function objectEquals(x, y) {
    // if both are function
    if (x instanceof Function) {
        if (y instanceof Function) {
            return x.toString() === y.toString();
        }
        return false;
    }
    if (x === null || x === undefined || y === null || y === undefined) {
        return x === y;
    }
    if (x === y || x.valueOf() === y.valueOf()) {
        return true;
    }
    // if one of them is date, they must had equal valueOf
    if (x instanceof Date) {
        return false;
    }
    if (y instanceof Date) {
        return false;
    }
    // if they are not function or strictly equal, they both need to be Objects
    if (!(x instanceof Object)) {
        return false;
    }
    if (!(y instanceof Object)) {
        return false;
    }
    var /** @type {?} */ p = Object.keys(x);
    return p.every(function (i) {
        return objectEquals(x[i], y[i]);
    });
    /* return Object.keys(y).every(function (i) { return p.indexOf(i) !== -1; }) ?
              p.every(function (i) { return objectEquals(x[i], y[i]); }) : false; */
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var MycompsService = /** @class */ (function () {
    function MycompsService() {
    }
    MycompsService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] },
    ];
    /** @nocollapse */
    MycompsService.ctorParameters = function () { return []; };
    /** @nocollapse */ MycompsService.ngInjectableDef = defineInjectable({ factory: function MycompsService_Factory() { return new MycompsService(); }, token: MycompsService, providedIn: "root" });
    return MycompsService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var MycompsComponent = /** @class */ (function () {
    function MycompsComponent() {
    }
    /**
     * @return {?}
     */
    MycompsComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    MycompsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'my-mycomps',
                    template: "\n    <p>\n      mycomps works artur!\n    </p>\n  ",
                    styles: ["p{background:red}"]
                },] },
    ];
    /** @nocollapse */
    MycompsComponent.ctorParameters = function () { return []; };
    return MycompsComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var MytestcompComponent = /** @class */ (function () {
    function MytestcompComponent() {
    }
    /**
     * @return {?}
     */
    MytestcompComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    MytestcompComponent.decorators = [
        { type: Component, args: [{
                    selector: 'my-mytestcomp',
                    template: "\n<div>\n   <p>\n  mytestcomp works***** from library!\n</p>\n    \n</div>\n\n\n",
                    styles: ["div p{background:pink}"]
                },] },
    ];
    /** @nocollapse */
    MytestcompComponent.ctorParameters = function () { return []; };
    return MytestcompComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var AppCoreInterceptor = /** @class */ (function (_super) {
    __extends(AppCoreInterceptor, _super);
    function AppCoreInterceptor() {
        return _super.call(this) || this;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    AppCoreInterceptor.prototype.intercept = /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    function (req, next) {
        var _this = this;
        var /** @type {?} */ changedReq = req.clone({
            headers: req.headers.set('My-Header', 'MyHeaderValue')
        });
        this.log.info('pass intercept coreee');
        return next.handle(changedReq)
            .pipe(tap(function (event) {
            if (event instanceof HttpResponse) {
                var /** @type {?} */ r = /** @type {?} */ (event);
                _this.log.info('url resp**===' + r.url);
                _this.log.info('status resp**===' + r.status);
                _this.log.info('headers resp**===' + JSON.stringify(r.headers));
            }
        }, function (error) {
            _this.log.info('headers error resp**==' + JSON.stringify(error.headers));
            _this.log.info('error resp**==' + JSON.stringify(error.error));
            _this.log.info('error core status***' + error.status);
        }), catchError(function (err) {
            _this.log.info('error cath core***' + err);
            return throwError(err);
        }));
        /*   catch((err)=>{
                    console.log('error cath core***'+err);
                    return Observable.throw(err);
                  }); */
    };
    AppCoreInterceptor.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AppCoreInterceptor.ctorParameters = function () { return []; };
    return AppCoreInterceptor;
}(BaseUtil));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var AppInterceptorMock = /** @class */ (function (_super) {
    __extends(AppInterceptorMock, _super);
    function AppInterceptorMock(mockSrv) {
        var _this = _super.call(this) || this;
        _this.mockSrv = mockSrv;
        return _this;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    AppInterceptorMock.prototype.intercept = /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    function (req, next) {
        var _this = this;
        var /** @type {?} */ mock = this.mockSrv.getRequestMock(req);
        this.log.info('enter app interceptor MOCK***********');
        return of(null).pipe(mergeMap(function () {
            if (mock) {
                return _this.mockSrv.getResponseMock(mock);
            }
            var /** @type {?} */ changedReq = req.clone({
                headers: req.headers.set('My-Header', 'MyHeaderValue')
            });
            return next.handle(changedReq);
        }), materialize(), delay(500), dematerialize());
    };
    AppInterceptorMock.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AppInterceptorMock.ctorParameters = function () { return [
        { type: MockDataService }
    ]; };
    return AppInterceptorMock;
}(BaseUtil));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var MockUtilModule = /** @class */ (function () {
    function MockUtilModule() {
        console.log('enter mockkkk module');
    }
    MockUtilModule.decorators = [
        { type: NgModule, args: [{
                    imports: [],
                    declarations: [],
                    providers: [
                        MockDataService,
                        {
                            provide: HTTP_INTERCEPTORS,
                            useClass: AppCoreInterceptor,
                            multi: true
                        },
                        {
                            provide: HTTP_INTERCEPTORS,
                            useClass: AppInterceptorMock,
                            multi: true
                        }
                    ]
                },] },
    ];
    /** @nocollapse */
    MockUtilModule.ctorParameters = function () { return []; };
    return MockUtilModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var MycompsModule = /** @class */ (function () {
    function MycompsModule() {
    }
    MycompsModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        MockUtilModule
                    ],
                    declarations: [MycompsComponent, MytestcompComponent],
                    exports: [MycompsComponent, MytestcompComponent]
                },] },
    ];
    return MycompsModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { TOKEN_MOCK, MockDataService, MycompsService, MycompsComponent, MycompsModule, AppCoreInterceptor as ɵc, AppInterceptorMock as ɵd, BaseUtil as ɵa, MockUtilModule as ɵb, MytestcompComponent as ɵe };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXljb21wcy5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vbXljb21wcy9saWIvbW9ja3V0aWwvYmFzZS11dGlsLnRzIiwibmc6Ly9teWNvbXBzL2xpYi9tb2NrdXRpbC9tb2NrLWRhdGEuc2VydmljZS50cyIsIm5nOi8vbXljb21wcy9saWIvbXljb21wcy5zZXJ2aWNlLnRzIiwibmc6Ly9teWNvbXBzL2xpYi9teWNvbXBzLmNvbXBvbmVudC50cyIsIm5nOi8vbXljb21wcy9saWIvbXl0ZXN0Y29tcC9teXRlc3Rjb21wLmNvbXBvbmVudC50cyIsIm5nOi8vbXljb21wcy9saWIvbW9ja3V0aWwvYXBwLmNvcmUuaW50ZXJjZXB0b3IudHMiLCJuZzovL215Y29tcHMvbGliL21vY2t1dGlsL2FwcC5pbnRlcmNlcHRvci50cyIsIm5nOi8vbXljb21wcy9saWIvbW9ja3V0aWwvbW9jay11dGlsLm1vZHVsZS50cyIsIm5nOi8vbXljb21wcy9saWIvbXljb21wcy5tb2R1bGUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBCYXNlVXRpbCB7XHJcbiAgcHJvdGVjdGVkIGxvZztcclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHRoaXMubG9nID0ge1xyXG4gICAgICBpbmZvKG1zZykge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKG1zZyk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcbiAgICB0aGlzLmxvZy5pbmZvKCdDbGFzcyAnICsgdGhpcy5jb25zdHJ1Y3Rvci5uYW1lICsgJyBjcmVhdGVkJyk7XHJcbiAgfVxyXG59XHJcbiIsImltcG9ydCB7IEh0dHBFcnJvclJlc3BvbnNlLCBIdHRwSGVhZGVycywgSHR0cFJlcXVlc3QsIEh0dHBSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlLCBJbmplY3Rpb25Ub2tlbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBvZiwgdGhyb3dFcnJvciwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQmFzZVV0aWwgfSBmcm9tICcuL2Jhc2UtdXRpbCc7XHJcbmltcG9ydCB7IElIZWFkZXIsIElNb2NrLCBJTW9ja0RhdGEsIElQYXJhbSB9IGZyb20gJy4vaW1vY2stZGF0YS5tb2RlbCc7XHJcblxyXG5leHBvcnQgY29uc3QgVE9LRU5fTU9DSyA9IG5ldyBJbmplY3Rpb25Ub2tlbignbW9ja2RhdGEnKTtcclxuLy8gaW1wb3J0ICdyeGpzL2FkZC9vYnNlcnZhYmxlL3Rocm93JztcclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTW9ja0RhdGFTZXJ2aWNlIGV4dGVuZHMgQmFzZVV0aWwge1xyXG5cclxuICBjb25zdHJ1Y3RvcihASW5qZWN0KFRPS0VOX01PQ0spIHByaXZhdGUgbW9ja0RhdGE6IElNb2NrKSB7XHJcbiAgICBzdXBlcigpO1xyXG4gIH1cclxuXHJcblxyXG4gIGdldFJlcXVlc3RNb2NrKHJlcTogSHR0cFJlcXVlc3Q8YW55Pik6IElNb2NrRGF0YSB7XHJcbiAgICByZXR1cm4gdGhpcy5tb2NrRGF0YS5tb2Nrcy5maW5kKChkYXRhOiBJTW9ja0RhdGEpID0+IHtcclxuICAgICAgLy8gY2hlY2sgaWYgdGhlIG1ldGhvZCBhbmQgdXJsIGFuZCBwYXJhbXMgYW5kIG1hdGNoIHBhcmFtc1xyXG4gICAgICBpZiAoXHJcbiAgICAgICAgcmVxLm1ldGhvZCA9PT0gZGF0YS50eXBlICYmXHJcbiAgICAgICAgcmVxLnVybC5pbmNsdWRlcyhkYXRhLnVybCkgJiZcclxuICAgICAgICB0aGlzLm1hdGNoSGVhZGVycyhkYXRhLCByZXEpICYmXHJcbiAgICAgICAgdGhpcy5tYXRjaFBhcmFtcyhkYXRhLCByZXEpICYmXHJcbiAgICAgICAgdGhpcy5tYXRjaEJvZHkoZGF0YSwgcmVxKVxyXG4gICAgICApIHtcclxuICAgICAgICBpZiAoZGF0YS5uYW1lKSB7XHJcbiAgICAgICAgICB0aGlzLmxvZy5pbmZvKCctLS1tb2NrIGRhdGEgbWF0Y2hlZCAnICsgZGF0YS5uYW1lKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBnZXRSZXF1ZXN0TW9ja0RhdGEocmVxOiBIdHRwUmVxdWVzdDxhbnk+LCBtb2NrRGF0YTogSU1vY2spOiBJTW9ja0RhdGEge1xyXG4gICAgaWYgKCFtb2NrRGF0YSkge1xyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gbW9ja0RhdGEubW9ja3MuZmluZCgoZGF0YTogSU1vY2tEYXRhKSA9PiB7XHJcbiAgICAgIC8vIGNoZWNrIGlmIHRoZSBtZXRob2QgYW5kIHVybCBhbmQgcGFyYW1zIGFuZCBtYXRjaCBwYXJhbXNcclxuICAgICAgaWYgKFxyXG4gICAgICAgIHRoaXMubWF0Y2hNZXRob2QoZGF0YSwgcmVxKSAmJlxyXG4gICAgICAgIHRoaXMubWF0Y2hVcmwoZGF0YSwgcmVxKSAmJlxyXG4gICAgICAgIHRoaXMubWF0Y2hIZWFkZXJzKGRhdGEsIHJlcSkgJiZcclxuICAgICAgICB0aGlzLm1hdGNoUGFyYW1zKGRhdGEsIHJlcSkgJiZcclxuICAgICAgICB0aGlzLm1hdGNoQm9keShkYXRhLCByZXEpXHJcbiAgICAgICkge1xyXG4gICAgICAgIGlmIChkYXRhLm5hbWUpIHtcclxuICAgICAgICAgIHRoaXMubG9nLmluZm8oJy0tLW1vY2sgZGF0YSBtYXRjaGVkICcgKyBkYXRhLm5hbWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgbWF0Y2hNZXRob2QoZGF0YTogSU1vY2tEYXRhLCByZXE6IEh0dHBSZXF1ZXN0PGFueT4pOiBib29sZWFuIHtcclxuICAgIGlmIChkYXRhLnR5cGUpIHtcclxuICAgICAgcmV0dXJuIHJlcS5tZXRob2QgPT09IGRhdGEudHlwZTtcclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBtYXRjaFVybChkYXRhOiBJTW9ja0RhdGEsIHJlcTogSHR0cFJlcXVlc3Q8YW55Pik6IGJvb2xlYW4ge1xyXG4gICAgaWYgKGRhdGEudXJsKSB7XHJcbiAgICAgIHJldHVybiByZXEudXJsLmluY2x1ZGVzKGRhdGEudXJsKTtcclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBtYXRjaFBhcmFtcyhkYXRhOiBJTW9ja0RhdGEsIHJlcTogSHR0cFJlcXVlc3Q8YW55Pik6IGJvb2xlYW4ge1xyXG4gICAgaWYgKGRhdGEucGFyYW1zKSB7XHJcbiAgICAgIHJldHVybiBkYXRhLnBhcmFtcy5ldmVyeShcclxuICAgICAgICAocGFyYW06IElQYXJhbSkgPT4gcmVxLnBhcmFtc1twYXJhbS5rZXldID09PSBwYXJhbS52YWx1ZSB8fCAocmVxLnBhcmFtcy5nZXQgJiYgcmVxLnBhcmFtcy5nZXQocGFyYW0ua2V5KSA9PT0gcGFyYW0udmFsdWUpXHJcbiAgICAgICk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcbiAgcHJpdmF0ZSBtYXRjaEhlYWRlcnMoZGF0YTogSU1vY2tEYXRhLCByZXE6IEh0dHBSZXF1ZXN0PGFueT4pOiBib29sZWFuIHtcclxuICAgIGlmIChkYXRhLmhlYWRlcnNSZXF1ZXN0KSB7XHJcbiAgICAgIHJldHVybiBkYXRhLmhlYWRlcnNSZXF1ZXN0LmV2ZXJ5KChoZWFkZXI6IElIZWFkZXIpID0+IHJlcS5oZWFkZXJzLmdldChoZWFkZXIua2V5KSA9PT0gaGVhZGVyLnZhbHVlKTtcclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBtYXRjaEJvZHkoZGF0YTogSU1vY2tEYXRhLCByZXE6IEh0dHBSZXF1ZXN0PGFueT4pOiBib29sZWFuIHtcclxuICAgIGlmIChkYXRhLmJvZHkpIHtcclxuICAgICAgY29uc3QgcmVzID0gb2JqZWN0RXF1YWxzKGRhdGEuYm9keSwgcmVxLmJvZHkpO1xyXG4gICAgICB0aGlzLmxvZy5pbmZvKGAgJHtkYXRhLm5hbWV9IC1jb21wYXJlIHJlcz0ke3Jlc30gYm9keSByZXEgJHtKU09OLnN0cmluZ2lmeShyZXEuYm9keSl9YCk7XHJcbiAgICAgIC8vIHJldHVybiBKU09OLnN0cmluZ2lmeShkYXRhLmJvZHkpPT1KU09OLnN0cmluZ2lmeShyZXEuYm9keSk7XHJcbiAgICAgIHJldHVybiByZXM7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIGdldFJlc3BvbnNlTW9jayhkYXRhOiBJTW9ja0RhdGEpOk9ic2VydmFibGU8YW55PiB7XHJcbiAgICBjb25zdCByZXNwID0gdGhpcy5nZXRSZXNwb25zZU1vY2tPYmooZGF0YSk7XHJcbiAgICBpZiAocmVzcCBpbnN0YW5jZW9mIEh0dHBFcnJvclJlc3BvbnNlKSB7XHJcbiAgICAgIHJldHVybiB0aHJvd0Vycm9yKHJlc3ApO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIG9mKHJlc3ApO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0UmVzcG9uc2VNb2NrT2JqKGRhdGE6IElNb2NrRGF0YSk6IEh0dHBFcnJvclJlc3BvbnNlIHwgSHR0cFJlc3BvbnNlPG9iamVjdD4ge1xyXG4gICAgaWYgKCFkYXRhLnJlc3Bvc2VTdGF0dXMpIHtcclxuICAgICAgZGF0YS5yZXNwb3NlU3RhdHVzID0gMjAwO1xyXG4gICAgfVxyXG4gICAgbGV0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoKTtcclxuICAgIGlmIChkYXRhLmhlYWRlcnNSZXNwb25zZSkge1xyXG4gICAgICBkYXRhLmhlYWRlcnNSZXNwb25zZS5mb3JFYWNoKGhlYWRlciA9PiAoaGVhZGVycyA9IGhlYWRlcnMuYXBwZW5kKGhlYWRlci5rZXksIGhlYWRlci52YWx1ZSkpKTtcclxuICAgIH1cclxuICAgIGlmIChkYXRhLnJlc3Bvc2VTdGF0dXMgIT09IDIwMCkge1xyXG4gICAgICByZXR1cm4gbmV3IEh0dHBFcnJvclJlc3BvbnNlKHtcclxuICAgICAgICBzdGF0dXM6IGRhdGEucmVzcG9zZVN0YXR1cyxcclxuICAgICAgICBoZWFkZXJzOiBoZWFkZXJzLFxyXG4gICAgICAgIGVycm9yOiBkYXRhLmVycm9yLFxyXG4gICAgICAgIHN0YXR1c1RleHQ6IGRhdGEuc3RhdHVzVGV4dFxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiAoXHJcbiAgICAgIG5ldyBIdHRwUmVzcG9uc2Uoe1xyXG4gICAgICAgIHN0YXR1czogZGF0YS5yZXNwb3NlU3RhdHVzLFxyXG4gICAgICAgIGJvZHk6IGRhdGEucmVzcG9uc2UgPyBkYXRhLnJlc3BvbnNlIDogbnVsbCxcclxuICAgICAgICBoZWFkZXJzOiBoZWFkZXJzXHJcbiAgICAgIH0pXHJcbiAgICApO1xyXG4gIH1cclxufVxyXG5cclxuZnVuY3Rpb24gb2JqZWN0RXF1YWxzKHgsIHkpIHtcclxuICAvLyBpZiBib3RoIGFyZSBmdW5jdGlvblxyXG4gIGlmICh4IGluc3RhbmNlb2YgRnVuY3Rpb24pIHtcclxuICAgIGlmICh5IGluc3RhbmNlb2YgRnVuY3Rpb24pIHtcclxuICAgICAgcmV0dXJuIHgudG9TdHJpbmcoKSA9PT0geS50b1N0cmluZygpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuICBpZiAoeCA9PT0gbnVsbCB8fCB4ID09PSB1bmRlZmluZWQgfHwgeSA9PT0gbnVsbCB8fCB5ID09PSB1bmRlZmluZWQpIHtcclxuICAgIHJldHVybiB4ID09PSB5O1xyXG4gIH1cclxuICBpZiAoeCA9PT0geSB8fCB4LnZhbHVlT2YoKSA9PT0geS52YWx1ZU9mKCkpIHtcclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgLy8gaWYgb25lIG9mIHRoZW0gaXMgZGF0ZSwgdGhleSBtdXN0IGhhZCBlcXVhbCB2YWx1ZU9mXHJcbiAgaWYgKHggaW5zdGFuY2VvZiBEYXRlKSB7XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG4gIGlmICh5IGluc3RhbmNlb2YgRGF0ZSkge1xyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgLy8gaWYgdGhleSBhcmUgbm90IGZ1bmN0aW9uIG9yIHN0cmljdGx5IGVxdWFsLCB0aGV5IGJvdGggbmVlZCB0byBiZSBPYmplY3RzXHJcbiAgaWYgKCEoeCBpbnN0YW5jZW9mIE9iamVjdCkpIHtcclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcbiAgaWYgKCEoeSBpbnN0YW5jZW9mIE9iamVjdCkpIHtcclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcblxyXG4gIGNvbnN0IHAgPSBPYmplY3Qua2V5cyh4KTtcclxuICByZXR1cm4gcC5ldmVyeShmdW5jdGlvbihpKSB7XHJcbiAgICByZXR1cm4gb2JqZWN0RXF1YWxzKHhbaV0sIHlbaV0pO1xyXG4gIH0pO1xyXG5cclxuICAvKiByZXR1cm4gT2JqZWN0LmtleXMoeSkuZXZlcnkoZnVuY3Rpb24gKGkpIHsgcmV0dXJuIHAuaW5kZXhPZihpKSAhPT0gLTE7IH0pID9cclxuICAgICAgICAgIHAuZXZlcnkoZnVuY3Rpb24gKGkpIHsgcmV0dXJuIG9iamVjdEVxdWFscyh4W2ldLCB5W2ldKTsgfSkgOiBmYWxzZTsgKi9cclxufVxyXG4iLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNeWNvbXBzU2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbXktbXljb21wcycsXG4gIHRlbXBsYXRlOiBgXG4gICAgPHA+XG4gICAgICBteWNvbXBzIHdvcmtzIGFydHVyIVxuICAgIDwvcD5cbiAgYFxuICAsICBzdHlsZXM6IFtgcHtiYWNrZ3JvdW5kOnJlZH1gXVxufSlcbmV4cG9ydCBjbGFzcyBNeWNvbXBzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ215LW15dGVzdGNvbXAnLFxuICB0ZW1wbGF0ZTogYFxuPGRpdj5cbiAgIDxwPlxuICBteXRlc3Rjb21wIHdvcmtzKioqKiogZnJvbSBsaWJyYXJ5IVxuPC9wPlxuICAgIFxuPC9kaXY+XG5cblxuYCxcbiAgc3R5bGVzOiBbYGRpdiBwe2JhY2tncm91bmQ6cGlua31gXVxufSlcbmV4cG9ydCBjbGFzcyBNeXRlc3Rjb21wQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbn1cbiIsImltcG9ydCB7IEh0dHBFcnJvclJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7XHJcbiAgSHR0cEV2ZW50LFxyXG4gIEh0dHBJbnRlcmNlcHRvcixcclxuICBIdHRwSGFuZGxlcixcclxuICBIdHRwUmVxdWVzdCxcclxuICBIdHRwUmVzcG9uc2VcclxufSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgLCAgb2YsIHRocm93RXJyb3J9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwLCB0YXAsIHJlZkNvdW50LCBwdWJsaXNoUmVwbGF5LCBkZWxheSwgcmV0cnkgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IE1vY2tEYXRhU2VydmljZSB9IGZyb20gJy4vbW9jay1kYXRhLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBCYXNlVXRpbCB9IGZyb20gJy4vYmFzZS11dGlsJztcclxuXHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBBcHBDb3JlSW50ZXJjZXB0b3IgZXh0ZW5kcyBCYXNlVXRpbCBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICBzdXBlcigpO1xyXG4gIH1cclxuXHJcbiAgaW50ZXJjZXB0KHJlcTogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XHJcbiAgICAgICAgIGNvbnN0IGNoYW5nZWRSZXEgPSByZXEuY2xvbmUoe1xyXG4gICAgICAgICAgICBoZWFkZXJzOiByZXEuaGVhZGVycy5zZXQoJ015LUhlYWRlcicsICdNeUhlYWRlclZhbHVlJylcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgdGhpcy5sb2cuaW5mbygncGFzcyBpbnRlcmNlcHQgY29yZWVlJyk7XHJcbiAgICAgICAgICByZXR1cm4gbmV4dC5oYW5kbGUoY2hhbmdlZFJlcSlcclxuICAgICAgICAgIC5waXBlKHRhcCgoZXZlbnQ6IEh0dHBFdmVudDxhbnk+KSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChldmVudCBpbnN0YW5jZW9mIEh0dHBSZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgIGNvbnN0IHIgPSBldmVudCBhcyBIdHRwUmVzcG9uc2U8YW55PjtcclxuICAgICAgICAgICAgICB0aGlzLmxvZy5pbmZvKCd1cmwgcmVzcCoqPT09JyArIHIudXJsKTtcclxuICAgICAgICAgICAgICB0aGlzLmxvZy5pbmZvKCdzdGF0dXMgcmVzcCoqPT09JyArIHIuc3RhdHVzKTtcclxuICAgICAgICAgICAgICB0aGlzLmxvZy5pbmZvKCdoZWFkZXJzIHJlc3AqKj09PScgKyBKU09OLnN0cmluZ2lmeShyLmhlYWRlcnMpKTtcclxuICAgICAgICAgICB9XHJcbiAgICAgICAgICB9LCAoZXJyb3I6IEh0dHBFcnJvclJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ2hlYWRlcnMgZXJyb3IgcmVzcCoqPT0nICsgSlNPTi5zdHJpbmdpZnkoZXJyb3IuaGVhZGVycykpO1xyXG4gICAgICAgICAgICB0aGlzLmxvZy5pbmZvKCdlcnJvciByZXNwKio9PScgKyBKU09OLnN0cmluZ2lmeShlcnJvci5lcnJvcikpO1xyXG4gICAgICAgICAgICB0aGlzLmxvZy5pbmZvKCdlcnJvciBjb3JlIHN0YXR1cyoqKicgKyBlcnJvci5zdGF0dXMpO1xyXG4gICAgICAgICAgfSlcclxuICAgICAgICAgICwgY2F0Y2hFcnJvcigoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ2Vycm9yIGNhdGggY29yZSoqKicgKyBlcnIpO1xyXG4gICAgICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihlcnIpO1xyXG4gICAgICAgICAgfSlcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICAvKiAgIGNhdGNoKChlcnIpPT57XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdlcnJvciBjYXRoIGNvcmUqKionK2Vycik7XHJcbiAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLnRocm93KGVycik7XHJcbiAgICAgICAgICB9KTsgKi9cclxuXHJcbiAgICAgICAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQgeyBIdHRwRXJyb3JSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge1xyXG4gIEh0dHBFdmVudCxcclxuICBIdHRwSW50ZXJjZXB0b3IsXHJcbiAgSHR0cEhhbmRsZXIsXHJcbiAgSHR0cFJlcXVlc3QsXHJcbiAgSHR0cFJlc3BvbnNlXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5cclxuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwLCB0YXAsIHJlZkNvdW50LCBwdWJsaXNoUmVwbGF5LCBkZWxheSwgcmV0cnksIG1lcmdlTWFwLCBtYXRlcmlhbGl6ZSwgZGVtYXRlcmlhbGl6ZSB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgTW9ja0RhdGFTZXJ2aWNlIH0gZnJvbSAnLi9tb2NrLWRhdGEuc2VydmljZSc7XHJcbmltcG9ydCB7IElNb2NrRGF0YSB9IGZyb20gJy4vaW1vY2stZGF0YS5tb2RlbCc7XHJcblxyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBCYXNlVXRpbCB9IGZyb20gJy4vYmFzZS11dGlsJztcclxuXHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBBcHBJbnRlcmNlcHRvck1vY2sgZXh0ZW5kcyBCYXNlVXRpbCBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBtb2NrU3J2OiBNb2NrRGF0YVNlcnZpY2UpIHtcclxuICAgIHN1cGVyKCk7XHJcbiAgfVxyXG5cclxuICBpbnRlcmNlcHQocmVxOiBIdHRwUmVxdWVzdDxhbnk+LCBuZXh0OiBIdHRwSGFuZGxlcik6IE9ic2VydmFibGU8SHR0cEV2ZW50PGFueT4+IHtcclxuICAgY29uc3QgbW9jazogSU1vY2tEYXRhID0gdGhpcy5tb2NrU3J2LmdldFJlcXVlc3RNb2NrKHJlcSk7XHJcblxyXG4gICB0aGlzLmxvZy5pbmZvKCdlbnRlciBhcHAgaW50ZXJjZXB0b3IgTU9DSyoqKioqKioqKioqJyk7XHJcblxyXG4gICByZXR1cm4gb2YobnVsbCkucGlwZShcclxuICAgICAgbWVyZ2VNYXAoKCkgPT4ge1xyXG4gICAgICAgIGlmIChtb2NrKSB7XHJcbiAgICAgICAgICByZXR1cm4gdGhpcy5tb2NrU3J2LmdldFJlc3BvbnNlTW9jayhtb2NrKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgY2hhbmdlZFJlcSA9IHJlcS5jbG9uZSh7XHJcbiAgICAgICAgICBoZWFkZXJzOiByZXEuaGVhZGVycy5zZXQoJ015LUhlYWRlcicsICdNeUhlYWRlclZhbHVlJylcclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gbmV4dC5oYW5kbGUoY2hhbmdlZFJlcSk7XHJcbiAgICAgIH0pXHJcbiAgICAgICwgbWF0ZXJpYWxpemUoKVxyXG4gICAgICAsIGRlbGF5KDUwMClcclxuICAgICAgLCBkZW1hdGVyaWFsaXplKClcclxuICAgICk7XHJcblxyXG5cclxuICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgSFRUUF9JTlRFUkNFUFRPUlMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBBcHBDb3JlSW50ZXJjZXB0b3IgfSBmcm9tICcuL2FwcC5jb3JlLmludGVyY2VwdG9yJztcclxuaW1wb3J0IHsgQXBwSW50ZXJjZXB0b3JNb2NrIH0gZnJvbSAnLi9hcHAuaW50ZXJjZXB0b3InO1xyXG5pbXBvcnQgeyBNb2NrRGF0YVNlcnZpY2UgfSBmcm9tICcuL21vY2stZGF0YS5zZXJ2aWNlJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgaW1wb3J0czogW10sXHJcbiAgZGVjbGFyYXRpb25zOiBbXSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIE1vY2tEYXRhU2VydmljZSxcclxuICAgIHtcclxuICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXHJcbiAgICAgIHVzZUNsYXNzOiBBcHBDb3JlSW50ZXJjZXB0b3IsXHJcbiAgICAgIG11bHRpOiB0cnVlXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBIVFRQX0lOVEVSQ0VQVE9SUyxcclxuICAgICAgdXNlQ2xhc3M6IEFwcEludGVyY2VwdG9yTW9jayxcclxuICAgICAgbXVsdGk6IHRydWVcclxuICAgIH1cclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNb2NrVXRpbE1vZHVsZSB7XHJcbiAgY29uc3RydWN0b3IoKXtcclxuICAgIGNvbnNvbGUubG9nKCdlbnRlciBtb2Nra2trIG1vZHVsZScpXHJcbiAgfVxyXG59XHJcbiIsImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNeWNvbXBzQ29tcG9uZW50IH0gZnJvbSAnLi9teWNvbXBzLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBNeXRlc3Rjb21wQ29tcG9uZW50IH0gZnJvbSAnLi9teXRlc3Rjb21wL215dGVzdGNvbXAuY29tcG9uZW50JztcbmltcG9ydCB7IE1vY2tVdGlsTW9kdWxlIH0gZnJvbSAnLi9tb2NrdXRpbC9tb2NrLXV0aWwubW9kdWxlJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIE1vY2tVdGlsTW9kdWxlXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW015Y29tcHNDb21wb25lbnQsIE15dGVzdGNvbXBDb21wb25lbnRdLFxuICBleHBvcnRzOiBbTXljb21wc0NvbXBvbmVudCwgTXl0ZXN0Y29tcENvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgTXljb21wc01vZHVsZSB7IH1cbiJdLCJuYW1lcyI6WyJ0c2xpYl8xLl9fZXh0ZW5kcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUNBOzs7QUFBQTtJQUVFO1FBQ0UsSUFBSSxDQUFDLEdBQUcsR0FBRztZQUNULElBQUk7Ozs7c0JBQUMsR0FBRztnQkFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ2xCO1NBQ0YsQ0FBQztRQUNGLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxVQUFVLENBQUMsQ0FBQztLQUM5RDttQkFWSDtJQVdDOzs7Ozs7cUJDSlksVUFBVSxHQUFHLElBQUksY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDOztJQUdwQkEsbUNBQVE7SUFFM0MseUJBQXdDLFFBQWU7UUFBdkQsWUFDRSxpQkFBTyxTQUNSO1FBRnVDLGNBQVEsR0FBUixRQUFRLENBQU87O0tBRXREOzs7OztJQUdELHdDQUFjOzs7O0lBQWQsVUFBZSxHQUFxQjtRQUFwQyxpQkFpQkM7UUFoQkMsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBQyxJQUFlOztZQUU5QyxJQUNFLEdBQUcsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLElBQUk7Z0JBQ3hCLEdBQUcsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7Z0JBQzFCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQztnQkFDNUIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDO2dCQUMzQixLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxHQUFHLENBQzFCLEVBQUU7Z0JBQ0EsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO29CQUNiLEtBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDcEQ7Z0JBQ0QsT0FBTyxJQUFJLENBQUM7YUFDYjtZQUNELE9BQU8sS0FBSyxDQUFDO1NBQ2QsQ0FBQyxDQUFDO0tBQ0o7Ozs7OztJQUVELDRDQUFrQjs7Ozs7SUFBbEIsVUFBbUIsR0FBcUIsRUFBRSxRQUFlO1FBQXpELGlCQXFCQztRQXBCQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2IsT0FBTyxJQUFJLENBQUM7U0FDYjtRQUVELE9BQU8sUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBQyxJQUFlOztZQUV6QyxJQUNFLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQztnQkFDM0IsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDO2dCQUN4QixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxHQUFHLENBQUM7Z0JBQzVCLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQztnQkFDM0IsS0FBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUMxQixFQUFFO2dCQUNBLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDYixLQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ3BEO2dCQUNELE9BQU8sSUFBSSxDQUFDO2FBQ2I7WUFDRCxPQUFPLEtBQUssQ0FBQztTQUNkLENBQUMsQ0FBQztLQUNKOzs7Ozs7SUFFTyxxQ0FBVzs7Ozs7Y0FBQyxJQUFlLEVBQUUsR0FBcUI7UUFDeEQsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ2IsT0FBTyxHQUFHLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDakM7UUFDRCxPQUFPLElBQUksQ0FBQzs7Ozs7OztJQUdOLGtDQUFROzs7OztjQUFDLElBQWUsRUFBRSxHQUFxQjtRQUNyRCxJQUFJLElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDWixPQUFPLEdBQUcsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNuQztRQUNELE9BQU8sSUFBSSxDQUFDOzs7Ozs7O0lBR04scUNBQVc7Ozs7O2NBQUMsSUFBZSxFQUFFLEdBQXFCO1FBQ3hELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNmLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQ3RCLFVBQUMsS0FBYSxJQUFLLE9BQUEsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssS0FBSyxDQUFDLEtBQUssS0FBSyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFBLENBQzFILENBQUM7U0FDSDtRQUNELE9BQU8sSUFBSSxDQUFDOzs7Ozs7O0lBRU4sc0NBQVk7Ozs7O2NBQUMsSUFBZSxFQUFFLEdBQXFCO1FBQ3pELElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN2QixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLFVBQUMsTUFBZSxJQUFLLE9BQUEsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLE1BQU0sQ0FBQyxLQUFLLEdBQUEsQ0FBQyxDQUFDO1NBQ3JHO1FBQ0QsT0FBTyxJQUFJLENBQUM7Ozs7Ozs7SUFHTixtQ0FBUzs7Ozs7Y0FBQyxJQUFlLEVBQUUsR0FBcUI7UUFDdEQsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ2IscUJBQU0sR0FBRyxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5QyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFJLElBQUksQ0FBQyxJQUFJLHNCQUFpQixHQUFHLGtCQUFhLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBRyxDQUFDLENBQUM7O1lBRXhGLE9BQU8sR0FBRyxDQUFDO1NBQ1o7UUFDRCxPQUFPLElBQUksQ0FBQzs7Ozs7O0lBR2QseUNBQWU7Ozs7SUFBZixVQUFnQixJQUFlO1FBQzdCLHFCQUFNLElBQUksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0MsSUFBSSxJQUFJLFlBQVksaUJBQWlCLEVBQUU7WUFDckMsT0FBTyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDekI7YUFBTTtZQUNMLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2pCO0tBQ0Y7Ozs7O0lBRUQsNENBQWtCOzs7O0lBQWxCLFVBQW1CLElBQWU7UUFDaEMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGFBQWEsR0FBRyxHQUFHLENBQUM7U0FDMUI7UUFDRCxxQkFBSSxPQUFPLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQztRQUNoQyxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDeEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsVUFBQSxNQUFNLElBQUksUUFBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBQyxDQUFDLENBQUM7U0FDOUY7UUFDRCxJQUFJLElBQUksQ0FBQyxhQUFhLEtBQUssR0FBRyxFQUFFO1lBQzlCLE9BQU8sSUFBSSxpQkFBaUIsQ0FBQztnQkFDM0IsTUFBTSxFQUFFLElBQUksQ0FBQyxhQUFhO2dCQUMxQixPQUFPLEVBQUUsT0FBTztnQkFDaEIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO2dCQUNqQixVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVU7YUFDNUIsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxRQUNFLElBQUksWUFBWSxDQUFDO1lBQ2YsTUFBTSxFQUFFLElBQUksQ0FBQyxhQUFhO1lBQzFCLElBQUksRUFBRSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSTtZQUMxQyxPQUFPLEVBQUUsT0FBTztTQUNqQixDQUFDLEVBQ0Y7S0FDSDs7Z0JBekhGLFVBQVU7Ozs7Z0RBR0ksTUFBTSxTQUFDLFVBQVU7OzBCQVpoQztFQVVxQyxRQUFROzs7Ozs7QUEySDdDLHNCQUFzQixDQUFDLEVBQUUsQ0FBQzs7SUFFeEIsSUFBSSxDQUFDLFlBQVksUUFBUSxFQUFFO1FBQ3pCLElBQUksQ0FBQyxZQUFZLFFBQVEsRUFBRTtZQUN6QixPQUFPLENBQUMsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDdEM7UUFDRCxPQUFPLEtBQUssQ0FBQztLQUNkO0lBQ0QsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxTQUFTLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssU0FBUyxFQUFFO1FBQ2xFLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUNoQjtJQUNELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDLE9BQU8sRUFBRSxFQUFFO1FBQzFDLE9BQU8sSUFBSSxDQUFDO0tBQ2I7O0lBR0QsSUFBSSxDQUFDLFlBQVksSUFBSSxFQUFFO1FBQ3JCLE9BQU8sS0FBSyxDQUFDO0tBQ2Q7SUFDRCxJQUFJLENBQUMsWUFBWSxJQUFJLEVBQUU7UUFDckIsT0FBTyxLQUFLLENBQUM7S0FDZDs7SUFHRCxJQUFJLEVBQUUsQ0FBQyxZQUFZLE1BQU0sQ0FBQyxFQUFFO1FBQzFCLE9BQU8sS0FBSyxDQUFDO0tBQ2Q7SUFDRCxJQUFJLEVBQUUsQ0FBQyxZQUFZLE1BQU0sQ0FBQyxFQUFFO1FBQzFCLE9BQU8sS0FBSyxDQUFDO0tBQ2Q7SUFFRCxxQkFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN6QixPQUFPLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBUyxDQUFDO1FBQ3ZCLE9BQU8sWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztLQUNqQyxDQUFDLENBQUM7OztDQUlKOzs7Ozs7QUMzS0Q7SUFPRTtLQUFpQjs7Z0JBTGxCLFVBQVUsU0FBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkI7Ozs7O3lCQUpEOzs7Ozs7O0FDQUE7SUFhRTtLQUFpQjs7OztJQUVqQixtQ0FBUTs7O0lBQVI7S0FDQzs7Z0JBZEYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxZQUFZO29CQUN0QixRQUFRLEVBQUUscURBSVQ7b0JBQ0UsTUFBTSxFQUFFLENBQUMsbUJBQW1CLENBQUM7aUJBQ2pDOzs7OzJCQVZEOzs7Ozs7O0FDQUE7SUFrQkU7S0FBaUI7Ozs7SUFFakIsc0NBQVE7OztJQUFSO0tBQ0M7O2dCQW5CRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGVBQWU7b0JBQ3pCLFFBQVEsRUFBRSxrRkFTWDtvQkFDQyxNQUFNLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQztpQkFDbkM7Ozs7OEJBZkQ7Ozs7Ozs7O0lDaUJ3Q0Esc0NBQVE7SUFDOUM7ZUFDRSxpQkFBTztLQUNSOzs7Ozs7SUFFRCxzQ0FBUzs7Ozs7SUFBVCxVQUFVLEdBQXFCLEVBQUUsSUFBaUI7UUFBbEQsaUJBNkJPO1FBNUJBLHFCQUFNLFVBQVUsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO1lBQzFCLE9BQU8sRUFBRSxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsZUFBZSxDQUFDO1NBQ3ZELENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7UUFDdkMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQzthQUM3QixJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUMsS0FBcUI7WUFDOUIsSUFBSSxLQUFLLFlBQVksWUFBWSxFQUFFO2dCQUNqQyxxQkFBTSxDQUFDLHFCQUFHLEtBQTBCLENBQUEsQ0FBQztnQkFDckMsS0FBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDdkMsS0FBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUM3QyxLQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2FBQ2pFO1NBQ0QsRUFBRSxVQUFDLEtBQXdCO1lBQzFCLEtBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDeEUsS0FBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUM5RCxLQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDdEQsQ0FBQyxFQUNBLFVBQVUsQ0FBQyxVQUFDLEdBQUc7WUFDZixLQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxHQUFHLENBQUMsQ0FBQztZQUMxQyxPQUFPLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUN4QixDQUFDLENBQ0gsQ0FBQzs7Ozs7S0FPRDs7Z0JBbkNSLFVBQVU7Ozs7NkJBaEJYO0VBaUJ3QyxRQUFROzs7Ozs7O0lDRVJBLHNDQUFRO0lBQzlDLDRCQUFvQixPQUF3QjtRQUE1QyxZQUNFLGlCQUFPLFNBQ1I7UUFGbUIsYUFBTyxHQUFQLE9BQU8sQ0FBaUI7O0tBRTNDOzs7Ozs7SUFFRCxzQ0FBUzs7Ozs7SUFBVCxVQUFVLEdBQXFCLEVBQUUsSUFBaUI7UUFBbEQsaUJBcUJDO1FBcEJBLHFCQUFNLElBQUksR0FBYyxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUV6RCxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDO1FBRXZELE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FDakIsUUFBUSxDQUFDO1lBQ1AsSUFBSSxJQUFJLEVBQUU7Z0JBQ1IsT0FBTyxLQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUMzQztZQUNELHFCQUFNLFVBQVUsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO2dCQUMzQixPQUFPLEVBQUUsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLGVBQWUsQ0FBQzthQUN2RCxDQUFDLENBQUM7WUFDSCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDaEMsQ0FBQyxFQUNBLFdBQVcsRUFBRSxFQUNiLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFDVixhQUFhLEVBQUUsQ0FDbEIsQ0FBQztLQUdIOztnQkEzQkYsVUFBVTs7OztnQkFQRixlQUFlOzs2QkFYeEI7RUFtQndDLFFBQVE7Ozs7OztBQ25CaEQ7SUF5QkU7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDLENBQUE7S0FDcEM7O2dCQXBCRixRQUFRLFNBQUM7b0JBQ1IsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLFNBQVMsRUFBRTt3QkFDVCxlQUFlO3dCQUNmOzRCQUNFLE9BQU8sRUFBRSxpQkFBaUI7NEJBQzFCLFFBQVEsRUFBRSxrQkFBa0I7NEJBQzVCLEtBQUssRUFBRSxJQUFJO3lCQUNaO3dCQUNEOzRCQUNFLE9BQU8sRUFBRSxpQkFBaUI7NEJBQzFCLFFBQVEsRUFBRSxrQkFBa0I7NEJBQzVCLEtBQUssRUFBRSxJQUFJO3lCQUNaO3FCQUNGO2lCQUNGOzs7O3lCQXZCRDs7Ozs7OztBQ0FBOzs7O2dCQUtDLFFBQVEsU0FBQztvQkFDUixPQUFPLEVBQUU7d0JBQ1AsY0FBYztxQkFDZjtvQkFDRCxZQUFZLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxtQkFBbUIsQ0FBQztvQkFDckQsT0FBTyxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsbUJBQW1CLENBQUM7aUJBQ2pEOzt3QkFYRDs7Ozs7Ozs7Ozs7Ozs7OyJ9