/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BaseUtil } from './base-util';
export class AppCoreInterceptor extends BaseUtil {
    constructor() {
        super();
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        const /** @type {?} */ changedReq = req.clone({
            headers: req.headers.set('My-Header', 'MyHeaderValue')
        });
        this.log.info('pass intercept coreee');
        return next.handle(changedReq)
            .pipe(tap((event) => {
            if (event instanceof HttpResponse) {
                const /** @type {?} */ r = /** @type {?} */ (event);
                this.log.info('url resp**===' + r.url);
                this.log.info('status resp**===' + r.status);
                this.log.info('headers resp**===' + JSON.stringify(r.headers));
            }
        }, (error) => {
            this.log.info('headers error resp**==' + JSON.stringify(error.headers));
            this.log.info('error resp**==' + JSON.stringify(error.error));
            this.log.info('error core status***' + error.status);
        }), catchError((err) => {
            this.log.info('error cath core***' + err);
            return throwError(err);
        }));
        /*   catch((err)=>{
                    console.log('error cath core***'+err);
                    return Observable.throw(err);
                  }); */
    }
}
AppCoreInterceptor.decorators = [
    { type: Injectable },
];
/** @nocollapse */
AppCoreInterceptor.ctorParameters = () => [];

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvcmUuaW50ZXJjZXB0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teWNvbXBzLyIsInNvdXJjZXMiOlsibGliL21vY2t1dGlsL2FwcC5jb3JlLmludGVyY2VwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFDQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFLTCxZQUFZLEVBQ2IsTUFBTSxzQkFBc0IsQ0FBQztBQUM5QixPQUFPLEVBQW9CLFVBQVUsRUFBQyxNQUFNLE1BQU0sQ0FBQztBQUVuRCxPQUFPLEVBQUUsVUFBVSxFQUFPLEdBQUcsRUFBeUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUU3RixPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBSXZDLE1BQU0seUJBQTBCLFNBQVEsUUFBUTtJQUM5QztRQUNFLEtBQUssRUFBRSxDQUFDO0tBQ1Q7Ozs7OztJQUVELFNBQVMsQ0FBQyxHQUFxQixFQUFFLElBQWlCO1FBQzNDLHVCQUFNLFVBQVUsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO1lBQzFCLE9BQU8sRUFBRSxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsZUFBZSxDQUFDO1NBQ3ZELENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7UUFDdkMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDO2FBQzdCLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFxQixFQUFFLEVBQUU7WUFDbEMsRUFBRSxDQUFDLENBQUMsS0FBSyxZQUFZLFlBQVksQ0FBQyxDQUFDLENBQUM7Z0JBQ2xDLHVCQUFNLENBQUMscUJBQUcsS0FBMEIsQ0FBQSxDQUFDO2dCQUNyQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN2QyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzdDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7YUFDakU7U0FDRCxFQUFFLENBQUMsS0FBd0IsRUFBRSxFQUFFO1lBQzlCLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDeEUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUM5RCxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDdEQsQ0FBQyxFQUNBLFVBQVUsQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO1lBQ25CLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQzFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDeEIsQ0FBQyxDQUNILENBQUM7Ozs7O0tBT0Q7OztZQW5DUixVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSHR0cEVycm9yUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtcclxuICBIdHRwRXZlbnQsXHJcbiAgSHR0cEludGVyY2VwdG9yLFxyXG4gIEh0dHBIYW5kbGVyLFxyXG4gIEh0dHBSZXF1ZXN0LFxyXG4gIEh0dHBSZXNwb25zZVxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSAsICBvZiwgdGhyb3dFcnJvcn0gZnJvbSAncnhqcyc7XHJcblxyXG5pbXBvcnQgeyBjYXRjaEVycm9yLCBtYXAsIHRhcCwgcmVmQ291bnQsIHB1Ymxpc2hSZXBsYXksIGRlbGF5LCByZXRyeSB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgTW9ja0RhdGFTZXJ2aWNlIH0gZnJvbSAnLi9tb2NrLWRhdGEuc2VydmljZSc7XHJcbmltcG9ydCB7IEJhc2VVdGlsIH0gZnJvbSAnLi9iYXNlLXV0aWwnO1xyXG5cclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEFwcENvcmVJbnRlcmNlcHRvciBleHRlbmRzIEJhc2VVdGlsIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHN1cGVyKCk7XHJcbiAgfVxyXG5cclxuICBpbnRlcmNlcHQocmVxOiBIdHRwUmVxdWVzdDxhbnk+LCBuZXh0OiBIdHRwSGFuZGxlcik6IE9ic2VydmFibGU8SHR0cEV2ZW50PGFueT4+IHtcclxuICAgICAgICAgY29uc3QgY2hhbmdlZFJlcSA9IHJlcS5jbG9uZSh7XHJcbiAgICAgICAgICAgIGhlYWRlcnM6IHJlcS5oZWFkZXJzLnNldCgnTXktSGVhZGVyJywgJ015SGVhZGVyVmFsdWUnKVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICB0aGlzLmxvZy5pbmZvKCdwYXNzIGludGVyY2VwdCBjb3JlZWUnKTtcclxuICAgICAgICAgIHJldHVybiBuZXh0LmhhbmRsZShjaGFuZ2VkUmVxKVxyXG4gICAgICAgICAgLnBpcGUodGFwKChldmVudDogSHR0cEV2ZW50PGFueT4pID0+IHtcclxuICAgICAgICAgICAgaWYgKGV2ZW50IGluc3RhbmNlb2YgSHR0cFJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgciA9IGV2ZW50IGFzIEh0dHBSZXNwb25zZTxhbnk+O1xyXG4gICAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ3VybCByZXNwKio9PT0nICsgci51cmwpO1xyXG4gICAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ3N0YXR1cyByZXNwKio9PT0nICsgci5zdGF0dXMpO1xyXG4gICAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ2hlYWRlcnMgcmVzcCoqPT09JyArIEpTT04uc3RyaW5naWZ5KHIuaGVhZGVycykpO1xyXG4gICAgICAgICAgIH1cclxuICAgICAgICAgIH0sIChlcnJvcjogSHR0cEVycm9yUmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5sb2cuaW5mbygnaGVhZGVycyBlcnJvciByZXNwKio9PScgKyBKU09OLnN0cmluZ2lmeShlcnJvci5oZWFkZXJzKSk7XHJcbiAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ2Vycm9yIHJlc3AqKj09JyArIEpTT04uc3RyaW5naWZ5KGVycm9yLmVycm9yKSk7XHJcbiAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ2Vycm9yIGNvcmUgc3RhdHVzKioqJyArIGVycm9yLnN0YXR1cyk7XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICAgLCBjYXRjaEVycm9yKChlcnIpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5sb2cuaW5mbygnZXJyb3IgY2F0aCBjb3JlKioqJyArIGVycik7XHJcbiAgICAgICAgICAgIHJldHVybiB0aHJvd0Vycm9yKGVycik7XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIC8qICAgY2F0Y2goKGVycik9PntcclxuICAgICAgICAgICAgY29uc29sZS5sb2coJ2Vycm9yIGNhdGggY29yZSoqKicrZXJyKTtcclxuICAgICAgICAgICAgcmV0dXJuIE9ic2VydmFibGUudGhyb3coZXJyKTtcclxuICAgICAgICAgIH0pOyAqL1xyXG5cclxuICAgICAgICB9XHJcblxyXG59XHJcbiJdfQ==