/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { MockDataService } from './mock-data.service';
import { of } from 'rxjs';
import { BaseUtil } from './base-util';
var AppInterceptorMock = /** @class */ (function (_super) {
    tslib_1.__extends(AppInterceptorMock, _super);
    function AppInterceptorMock(mockSrv) {
        var _this = _super.call(this) || this;
        _this.mockSrv = mockSrv;
        return _this;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    AppInterceptorMock.prototype.intercept = /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    function (req, next) {
        var _this = this;
        var /** @type {?} */ mock = this.mockSrv.getRequestMock(req);
        this.log.info('enter app interceptor MOCK***********');
        return of(null).pipe(mergeMap(function () {
            if (mock) {
                return _this.mockSrv.getResponseMock(mock);
            }
            var /** @type {?} */ changedReq = req.clone({
                headers: req.headers.set('My-Header', 'MyHeaderValue')
            });
            return next.handle(changedReq);
        }), materialize(), delay(500), dematerialize());
    };
    AppInterceptorMock.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AppInterceptorMock.ctorParameters = function () { return [
        { type: MockDataService }
    ]; };
    return AppInterceptorMock;
}(BaseUtil));
export { AppInterceptorMock };
function AppInterceptorMock_tsickle_Closure_declarations() {
    /** @type {?} */
    AppInterceptorMock.prototype.mockSrv;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXljb21wcy8iLCJzb3VyY2VzIjpbImxpYi9tb2NrdXRpbC9hcHAuaW50ZXJjZXB0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFDQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBUzNDLE9BQU8sRUFBaUQsS0FBSyxFQUFTLFFBQVEsRUFBRSxXQUFXLEVBQUUsYUFBYSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDbkksT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBR3RELE9BQU8sRUFBYyxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDdEMsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGFBQWEsQ0FBQzs7SUFJQyw4Q0FBUTtJQUM5Qyw0QkFBb0IsT0FBd0I7UUFBNUMsWUFDRSxpQkFBTyxTQUNSO1FBRm1CLGFBQU8sR0FBUCxPQUFPLENBQWlCOztLQUUzQzs7Ozs7O0lBRUQsc0NBQVM7Ozs7O0lBQVQsVUFBVSxHQUFxQixFQUFFLElBQWlCO1FBQWxELGlCQXFCQztRQXBCQSxxQkFBTSxJQUFJLEdBQWMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFekQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsdUNBQXVDLENBQUMsQ0FBQztRQUV2RCxNQUFNLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FDakIsUUFBUSxDQUFDO1lBQ1AsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDVCxNQUFNLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDM0M7WUFDRCxxQkFBTSxVQUFVLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQztnQkFDM0IsT0FBTyxFQUFFLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxlQUFlLENBQUM7YUFDdkQsQ0FBQyxDQUFDO1lBQ0gsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDaEMsQ0FBQyxFQUNBLFdBQVcsRUFBRSxFQUNiLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFDVixhQUFhLEVBQUUsQ0FDbEIsQ0FBQztLQUdIOztnQkEzQkYsVUFBVTs7OztnQkFQRixlQUFlOzs2QkFYeEI7RUFtQndDLFFBQVE7U0FBbkMsa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSHR0cEVycm9yUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtcclxuICBIdHRwRXZlbnQsXHJcbiAgSHR0cEludGVyY2VwdG9yLFxyXG4gIEh0dHBIYW5kbGVyLFxyXG4gIEh0dHBSZXF1ZXN0LFxyXG4gIEh0dHBSZXNwb25zZVxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuXHJcbmltcG9ydCB7IGNhdGNoRXJyb3IsIG1hcCwgdGFwLCByZWZDb3VudCwgcHVibGlzaFJlcGxheSwgZGVsYXksIHJldHJ5LCBtZXJnZU1hcCwgbWF0ZXJpYWxpemUsIGRlbWF0ZXJpYWxpemUgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IE1vY2tEYXRhU2VydmljZSB9IGZyb20gJy4vbW9jay1kYXRhLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBJTW9ja0RhdGEgfSBmcm9tICcuL2ltb2NrLWRhdGEubW9kZWwnO1xyXG5cclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgb2YgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQmFzZVV0aWwgfSBmcm9tICcuL2Jhc2UtdXRpbCc7XHJcblxyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQXBwSW50ZXJjZXB0b3JNb2NrIGV4dGVuZHMgQmFzZVV0aWwgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbW9ja1NydjogTW9ja0RhdGFTZXJ2aWNlKSB7XHJcbiAgICBzdXBlcigpO1xyXG4gIH1cclxuXHJcbiAgaW50ZXJjZXB0KHJlcTogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XHJcbiAgIGNvbnN0IG1vY2s6IElNb2NrRGF0YSA9IHRoaXMubW9ja1Nydi5nZXRSZXF1ZXN0TW9jayhyZXEpO1xyXG5cclxuICAgdGhpcy5sb2cuaW5mbygnZW50ZXIgYXBwIGludGVyY2VwdG9yIE1PQ0sqKioqKioqKioqKicpO1xyXG5cclxuICAgcmV0dXJuIG9mKG51bGwpLnBpcGUoXHJcbiAgICAgIG1lcmdlTWFwKCgpID0+IHtcclxuICAgICAgICBpZiAobW9jaykge1xyXG4gICAgICAgICAgcmV0dXJuIHRoaXMubW9ja1Nydi5nZXRSZXNwb25zZU1vY2sobW9jayk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGNoYW5nZWRSZXEgPSByZXEuY2xvbmUoe1xyXG4gICAgICAgICAgaGVhZGVyczogcmVxLmhlYWRlcnMuc2V0KCdNeS1IZWFkZXInLCAnTXlIZWFkZXJWYWx1ZScpXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKGNoYW5nZWRSZXEpO1xyXG4gICAgICB9KVxyXG4gICAgICAsIG1hdGVyaWFsaXplKClcclxuICAgICAgLCBkZWxheSg1MDApXHJcbiAgICAgICwgZGVtYXRlcmlhbGl6ZSgpXHJcbiAgICApO1xyXG5cclxuXHJcbiAgfVxyXG59XHJcbiJdfQ==