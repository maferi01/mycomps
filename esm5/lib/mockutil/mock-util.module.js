/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AppCoreInterceptor } from './app.core.interceptor';
import { AppInterceptorMock } from './app.interceptor';
import { MockDataService } from './mock-data.service';
var MockUtilModule = /** @class */ (function () {
    function MockUtilModule() {
        console.log('enter mockkkk module');
    }
    MockUtilModule.decorators = [
        { type: NgModule, args: [{
                    imports: [],
                    declarations: [],
                    providers: [
                        MockDataService,
                        {
                            provide: HTTP_INTERCEPTORS,
                            useClass: AppCoreInterceptor,
                            multi: true
                        },
                        {
                            provide: HTTP_INTERCEPTORS,
                            useClass: AppInterceptorMock,
                            multi: true
                        }
                    ]
                },] },
    ];
    /** @nocollapse */
    MockUtilModule.ctorParameters = function () { return []; };
    return MockUtilModule;
}());
export { MockUtilModule };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9jay11dGlsLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215Y29tcHMvIiwic291cmNlcyI6WyJsaWIvbW9ja3V0aWwvbW9jay11dGlsLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDekQsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV6QyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7O0lBb0JwRDtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQTtLQUNwQzs7Z0JBcEJGLFFBQVEsU0FBQztvQkFDUixPQUFPLEVBQUUsRUFBRTtvQkFDWCxZQUFZLEVBQUUsRUFBRTtvQkFDaEIsU0FBUyxFQUFFO3dCQUNULGVBQWU7d0JBQ2Y7NEJBQ0UsT0FBTyxFQUFFLGlCQUFpQjs0QkFDMUIsUUFBUSxFQUFFLGtCQUFrQjs0QkFDNUIsS0FBSyxFQUFFLElBQUk7eUJBQ1o7d0JBQ0Q7NEJBQ0UsT0FBTyxFQUFFLGlCQUFpQjs0QkFDMUIsUUFBUSxFQUFFLGtCQUFrQjs0QkFDNUIsS0FBSyxFQUFFLElBQUk7eUJBQ1o7cUJBQ0Y7aUJBQ0Y7Ozs7eUJBdkJEOztTQXdCYSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSFRUUF9JTlRFUkNFUFRPUlMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBBcHBDb3JlSW50ZXJjZXB0b3IgfSBmcm9tICcuL2FwcC5jb3JlLmludGVyY2VwdG9yJztcclxuaW1wb3J0IHsgQXBwSW50ZXJjZXB0b3JNb2NrIH0gZnJvbSAnLi9hcHAuaW50ZXJjZXB0b3InO1xyXG5pbXBvcnQgeyBNb2NrRGF0YVNlcnZpY2UgfSBmcm9tICcuL21vY2stZGF0YS5zZXJ2aWNlJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgaW1wb3J0czogW10sXHJcbiAgZGVjbGFyYXRpb25zOiBbXSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIE1vY2tEYXRhU2VydmljZSxcclxuICAgIHtcclxuICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXHJcbiAgICAgIHVzZUNsYXNzOiBBcHBDb3JlSW50ZXJjZXB0b3IsXHJcbiAgICAgIG11bHRpOiB0cnVlXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBIVFRQX0lOVEVSQ0VQVE9SUyxcclxuICAgICAgdXNlQ2xhc3M6IEFwcEludGVyY2VwdG9yTW9jayxcclxuICAgICAgbXVsdGk6IHRydWVcclxuICAgIH1cclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNb2NrVXRpbE1vZHVsZSB7XHJcbiAgY29uc3RydWN0b3IoKXtcclxuICAgIGNvbnNvbGUubG9nKCdlbnRlciBtb2Nra2trIG1vZHVsZScpXHJcbiAgfVxyXG59XHJcbiJdfQ==