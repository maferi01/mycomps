/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/*
 * Public API Surface of mycomps****
 */
export { TOKEN_MOCK, MockDataService } from './lib/mockutil/mock-data.service';
export { MycompsService } from './lib/mycomps.service';
export { MycompsComponent } from './lib/mycomps.component';
export { MycompsModule } from './lib/mycomps.module';

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljX2FwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215Y29tcHMvIiwic291cmNlcyI6WyJwdWJsaWNfYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFJQSw0Q0FBYyxrQ0FBa0MsQ0FBQztBQUNqRCwrQkFBYyx1QkFBdUIsQ0FBQztBQUN0QyxpQ0FBYyx5QkFBeUIsQ0FBQztBQUN4Qyw4QkFBYyxzQkFBc0IsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbiAqIFB1YmxpYyBBUEkgU3VyZmFjZSBvZiBteWNvbXBzKioqKlxyXG4gKi9cclxuZXhwb3J0ICogZnJvbSAnLi9saWIvbW9ja3V0aWwvaW1vY2stZGF0YS5tb2RlbCc7XHJcbmV4cG9ydCAqIGZyb20gJy4vbGliL21vY2t1dGlsL21vY2stZGF0YS5zZXJ2aWNlJztcclxuZXhwb3J0ICogZnJvbSAnLi9saWIvbXljb21wcy5zZXJ2aWNlJztcclxuZXhwb3J0ICogZnJvbSAnLi9saWIvbXljb21wcy5jb21wb25lbnQnO1xyXG5leHBvcnQgKiBmcm9tICcuL2xpYi9teWNvbXBzLm1vZHVsZSc7XHJcbiJdfQ==