export interface IMock {
    mocks: IMockData[];
}
export interface IMockData {
    name?: string;
    type?: string;
    url?: string;
    body?: any;
    params?: IParam[];
    headersRequest?: IHeader[];
    response?: any;
    responseFile?: string;
    resposeStatus?: number;
    headersResponse?: IHeader[];
    statusText?: string;
    error?: any;
}
export interface IParam {
    key: string;
    value: string;
}
export interface IHeader {
    key: string;
    value: string;
}
