(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/common/http'), require('@angular/core'), require('rxjs'), require('rxjs/operators')) :
    typeof define === 'function' && define.amd ? define('mycomps', ['exports', '@angular/common/http', '@angular/core', 'rxjs', 'rxjs/operators'], factory) :
    (factory((global.mycomps = {}),global.ng.common.http,global.ng.core,global.rxjs,global.rxjs.operators));
}(this, (function (exports,http,i0,rxjs,operators) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b)
            if (b.hasOwnProperty(p))
                d[p] = b[p]; };
    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @abstract
     */
    var /**
     * @abstract
     */ BaseUtil = (function () {
        function BaseUtil() {
            this.log = {
                info: /**
                 * @param {?} msg
                 * @return {?}
                 */ function (msg) {
                    console.log(msg);
                }
            };
            this.log.info('Class ' + this.constructor.name + ' created');
        }
        return BaseUtil;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var /** @type {?} */ TOKEN_MOCK = new i0.InjectionToken('mockdata');
    var MockDataService = (function (_super) {
        __extends(MockDataService, _super);
        function MockDataService(mockData) {
            var _this = _super.call(this) || this;
            _this.mockData = mockData;
            return _this;
        }
        /**
         * @param {?} req
         * @return {?}
         */
        MockDataService.prototype.getRequestMock = /**
         * @param {?} req
         * @return {?}
         */
            function (req) {
                var _this = this;
                return this.mockData.mocks.find(function (data) {
                    // check if the method and url and params and match params
                    if (req.method === data.type &&
                        req.url.includes(data.url) &&
                        _this.matchHeaders(data, req) &&
                        _this.matchParams(data, req) &&
                        _this.matchBody(data, req)) {
                        if (data.name) {
                            _this.log.info('---mock data matched ' + data.name);
                        }
                        return true;
                    }
                    return false;
                });
            };
        /**
         * @param {?} req
         * @param {?} mockData
         * @return {?}
         */
        MockDataService.prototype.getRequestMockData = /**
         * @param {?} req
         * @param {?} mockData
         * @return {?}
         */
            function (req, mockData) {
                var _this = this;
                if (!mockData) {
                    return null;
                }
                return mockData.mocks.find(function (data) {
                    // check if the method and url and params and match params
                    if (_this.matchMethod(data, req) &&
                        _this.matchUrl(data, req) &&
                        _this.matchHeaders(data, req) &&
                        _this.matchParams(data, req) &&
                        _this.matchBody(data, req)) {
                        if (data.name) {
                            _this.log.info('---mock data matched ' + data.name);
                        }
                        return true;
                    }
                    return false;
                });
            };
        /**
         * @param {?} data
         * @param {?} req
         * @return {?}
         */
        MockDataService.prototype.matchMethod = /**
         * @param {?} data
         * @param {?} req
         * @return {?}
         */
            function (data, req) {
                if (data.type) {
                    return req.method === data.type;
                }
                return true;
            };
        /**
         * @param {?} data
         * @param {?} req
         * @return {?}
         */
        MockDataService.prototype.matchUrl = /**
         * @param {?} data
         * @param {?} req
         * @return {?}
         */
            function (data, req) {
                if (data.url) {
                    return req.url.includes(data.url);
                }
                return true;
            };
        /**
         * @param {?} data
         * @param {?} req
         * @return {?}
         */
        MockDataService.prototype.matchParams = /**
         * @param {?} data
         * @param {?} req
         * @return {?}
         */
            function (data, req) {
                if (data.params) {
                    return data.params.every(function (param) { return req.params[param.key] === param.value || (req.params.get && req.params.get(param.key) === param.value); });
                }
                return true;
            };
        /**
         * @param {?} data
         * @param {?} req
         * @return {?}
         */
        MockDataService.prototype.matchHeaders = /**
         * @param {?} data
         * @param {?} req
         * @return {?}
         */
            function (data, req) {
                if (data.headersRequest) {
                    return data.headersRequest.every(function (header) { return req.headers.get(header.key) === header.value; });
                }
                return true;
            };
        /**
         * @param {?} data
         * @param {?} req
         * @return {?}
         */
        MockDataService.prototype.matchBody = /**
         * @param {?} data
         * @param {?} req
         * @return {?}
         */
            function (data, req) {
                if (data.body) {
                    var /** @type {?} */ res = objectEquals(data.body, req.body);
                    this.log.info(" " + data.name + " -compare res=" + res + " body req " + JSON.stringify(req.body));
                    // return JSON.stringify(data.body)==JSON.stringify(req.body);
                    return res;
                }
                return true;
            };
        /**
         * @param {?} data
         * @return {?}
         */
        MockDataService.prototype.getResponseMock = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                var /** @type {?} */ resp = this.getResponseMockObj(data);
                if (resp instanceof http.HttpErrorResponse) {
                    return rxjs.throwError(resp);
                }
                else {
                    return rxjs.of(resp);
                }
            };
        /**
         * @param {?} data
         * @return {?}
         */
        MockDataService.prototype.getResponseMockObj = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                if (!data.resposeStatus) {
                    data.resposeStatus = 200;
                }
                var /** @type {?} */ headers = new http.HttpHeaders();
                if (data.headersResponse) {
                    data.headersResponse.forEach(function (header) { return (headers = headers.append(header.key, header.value)); });
                }
                if (data.resposeStatus !== 200) {
                    return new http.HttpErrorResponse({
                        status: data.resposeStatus,
                        headers: headers,
                        error: data.error,
                        statusText: data.statusText
                    });
                }
                return (new http.HttpResponse({
                    status: data.resposeStatus,
                    body: data.response ? data.response : null,
                    headers: headers
                }));
            };
        MockDataService.decorators = [
            { type: i0.Injectable },
        ];
        /** @nocollapse */
        MockDataService.ctorParameters = function () {
            return [
                { type: undefined, decorators: [{ type: i0.Inject, args: [TOKEN_MOCK,] }] }
            ];
        };
        return MockDataService;
    }(BaseUtil));
    /**
     * @param {?} x
     * @param {?} y
     * @return {?}
     */
    function objectEquals(x, y) {
        // if both are function
        if (x instanceof Function) {
            if (y instanceof Function) {
                return x.toString() === y.toString();
            }
            return false;
        }
        if (x === null || x === undefined || y === null || y === undefined) {
            return x === y;
        }
        if (x === y || x.valueOf() === y.valueOf()) {
            return true;
        }
        // if one of them is date, they must had equal valueOf
        if (x instanceof Date) {
            return false;
        }
        if (y instanceof Date) {
            return false;
        }
        // if they are not function or strictly equal, they both need to be Objects
        if (!(x instanceof Object)) {
            return false;
        }
        if (!(y instanceof Object)) {
            return false;
        }
        var /** @type {?} */ p = Object.keys(x);
        return p.every(function (i) {
            return objectEquals(x[i], y[i]);
        });
        /* return Object.keys(y).every(function (i) { return p.indexOf(i) !== -1; }) ?
                  p.every(function (i) { return objectEquals(x[i], y[i]); }) : false; */
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var MycompsService = (function () {
        function MycompsService() {
        }
        MycompsService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] },
        ];
        /** @nocollapse */
        MycompsService.ctorParameters = function () { return []; };
        /** @nocollapse */ MycompsService.ngInjectableDef = i0.defineInjectable({ factory: function MycompsService_Factory() { return new MycompsService(); }, token: MycompsService, providedIn: "root" });
        return MycompsService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var MycompsComponent = (function () {
        function MycompsComponent() {
        }
        /**
         * @return {?}
         */
        MycompsComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        MycompsComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'my-mycomps',
                        template: "\n    <p>\n      mycomps works artur!\n    </p>\n  ",
                        styles: ["p{background:red}"]
                    },] },
        ];
        /** @nocollapse */
        MycompsComponent.ctorParameters = function () { return []; };
        return MycompsComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var MytestcompComponent = (function () {
        function MytestcompComponent() {
        }
        /**
         * @return {?}
         */
        MytestcompComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        MytestcompComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'my-mytestcomp',
                        template: "\n<div>\n   <p>\n  mytestcomp works***** from library!\n</p>\n    \n</div>\n\n\n",
                        styles: ["div p{background:pink}"]
                    },] },
        ];
        /** @nocollapse */
        MytestcompComponent.ctorParameters = function () { return []; };
        return MytestcompComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var AppCoreInterceptor = (function (_super) {
        __extends(AppCoreInterceptor, _super);
        function AppCoreInterceptor() {
            return _super.call(this) || this;
        }
        /**
         * @param {?} req
         * @param {?} next
         * @return {?}
         */
        AppCoreInterceptor.prototype.intercept = /**
         * @param {?} req
         * @param {?} next
         * @return {?}
         */
            function (req, next) {
                var _this = this;
                var /** @type {?} */ changedReq = req.clone({
                    headers: req.headers.set('My-Header', 'MyHeaderValue')
                });
                this.log.info('pass intercept coreee');
                return next.handle(changedReq)
                    .pipe(operators.tap(function (event) {
                    if (event instanceof http.HttpResponse) {
                        var /** @type {?} */ r = (event);
                        _this.log.info('url resp**===' + r.url);
                        _this.log.info('status resp**===' + r.status);
                        _this.log.info('headers resp**===' + JSON.stringify(r.headers));
                    }
                }, function (error) {
                    _this.log.info('headers error resp**==' + JSON.stringify(error.headers));
                    _this.log.info('error resp**==' + JSON.stringify(error.error));
                    _this.log.info('error core status***' + error.status);
                }), operators.catchError(function (err) {
                    _this.log.info('error cath core***' + err);
                    return rxjs.throwError(err);
                }));
                /*   catch((err)=>{
                            console.log('error cath core***'+err);
                            return Observable.throw(err);
                          }); */
            };
        AppCoreInterceptor.decorators = [
            { type: i0.Injectable },
        ];
        /** @nocollapse */
        AppCoreInterceptor.ctorParameters = function () { return []; };
        return AppCoreInterceptor;
    }(BaseUtil));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var AppInterceptorMock = (function (_super) {
        __extends(AppInterceptorMock, _super);
        function AppInterceptorMock(mockSrv) {
            var _this = _super.call(this) || this;
            _this.mockSrv = mockSrv;
            return _this;
        }
        /**
         * @param {?} req
         * @param {?} next
         * @return {?}
         */
        AppInterceptorMock.prototype.intercept = /**
         * @param {?} req
         * @param {?} next
         * @return {?}
         */
            function (req, next) {
                var _this = this;
                var /** @type {?} */ mock = this.mockSrv.getRequestMock(req);
                this.log.info('enter app interceptor MOCK***********');
                return rxjs.of(null).pipe(operators.mergeMap(function () {
                    if (mock) {
                        return _this.mockSrv.getResponseMock(mock);
                    }
                    var /** @type {?} */ changedReq = req.clone({
                        headers: req.headers.set('My-Header', 'MyHeaderValue')
                    });
                    return next.handle(changedReq);
                }), operators.materialize(), operators.delay(500), operators.dematerialize());
            };
        AppInterceptorMock.decorators = [
            { type: i0.Injectable },
        ];
        /** @nocollapse */
        AppInterceptorMock.ctorParameters = function () {
            return [
                { type: MockDataService }
            ];
        };
        return AppInterceptorMock;
    }(BaseUtil));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var MockUtilModule = (function () {
        function MockUtilModule() {
            console.log('enter mockkkk module');
        }
        MockUtilModule.decorators = [
            { type: i0.NgModule, args: [{
                        imports: [],
                        declarations: [],
                        providers: [
                            MockDataService,
                            {
                                provide: http.HTTP_INTERCEPTORS,
                                useClass: AppCoreInterceptor,
                                multi: true
                            },
                            {
                                provide: http.HTTP_INTERCEPTORS,
                                useClass: AppInterceptorMock,
                                multi: true
                            }
                        ]
                    },] },
        ];
        /** @nocollapse */
        MockUtilModule.ctorParameters = function () { return []; };
        return MockUtilModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var MycompsModule = (function () {
        function MycompsModule() {
        }
        MycompsModule.decorators = [
            { type: i0.NgModule, args: [{
                        imports: [
                            MockUtilModule
                        ],
                        declarations: [MycompsComponent, MytestcompComponent],
                        exports: [MycompsComponent, MytestcompComponent]
                    },] },
        ];
        return MycompsModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    exports.TOKEN_MOCK = TOKEN_MOCK;
    exports.MockDataService = MockDataService;
    exports.MycompsService = MycompsService;
    exports.MycompsComponent = MycompsComponent;
    exports.MycompsModule = MycompsModule;
    exports.ɵc = AppCoreInterceptor;
    exports.ɵd = AppInterceptorMock;
    exports.ɵa = BaseUtil;
    exports.ɵb = MockUtilModule;
    exports.ɵe = MytestcompComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXljb21wcy51bWQuanMubWFwIiwic291cmNlcyI6W251bGwsIm5nOi8vbXljb21wcy9saWIvbW9ja3V0aWwvYmFzZS11dGlsLnRzIiwibmc6Ly9teWNvbXBzL2xpYi9tb2NrdXRpbC9tb2NrLWRhdGEuc2VydmljZS50cyIsIm5nOi8vbXljb21wcy9saWIvbXljb21wcy5zZXJ2aWNlLnRzIiwibmc6Ly9teWNvbXBzL2xpYi9teWNvbXBzLmNvbXBvbmVudC50cyIsIm5nOi8vbXljb21wcy9saWIvbXl0ZXN0Y29tcC9teXRlc3Rjb21wLmNvbXBvbmVudC50cyIsIm5nOi8vbXljb21wcy9saWIvbW9ja3V0aWwvYXBwLmNvcmUuaW50ZXJjZXB0b3IudHMiLCJuZzovL215Y29tcHMvbGliL21vY2t1dGlsL2FwcC5pbnRlcmNlcHRvci50cyIsIm5nOi8vbXljb21wcy9saWIvbW9ja3V0aWwvbW9jay11dGlsLm1vZHVsZS50cyIsIm5nOi8vbXljb21wcy9saWIvbXljb21wcy5tb2R1bGUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiLyohICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbkNvcHlyaWdodCAoYykgTWljcm9zb2Z0IENvcnBvcmF0aW9uLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2VcclxudGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGVcclxuTGljZW5zZSBhdCBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblRISVMgQ09ERSBJUyBQUk9WSURFRCBPTiBBTiAqQVMgSVMqIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcclxuS0lORCwgRUlUSEVSIEVYUFJFU1MgT1IgSU1QTElFRCwgSU5DTFVESU5HIFdJVEhPVVQgTElNSVRBVElPTiBBTlkgSU1QTElFRFxyXG5XQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgVElUTEUsIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLFxyXG5NRVJDSEFOVEFCTElUWSBPUiBOT04tSU5GUklOR0VNRU5ULlxyXG5cclxuU2VlIHRoZSBBcGFjaGUgVmVyc2lvbiAyLjAgTGljZW5zZSBmb3Igc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zXHJcbmFuZCBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiogKi9cclxuLyogZ2xvYmFsIFJlZmxlY3QsIFByb21pc2UgKi9cclxuXHJcbnZhciBleHRlbmRTdGF0aWNzID0gT2JqZWN0LnNldFByb3RvdHlwZU9mIHx8XHJcbiAgICAoeyBfX3Byb3RvX186IFtdIH0gaW5zdGFuY2VvZiBBcnJheSAmJiBmdW5jdGlvbiAoZCwgYikgeyBkLl9fcHJvdG9fXyA9IGI7IH0pIHx8XHJcbiAgICBmdW5jdGlvbiAoZCwgYikgeyBmb3IgKHZhciBwIGluIGIpIGlmIChiLmhhc093blByb3BlcnR5KHApKSBkW3BdID0gYltwXTsgfTtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2V4dGVuZHMoZCwgYikge1xyXG4gICAgZXh0ZW5kU3RhdGljcyhkLCBiKTtcclxuICAgIGZ1bmN0aW9uIF9fKCkgeyB0aGlzLmNvbnN0cnVjdG9yID0gZDsgfVxyXG4gICAgZC5wcm90b3R5cGUgPSBiID09PSBudWxsID8gT2JqZWN0LmNyZWF0ZShiKSA6IChfXy5wcm90b3R5cGUgPSBiLnByb3RvdHlwZSwgbmV3IF9fKCkpO1xyXG59XHJcblxyXG5leHBvcnQgdmFyIF9fYXNzaWduID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiBfX2Fzc2lnbih0KSB7XHJcbiAgICBmb3IgKHZhciBzLCBpID0gMSwgbiA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcclxuICAgICAgICBzID0gYXJndW1lbnRzW2ldO1xyXG4gICAgICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSkgdFtwXSA9IHNbcF07XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdDtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fcmVzdChzLCBlKSB7XHJcbiAgICB2YXIgdCA9IHt9O1xyXG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXHJcbiAgICAgICAgdFtwXSA9IHNbcF07XHJcbiAgICBpZiAocyAhPSBudWxsICYmIHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSBcImZ1bmN0aW9uXCIpXHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDApXHJcbiAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xyXG4gICAgcmV0dXJuIHQ7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2RlY29yYXRlKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKSB7XHJcbiAgICB2YXIgYyA9IGFyZ3VtZW50cy5sZW5ndGgsIHIgPSBjIDwgMyA/IHRhcmdldCA6IGRlc2MgPT09IG51bGwgPyBkZXNjID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcih0YXJnZXQsIGtleSkgOiBkZXNjLCBkO1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0LmRlY29yYXRlID09PSBcImZ1bmN0aW9uXCIpIHIgPSBSZWZsZWN0LmRlY29yYXRlKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKTtcclxuICAgIGVsc2UgZm9yICh2YXIgaSA9IGRlY29yYXRvcnMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIGlmIChkID0gZGVjb3JhdG9yc1tpXSkgciA9IChjIDwgMyA/IGQocikgOiBjID4gMyA/IGQodGFyZ2V0LCBrZXksIHIpIDogZCh0YXJnZXQsIGtleSkpIHx8IHI7XHJcbiAgICByZXR1cm4gYyA+IDMgJiYgciAmJiBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHIpLCByO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19wYXJhbShwYXJhbUluZGV4LCBkZWNvcmF0b3IpIHtcclxuICAgIHJldHVybiBmdW5jdGlvbiAodGFyZ2V0LCBrZXkpIHsgZGVjb3JhdG9yKHRhcmdldCwga2V5LCBwYXJhbUluZGV4KTsgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19tZXRhZGF0YShtZXRhZGF0YUtleSwgbWV0YWRhdGFWYWx1ZSkge1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0Lm1ldGFkYXRhID09PSBcImZ1bmN0aW9uXCIpIHJldHVybiBSZWZsZWN0Lm1ldGFkYXRhKG1ldGFkYXRhS2V5LCBtZXRhZGF0YVZhbHVlKTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXdhaXRlcih0aGlzQXJnLCBfYXJndW1lbnRzLCBQLCBnZW5lcmF0b3IpIHtcclxuICAgIHJldHVybiBuZXcgKFAgfHwgKFAgPSBQcm9taXNlKSkoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICAgIGZ1bmN0aW9uIGZ1bGZpbGxlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvci5uZXh0KHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cclxuICAgICAgICBmdW5jdGlvbiByZWplY3RlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvcltcInRocm93XCJdKHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cclxuICAgICAgICBmdW5jdGlvbiBzdGVwKHJlc3VsdCkgeyByZXN1bHQuZG9uZSA/IHJlc29sdmUocmVzdWx0LnZhbHVlKSA6IG5ldyBQKGZ1bmN0aW9uIChyZXNvbHZlKSB7IHJlc29sdmUocmVzdWx0LnZhbHVlKTsgfSkudGhlbihmdWxmaWxsZWQsIHJlamVjdGVkKTsgfVxyXG4gICAgICAgIHN0ZXAoKGdlbmVyYXRvciA9IGdlbmVyYXRvci5hcHBseSh0aGlzQXJnLCBfYXJndW1lbnRzIHx8IFtdKSkubmV4dCgpKTtcclxuICAgIH0pO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19nZW5lcmF0b3IodGhpc0FyZywgYm9keSkge1xyXG4gICAgdmFyIF8gPSB7IGxhYmVsOiAwLCBzZW50OiBmdW5jdGlvbigpIHsgaWYgKHRbMF0gJiAxKSB0aHJvdyB0WzFdOyByZXR1cm4gdFsxXTsgfSwgdHJ5czogW10sIG9wczogW10gfSwgZiwgeSwgdCwgZztcclxuICAgIHJldHVybiBnID0geyBuZXh0OiB2ZXJiKDApLCBcInRocm93XCI6IHZlcmIoMSksIFwicmV0dXJuXCI6IHZlcmIoMikgfSwgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIChnW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXM7IH0pLCBnO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IHJldHVybiBmdW5jdGlvbiAodikgeyByZXR1cm4gc3RlcChbbiwgdl0pOyB9OyB9XHJcbiAgICBmdW5jdGlvbiBzdGVwKG9wKSB7XHJcbiAgICAgICAgaWYgKGYpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJHZW5lcmF0b3IgaXMgYWxyZWFkeSBleGVjdXRpbmcuXCIpO1xyXG4gICAgICAgIHdoaWxlIChfKSB0cnkge1xyXG4gICAgICAgICAgICBpZiAoZiA9IDEsIHkgJiYgKHQgPSBvcFswXSAmIDIgPyB5W1wicmV0dXJuXCJdIDogb3BbMF0gPyB5W1widGhyb3dcIl0gfHwgKCh0ID0geVtcInJldHVyblwiXSkgJiYgdC5jYWxsKHkpLCAwKSA6IHkubmV4dCkgJiYgISh0ID0gdC5jYWxsKHksIG9wWzFdKSkuZG9uZSkgcmV0dXJuIHQ7XHJcbiAgICAgICAgICAgIGlmICh5ID0gMCwgdCkgb3AgPSBbb3BbMF0gJiAyLCB0LnZhbHVlXTtcclxuICAgICAgICAgICAgc3dpdGNoIChvcFswXSkge1xyXG4gICAgICAgICAgICAgICAgY2FzZSAwOiBjYXNlIDE6IHQgPSBvcDsgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDQ6IF8ubGFiZWwrKzsgcmV0dXJuIHsgdmFsdWU6IG9wWzFdLCBkb25lOiBmYWxzZSB9O1xyXG4gICAgICAgICAgICAgICAgY2FzZSA1OiBfLmxhYmVsKys7IHkgPSBvcFsxXTsgb3AgPSBbMF07IGNvbnRpbnVlO1xyXG4gICAgICAgICAgICAgICAgY2FzZSA3OiBvcCA9IF8ub3BzLnBvcCgpOyBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xyXG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgICAgICBpZiAoISh0ID0gXy50cnlzLCB0ID0gdC5sZW5ndGggPiAwICYmIHRbdC5sZW5ndGggLSAxXSkgJiYgKG9wWzBdID09PSA2IHx8IG9wWzBdID09PSAyKSkgeyBfID0gMDsgY29udGludWU7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDMgJiYgKCF0IHx8IChvcFsxXSA+IHRbMF0gJiYgb3BbMV0gPCB0WzNdKSkpIHsgXy5sYWJlbCA9IG9wWzFdOyBicmVhazsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gNiAmJiBfLmxhYmVsIDwgdFsxXSkgeyBfLmxhYmVsID0gdFsxXTsgdCA9IG9wOyBicmVhazsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0ICYmIF8ubGFiZWwgPCB0WzJdKSB7IF8ubGFiZWwgPSB0WzJdOyBfLm9wcy5wdXNoKG9wKTsgYnJlYWs7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAodFsyXSkgXy5vcHMucG9wKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBvcCA9IGJvZHkuY2FsbCh0aGlzQXJnLCBfKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7IG9wID0gWzYsIGVdOyB5ID0gMDsgfSBmaW5hbGx5IHsgZiA9IHQgPSAwOyB9XHJcbiAgICAgICAgaWYgKG9wWzBdICYgNSkgdGhyb3cgb3BbMV07IHJldHVybiB7IHZhbHVlOiBvcFswXSA/IG9wWzFdIDogdm9pZCAwLCBkb25lOiB0cnVlIH07XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2V4cG9ydFN0YXIobSwgZXhwb3J0cykge1xyXG4gICAgZm9yICh2YXIgcCBpbiBtKSBpZiAoIWV4cG9ydHMuaGFzT3duUHJvcGVydHkocCkpIGV4cG9ydHNbcF0gPSBtW3BdO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX192YWx1ZXMobykge1xyXG4gICAgdmFyIG0gPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb1tTeW1ib2wuaXRlcmF0b3JdLCBpID0gMDtcclxuICAgIGlmIChtKSByZXR1cm4gbS5jYWxsKG8pO1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBuZXh0OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGlmIChvICYmIGkgPj0gby5sZW5ndGgpIG8gPSB2b2lkIDA7XHJcbiAgICAgICAgICAgIHJldHVybiB7IHZhbHVlOiBvICYmIG9baSsrXSwgZG9uZTogIW8gfTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19yZWFkKG8sIG4pIHtcclxuICAgIHZhciBtID0gdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9bU3ltYm9sLml0ZXJhdG9yXTtcclxuICAgIGlmICghbSkgcmV0dXJuIG87XHJcbiAgICB2YXIgaSA9IG0uY2FsbChvKSwgciwgYXIgPSBbXSwgZTtcclxuICAgIHRyeSB7XHJcbiAgICAgICAgd2hpbGUgKChuID09PSB2b2lkIDAgfHwgbi0tID4gMCkgJiYgIShyID0gaS5uZXh0KCkpLmRvbmUpIGFyLnB1c2goci52YWx1ZSk7XHJcbiAgICB9XHJcbiAgICBjYXRjaCAoZXJyb3IpIHsgZSA9IHsgZXJyb3I6IGVycm9yIH07IH1cclxuICAgIGZpbmFsbHkge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGlmIChyICYmICFyLmRvbmUgJiYgKG0gPSBpW1wicmV0dXJuXCJdKSkgbS5jYWxsKGkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmaW5hbGx5IHsgaWYgKGUpIHRocm93IGUuZXJyb3I7IH1cclxuICAgIH1cclxuICAgIHJldHVybiBhcjtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fc3ByZWFkKCkge1xyXG4gICAgZm9yICh2YXIgYXIgPSBbXSwgaSA9IDA7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspXHJcbiAgICAgICAgYXIgPSBhci5jb25jYXQoX19yZWFkKGFyZ3VtZW50c1tpXSkpO1xyXG4gICAgcmV0dXJuIGFyO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hd2FpdCh2KSB7XHJcbiAgICByZXR1cm4gdGhpcyBpbnN0YW5jZW9mIF9fYXdhaXQgPyAodGhpcy52ID0gdiwgdGhpcykgOiBuZXcgX19hd2FpdCh2KTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXN5bmNHZW5lcmF0b3IodGhpc0FyZywgX2FyZ3VtZW50cywgZ2VuZXJhdG9yKSB7XHJcbiAgICBpZiAoIVN5bWJvbC5hc3luY0l0ZXJhdG9yKSB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3ltYm9sLmFzeW5jSXRlcmF0b3IgaXMgbm90IGRlZmluZWQuXCIpO1xyXG4gICAgdmFyIGcgPSBnZW5lcmF0b3IuYXBwbHkodGhpc0FyZywgX2FyZ3VtZW50cyB8fCBbXSksIGksIHEgPSBbXTtcclxuICAgIHJldHVybiBpID0ge30sIHZlcmIoXCJuZXh0XCIpLCB2ZXJiKFwidGhyb3dcIiksIHZlcmIoXCJyZXR1cm5cIiksIGlbU3ltYm9sLmFzeW5jSXRlcmF0b3JdID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gdGhpczsgfSwgaTtcclxuICAgIGZ1bmN0aW9uIHZlcmIobikgeyBpZiAoZ1tuXSkgaVtuXSA9IGZ1bmN0aW9uICh2KSB7IHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAoYSwgYikgeyBxLnB1c2goW24sIHYsIGEsIGJdKSA+IDEgfHwgcmVzdW1lKG4sIHYpOyB9KTsgfTsgfVxyXG4gICAgZnVuY3Rpb24gcmVzdW1lKG4sIHYpIHsgdHJ5IHsgc3RlcChnW25dKHYpKTsgfSBjYXRjaCAoZSkgeyBzZXR0bGUocVswXVszXSwgZSk7IH0gfVxyXG4gICAgZnVuY3Rpb24gc3RlcChyKSB7IHIudmFsdWUgaW5zdGFuY2VvZiBfX2F3YWl0ID8gUHJvbWlzZS5yZXNvbHZlKHIudmFsdWUudikudGhlbihmdWxmaWxsLCByZWplY3QpIDogc2V0dGxlKHFbMF1bMl0sIHIpOyB9XHJcbiAgICBmdW5jdGlvbiBmdWxmaWxsKHZhbHVlKSB7IHJlc3VtZShcIm5leHRcIiwgdmFsdWUpOyB9XHJcbiAgICBmdW5jdGlvbiByZWplY3QodmFsdWUpIHsgcmVzdW1lKFwidGhyb3dcIiwgdmFsdWUpOyB9XHJcbiAgICBmdW5jdGlvbiBzZXR0bGUoZiwgdikgeyBpZiAoZih2KSwgcS5zaGlmdCgpLCBxLmxlbmd0aCkgcmVzdW1lKHFbMF1bMF0sIHFbMF1bMV0pOyB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2FzeW5jRGVsZWdhdG9yKG8pIHtcclxuICAgIHZhciBpLCBwO1xyXG4gICAgcmV0dXJuIGkgPSB7fSwgdmVyYihcIm5leHRcIiksIHZlcmIoXCJ0aHJvd1wiLCBmdW5jdGlvbiAoZSkgeyB0aHJvdyBlOyB9KSwgdmVyYihcInJldHVyblwiKSwgaVtTeW1ib2wuaXRlcmF0b3JdID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gdGhpczsgfSwgaTtcclxuICAgIGZ1bmN0aW9uIHZlcmIobiwgZikgeyBpW25dID0gb1tuXSA/IGZ1bmN0aW9uICh2KSB7IHJldHVybiAocCA9ICFwKSA/IHsgdmFsdWU6IF9fYXdhaXQob1tuXSh2KSksIGRvbmU6IG4gPT09IFwicmV0dXJuXCIgfSA6IGYgPyBmKHYpIDogdjsgfSA6IGY7IH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXN5bmNWYWx1ZXMobykge1xyXG4gICAgaWYgKCFTeW1ib2wuYXN5bmNJdGVyYXRvcikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN5bWJvbC5hc3luY0l0ZXJhdG9yIGlzIG5vdCBkZWZpbmVkLlwiKTtcclxuICAgIHZhciBtID0gb1tTeW1ib2wuYXN5bmNJdGVyYXRvcl0sIGk7XHJcbiAgICByZXR1cm4gbSA/IG0uY2FsbChvKSA6IChvID0gdHlwZW9mIF9fdmFsdWVzID09PSBcImZ1bmN0aW9uXCIgPyBfX3ZhbHVlcyhvKSA6IG9bU3ltYm9sLml0ZXJhdG9yXSgpLCBpID0ge30sIHZlcmIoXCJuZXh0XCIpLCB2ZXJiKFwidGhyb3dcIiksIHZlcmIoXCJyZXR1cm5cIiksIGlbU3ltYm9sLmFzeW5jSXRlcmF0b3JdID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gdGhpczsgfSwgaSk7XHJcbiAgICBmdW5jdGlvbiB2ZXJiKG4pIHsgaVtuXSA9IG9bbl0gJiYgZnVuY3Rpb24gKHYpIHsgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHsgdiA9IG9bbl0odiksIHNldHRsZShyZXNvbHZlLCByZWplY3QsIHYuZG9uZSwgdi52YWx1ZSk7IH0pOyB9OyB9XHJcbiAgICBmdW5jdGlvbiBzZXR0bGUocmVzb2x2ZSwgcmVqZWN0LCBkLCB2KSB7IFByb21pc2UucmVzb2x2ZSh2KS50aGVuKGZ1bmN0aW9uKHYpIHsgcmVzb2x2ZSh7IHZhbHVlOiB2LCBkb25lOiBkIH0pOyB9LCByZWplY3QpOyB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX21ha2VUZW1wbGF0ZU9iamVjdChjb29rZWQsIHJhdykge1xyXG4gICAgaWYgKE9iamVjdC5kZWZpbmVQcm9wZXJ0eSkgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkoY29va2VkLCBcInJhd1wiLCB7IHZhbHVlOiByYXcgfSk7IH0gZWxzZSB7IGNvb2tlZC5yYXcgPSByYXc7IH1cclxuICAgIHJldHVybiBjb29rZWQ7XHJcbn07XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19pbXBvcnRTdGFyKG1vZCkge1xyXG4gICAgaWYgKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgcmV0dXJuIG1vZDtcclxuICAgIHZhciByZXN1bHQgPSB7fTtcclxuICAgIGlmIChtb2QgIT0gbnVsbCkgZm9yICh2YXIgayBpbiBtb2QpIGlmIChPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtb2QsIGspKSByZXN1bHRba10gPSBtb2Rba107XHJcbiAgICByZXN1bHQuZGVmYXVsdCA9IG1vZDtcclxuICAgIHJldHVybiByZXN1bHQ7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2ltcG9ydERlZmF1bHQobW9kKSB7XHJcbiAgICByZXR1cm4gKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgPyBtb2QgOiB7IGRlZmF1bHQ6IG1vZCB9O1xyXG59XHJcbiIsIlxyXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQmFzZVV0aWwge1xyXG4gIHByb3RlY3RlZCBsb2c7XHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICB0aGlzLmxvZyA9IHtcclxuICAgICAgaW5mbyhtc2cpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhtc2cpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG4gICAgdGhpcy5sb2cuaW5mbygnQ2xhc3MgJyArIHRoaXMuY29uc3RydWN0b3IubmFtZSArICcgY3JlYXRlZCcpO1xyXG4gIH1cclxufVxyXG4iLCJpbXBvcnQgeyBIdHRwRXJyb3JSZXNwb25zZSwgSHR0cEhlYWRlcnMsIEh0dHBSZXF1ZXN0LCBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSwgSW5qZWN0aW9uVG9rZW4gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgb2YsIHRocm93RXJyb3IsIE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IEJhc2VVdGlsIH0gZnJvbSAnLi9iYXNlLXV0aWwnO1xyXG5pbXBvcnQgeyBJSGVhZGVyLCBJTW9jaywgSU1vY2tEYXRhLCBJUGFyYW0gfSBmcm9tICcuL2ltb2NrLWRhdGEubW9kZWwnO1xyXG5cclxuZXhwb3J0IGNvbnN0IFRPS0VOX01PQ0sgPSBuZXcgSW5qZWN0aW9uVG9rZW4oJ21vY2tkYXRhJyk7XHJcbi8vIGltcG9ydCAncnhqcy9hZGQvb2JzZXJ2YWJsZS90aHJvdyc7XHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE1vY2tEYXRhU2VydmljZSBleHRlbmRzIEJhc2VVdGlsIHtcclxuXHJcbiAgY29uc3RydWN0b3IoQEluamVjdChUT0tFTl9NT0NLKSBwcml2YXRlIG1vY2tEYXRhOiBJTW9jaykge1xyXG4gICAgc3VwZXIoKTtcclxuICB9XHJcblxyXG5cclxuICBnZXRSZXF1ZXN0TW9jayhyZXE6IEh0dHBSZXF1ZXN0PGFueT4pOiBJTW9ja0RhdGEge1xyXG4gICAgcmV0dXJuIHRoaXMubW9ja0RhdGEubW9ja3MuZmluZCgoZGF0YTogSU1vY2tEYXRhKSA9PiB7XHJcbiAgICAgIC8vIGNoZWNrIGlmIHRoZSBtZXRob2QgYW5kIHVybCBhbmQgcGFyYW1zIGFuZCBtYXRjaCBwYXJhbXNcclxuICAgICAgaWYgKFxyXG4gICAgICAgIHJlcS5tZXRob2QgPT09IGRhdGEudHlwZSAmJlxyXG4gICAgICAgIHJlcS51cmwuaW5jbHVkZXMoZGF0YS51cmwpICYmXHJcbiAgICAgICAgdGhpcy5tYXRjaEhlYWRlcnMoZGF0YSwgcmVxKSAmJlxyXG4gICAgICAgIHRoaXMubWF0Y2hQYXJhbXMoZGF0YSwgcmVxKSAmJlxyXG4gICAgICAgIHRoaXMubWF0Y2hCb2R5KGRhdGEsIHJlcSlcclxuICAgICAgKSB7XHJcbiAgICAgICAgaWYgKGRhdGEubmFtZSkge1xyXG4gICAgICAgICAgdGhpcy5sb2cuaW5mbygnLS0tbW9jayBkYXRhIG1hdGNoZWQgJyArIGRhdGEubmFtZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0UmVxdWVzdE1vY2tEYXRhKHJlcTogSHR0cFJlcXVlc3Q8YW55PiwgbW9ja0RhdGE6IElNb2NrKTogSU1vY2tEYXRhIHtcclxuICAgIGlmICghbW9ja0RhdGEpIHtcclxuICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIG1vY2tEYXRhLm1vY2tzLmZpbmQoKGRhdGE6IElNb2NrRGF0YSkgPT4ge1xyXG4gICAgICAvLyBjaGVjayBpZiB0aGUgbWV0aG9kIGFuZCB1cmwgYW5kIHBhcmFtcyBhbmQgbWF0Y2ggcGFyYW1zXHJcbiAgICAgIGlmIChcclxuICAgICAgICB0aGlzLm1hdGNoTWV0aG9kKGRhdGEsIHJlcSkgJiZcclxuICAgICAgICB0aGlzLm1hdGNoVXJsKGRhdGEsIHJlcSkgJiZcclxuICAgICAgICB0aGlzLm1hdGNoSGVhZGVycyhkYXRhLCByZXEpICYmXHJcbiAgICAgICAgdGhpcy5tYXRjaFBhcmFtcyhkYXRhLCByZXEpICYmXHJcbiAgICAgICAgdGhpcy5tYXRjaEJvZHkoZGF0YSwgcmVxKVxyXG4gICAgICApIHtcclxuICAgICAgICBpZiAoZGF0YS5uYW1lKSB7XHJcbiAgICAgICAgICB0aGlzLmxvZy5pbmZvKCctLS1tb2NrIGRhdGEgbWF0Y2hlZCAnICsgZGF0YS5uYW1lKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIG1hdGNoTWV0aG9kKGRhdGE6IElNb2NrRGF0YSwgcmVxOiBIdHRwUmVxdWVzdDxhbnk+KTogYm9vbGVhbiB7XHJcbiAgICBpZiAoZGF0YS50eXBlKSB7XHJcbiAgICAgIHJldHVybiByZXEubWV0aG9kID09PSBkYXRhLnR5cGU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgbWF0Y2hVcmwoZGF0YTogSU1vY2tEYXRhLCByZXE6IEh0dHBSZXF1ZXN0PGFueT4pOiBib29sZWFuIHtcclxuICAgIGlmIChkYXRhLnVybCkge1xyXG4gICAgICByZXR1cm4gcmVxLnVybC5pbmNsdWRlcyhkYXRhLnVybCk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgbWF0Y2hQYXJhbXMoZGF0YTogSU1vY2tEYXRhLCByZXE6IEh0dHBSZXF1ZXN0PGFueT4pOiBib29sZWFuIHtcclxuICAgIGlmIChkYXRhLnBhcmFtcykge1xyXG4gICAgICByZXR1cm4gZGF0YS5wYXJhbXMuZXZlcnkoXHJcbiAgICAgICAgKHBhcmFtOiBJUGFyYW0pID0+IHJlcS5wYXJhbXNbcGFyYW0ua2V5XSA9PT0gcGFyYW0udmFsdWUgfHwgKHJlcS5wYXJhbXMuZ2V0ICYmIHJlcS5wYXJhbXMuZ2V0KHBhcmFtLmtleSkgPT09IHBhcmFtLnZhbHVlKVxyXG4gICAgICApO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG4gIHByaXZhdGUgbWF0Y2hIZWFkZXJzKGRhdGE6IElNb2NrRGF0YSwgcmVxOiBIdHRwUmVxdWVzdDxhbnk+KTogYm9vbGVhbiB7XHJcbiAgICBpZiAoZGF0YS5oZWFkZXJzUmVxdWVzdCkge1xyXG4gICAgICByZXR1cm4gZGF0YS5oZWFkZXJzUmVxdWVzdC5ldmVyeSgoaGVhZGVyOiBJSGVhZGVyKSA9PiByZXEuaGVhZGVycy5nZXQoaGVhZGVyLmtleSkgPT09IGhlYWRlci52YWx1ZSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgbWF0Y2hCb2R5KGRhdGE6IElNb2NrRGF0YSwgcmVxOiBIdHRwUmVxdWVzdDxhbnk+KTogYm9vbGVhbiB7XHJcbiAgICBpZiAoZGF0YS5ib2R5KSB7XHJcbiAgICAgIGNvbnN0IHJlcyA9IG9iamVjdEVxdWFscyhkYXRhLmJvZHksIHJlcS5ib2R5KTtcclxuICAgICAgdGhpcy5sb2cuaW5mbyhgICR7ZGF0YS5uYW1lfSAtY29tcGFyZSByZXM9JHtyZXN9IGJvZHkgcmVxICR7SlNPTi5zdHJpbmdpZnkocmVxLmJvZHkpfWApO1xyXG4gICAgICAvLyByZXR1cm4gSlNPTi5zdHJpbmdpZnkoZGF0YS5ib2R5KT09SlNPTi5zdHJpbmdpZnkocmVxLmJvZHkpO1xyXG4gICAgICByZXR1cm4gcmVzO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBnZXRSZXNwb25zZU1vY2soZGF0YTogSU1vY2tEYXRhKTpPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgY29uc3QgcmVzcCA9IHRoaXMuZ2V0UmVzcG9uc2VNb2NrT2JqKGRhdGEpO1xyXG4gICAgaWYgKHJlc3AgaW5zdGFuY2VvZiBIdHRwRXJyb3JSZXNwb25zZSkge1xyXG4gICAgICByZXR1cm4gdGhyb3dFcnJvcihyZXNwKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBvZihyZXNwKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldFJlc3BvbnNlTW9ja09iaihkYXRhOiBJTW9ja0RhdGEpOiBIdHRwRXJyb3JSZXNwb25zZSB8IEh0dHBSZXNwb25zZTxvYmplY3Q+IHtcclxuICAgIGlmICghZGF0YS5yZXNwb3NlU3RhdHVzKSB7XHJcbiAgICAgIGRhdGEucmVzcG9zZVN0YXR1cyA9IDIwMDtcclxuICAgIH1cclxuICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKCk7XHJcbiAgICBpZiAoZGF0YS5oZWFkZXJzUmVzcG9uc2UpIHtcclxuICAgICAgZGF0YS5oZWFkZXJzUmVzcG9uc2UuZm9yRWFjaChoZWFkZXIgPT4gKGhlYWRlcnMgPSBoZWFkZXJzLmFwcGVuZChoZWFkZXIua2V5LCBoZWFkZXIudmFsdWUpKSk7XHJcbiAgICB9XHJcbiAgICBpZiAoZGF0YS5yZXNwb3NlU3RhdHVzICE9PSAyMDApIHtcclxuICAgICAgcmV0dXJuIG5ldyBIdHRwRXJyb3JSZXNwb25zZSh7XHJcbiAgICAgICAgc3RhdHVzOiBkYXRhLnJlc3Bvc2VTdGF0dXMsXHJcbiAgICAgICAgaGVhZGVyczogaGVhZGVycyxcclxuICAgICAgICBlcnJvcjogZGF0YS5lcnJvcixcclxuICAgICAgICBzdGF0dXNUZXh0OiBkYXRhLnN0YXR1c1RleHRcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICBuZXcgSHR0cFJlc3BvbnNlKHtcclxuICAgICAgICBzdGF0dXM6IGRhdGEucmVzcG9zZVN0YXR1cyxcclxuICAgICAgICBib2R5OiBkYXRhLnJlc3BvbnNlID8gZGF0YS5yZXNwb25zZSA6IG51bGwsXHJcbiAgICAgICAgaGVhZGVyczogaGVhZGVyc1xyXG4gICAgICB9KVxyXG4gICAgKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIG9iamVjdEVxdWFscyh4LCB5KSB7XHJcbiAgLy8gaWYgYm90aCBhcmUgZnVuY3Rpb25cclxuICBpZiAoeCBpbnN0YW5jZW9mIEZ1bmN0aW9uKSB7XHJcbiAgICBpZiAoeSBpbnN0YW5jZW9mIEZ1bmN0aW9uKSB7XHJcbiAgICAgIHJldHVybiB4LnRvU3RyaW5nKCkgPT09IHkudG9TdHJpbmcoKTtcclxuICAgIH1cclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcbiAgaWYgKHggPT09IG51bGwgfHwgeCA9PT0gdW5kZWZpbmVkIHx8IHkgPT09IG51bGwgfHwgeSA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICByZXR1cm4geCA9PT0geTtcclxuICB9XHJcbiAgaWYgKHggPT09IHkgfHwgeC52YWx1ZU9mKCkgPT09IHkudmFsdWVPZigpKSB7XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIC8vIGlmIG9uZSBvZiB0aGVtIGlzIGRhdGUsIHRoZXkgbXVzdCBoYWQgZXF1YWwgdmFsdWVPZlxyXG4gIGlmICh4IGluc3RhbmNlb2YgRGF0ZSkge1xyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuICBpZiAoeSBpbnN0YW5jZW9mIERhdGUpIHtcclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcblxyXG4gIC8vIGlmIHRoZXkgYXJlIG5vdCBmdW5jdGlvbiBvciBzdHJpY3RseSBlcXVhbCwgdGhleSBib3RoIG5lZWQgdG8gYmUgT2JqZWN0c1xyXG4gIGlmICghKHggaW5zdGFuY2VvZiBPYmplY3QpKSB7XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG4gIGlmICghKHkgaW5zdGFuY2VvZiBPYmplY3QpKSB7XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBjb25zdCBwID0gT2JqZWN0LmtleXMoeCk7XHJcbiAgcmV0dXJuIHAuZXZlcnkoZnVuY3Rpb24oaSkge1xyXG4gICAgcmV0dXJuIG9iamVjdEVxdWFscyh4W2ldLCB5W2ldKTtcclxuICB9KTtcclxuXHJcbiAgLyogcmV0dXJuIE9iamVjdC5rZXlzKHkpLmV2ZXJ5KGZ1bmN0aW9uIChpKSB7IHJldHVybiBwLmluZGV4T2YoaSkgIT09IC0xOyB9KSA/XHJcbiAgICAgICAgICBwLmV2ZXJ5KGZ1bmN0aW9uIChpKSB7IHJldHVybiBvYmplY3RFcXVhbHMoeFtpXSwgeVtpXSk7IH0pIDogZmFsc2U7ICovXHJcbn1cclxuIiwiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTXljb21wc1NlcnZpY2Uge1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG59XHJcbiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ215LW15Y29tcHMnLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxwPlxuICAgICAgbXljb21wcyB3b3JrcyBhcnR1ciFcbiAgICA8L3A+XG4gIGBcbiAgLCAgc3R5bGVzOiBbYHB7YmFja2dyb3VuZDpyZWR9YF1cbn0pXG5leHBvcnQgY2xhc3MgTXljb21wc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdteS1teXRlc3Rjb21wJyxcbiAgdGVtcGxhdGU6IGBcbjxkaXY+XG4gICA8cD5cbiAgbXl0ZXN0Y29tcCB3b3JrcyoqKioqIGZyb20gbGlicmFyeSFcbjwvcD5cbiAgICBcbjwvZGl2PlxuXG5cbmAsXG4gIHN0eWxlczogW2BkaXYgcHtiYWNrZ3JvdW5kOnBpbmt9YF1cbn0pXG5leHBvcnQgY2xhc3MgTXl0ZXN0Y29tcENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBIdHRwRXJyb3JSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge1xyXG4gIEh0dHBFdmVudCxcclxuICBIdHRwSW50ZXJjZXB0b3IsXHJcbiAgSHR0cEhhbmRsZXIsXHJcbiAgSHR0cFJlcXVlc3QsXHJcbiAgSHR0cFJlc3BvbnNlXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlICwgIG9mLCB0aHJvd0Vycm9yfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IGNhdGNoRXJyb3IsIG1hcCwgdGFwLCByZWZDb3VudCwgcHVibGlzaFJlcGxheSwgZGVsYXksIHJldHJ5IH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBNb2NrRGF0YVNlcnZpY2UgfSBmcm9tICcuL21vY2stZGF0YS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQmFzZVV0aWwgfSBmcm9tICcuL2Jhc2UtdXRpbCc7XHJcblxyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQXBwQ29yZUludGVyY2VwdG9yIGV4dGVuZHMgQmFzZVV0aWwgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgc3VwZXIoKTtcclxuICB9XHJcblxyXG4gIGludGVyY2VwdChyZXE6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xyXG4gICAgICAgICBjb25zdCBjaGFuZ2VkUmVxID0gcmVxLmNsb25lKHtcclxuICAgICAgICAgICAgaGVhZGVyczogcmVxLmhlYWRlcnMuc2V0KCdNeS1IZWFkZXInLCAnTXlIZWFkZXJWYWx1ZScpXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIHRoaXMubG9nLmluZm8oJ3Bhc3MgaW50ZXJjZXB0IGNvcmVlZScpO1xyXG4gICAgICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKGNoYW5nZWRSZXEpXHJcbiAgICAgICAgICAucGlwZSh0YXAoKGV2ZW50OiBIdHRwRXZlbnQ8YW55PikgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZXZlbnQgaW5zdGFuY2VvZiBIdHRwUmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICBjb25zdCByID0gZXZlbnQgYXMgSHR0cFJlc3BvbnNlPGFueT47XHJcbiAgICAgICAgICAgICAgdGhpcy5sb2cuaW5mbygndXJsIHJlc3AqKj09PScgKyByLnVybCk7XHJcbiAgICAgICAgICAgICAgdGhpcy5sb2cuaW5mbygnc3RhdHVzIHJlc3AqKj09PScgKyByLnN0YXR1cyk7XHJcbiAgICAgICAgICAgICAgdGhpcy5sb2cuaW5mbygnaGVhZGVycyByZXNwKio9PT0nICsgSlNPTi5zdHJpbmdpZnkoci5oZWFkZXJzKSk7XHJcbiAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSwgKGVycm9yOiBIdHRwRXJyb3JSZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmxvZy5pbmZvKCdoZWFkZXJzIGVycm9yIHJlc3AqKj09JyArIEpTT04uc3RyaW5naWZ5KGVycm9yLmhlYWRlcnMpKTtcclxuICAgICAgICAgICAgdGhpcy5sb2cuaW5mbygnZXJyb3IgcmVzcCoqPT0nICsgSlNPTi5zdHJpbmdpZnkoZXJyb3IuZXJyb3IpKTtcclxuICAgICAgICAgICAgdGhpcy5sb2cuaW5mbygnZXJyb3IgY29yZSBzdGF0dXMqKionICsgZXJyb3Iuc3RhdHVzKTtcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgICAsIGNhdGNoRXJyb3IoKGVycikgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmxvZy5pbmZvKCdlcnJvciBjYXRoIGNvcmUqKionICsgZXJyKTtcclxuICAgICAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyKTtcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgLyogICBjYXRjaCgoZXJyKT0+e1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnZXJyb3IgY2F0aCBjb3JlKioqJytlcnIpO1xyXG4gICAgICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS50aHJvdyhlcnIpO1xyXG4gICAgICAgICAgfSk7ICovXHJcblxyXG4gICAgICAgIH1cclxuXHJcbn1cclxuIiwiaW1wb3J0IHsgSHR0cEVycm9yUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtcclxuICBIdHRwRXZlbnQsXHJcbiAgSHR0cEludGVyY2VwdG9yLFxyXG4gIEh0dHBIYW5kbGVyLFxyXG4gIEh0dHBSZXF1ZXN0LFxyXG4gIEh0dHBSZXNwb25zZVxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuXHJcbmltcG9ydCB7IGNhdGNoRXJyb3IsIG1hcCwgdGFwLCByZWZDb3VudCwgcHVibGlzaFJlcGxheSwgZGVsYXksIHJldHJ5LCBtZXJnZU1hcCwgbWF0ZXJpYWxpemUsIGRlbWF0ZXJpYWxpemUgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IE1vY2tEYXRhU2VydmljZSB9IGZyb20gJy4vbW9jay1kYXRhLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBJTW9ja0RhdGEgfSBmcm9tICcuL2ltb2NrLWRhdGEubW9kZWwnO1xyXG5cclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgb2YgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQmFzZVV0aWwgfSBmcm9tICcuL2Jhc2UtdXRpbCc7XHJcblxyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQXBwSW50ZXJjZXB0b3JNb2NrIGV4dGVuZHMgQmFzZVV0aWwgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbW9ja1NydjogTW9ja0RhdGFTZXJ2aWNlKSB7XHJcbiAgICBzdXBlcigpO1xyXG4gIH1cclxuXHJcbiAgaW50ZXJjZXB0KHJlcTogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XHJcbiAgIGNvbnN0IG1vY2s6IElNb2NrRGF0YSA9IHRoaXMubW9ja1Nydi5nZXRSZXF1ZXN0TW9jayhyZXEpO1xyXG5cclxuICAgdGhpcy5sb2cuaW5mbygnZW50ZXIgYXBwIGludGVyY2VwdG9yIE1PQ0sqKioqKioqKioqKicpO1xyXG5cclxuICAgcmV0dXJuIG9mKG51bGwpLnBpcGUoXHJcbiAgICAgIG1lcmdlTWFwKCgpID0+IHtcclxuICAgICAgICBpZiAobW9jaykge1xyXG4gICAgICAgICAgcmV0dXJuIHRoaXMubW9ja1Nydi5nZXRSZXNwb25zZU1vY2sobW9jayk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGNoYW5nZWRSZXEgPSByZXEuY2xvbmUoe1xyXG4gICAgICAgICAgaGVhZGVyczogcmVxLmhlYWRlcnMuc2V0KCdNeS1IZWFkZXInLCAnTXlIZWFkZXJWYWx1ZScpXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKGNoYW5nZWRSZXEpO1xyXG4gICAgICB9KVxyXG4gICAgICAsIG1hdGVyaWFsaXplKClcclxuICAgICAgLCBkZWxheSg1MDApXHJcbiAgICAgICwgZGVtYXRlcmlhbGl6ZSgpXHJcbiAgICApO1xyXG5cclxuXHJcbiAgfVxyXG59XHJcbiIsImltcG9ydCB7IEhUVFBfSU5URVJDRVBUT1JTIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgQXBwQ29yZUludGVyY2VwdG9yIH0gZnJvbSAnLi9hcHAuY29yZS5pbnRlcmNlcHRvcic7XHJcbmltcG9ydCB7IEFwcEludGVyY2VwdG9yTW9jayB9IGZyb20gJy4vYXBwLmludGVyY2VwdG9yJztcclxuaW1wb3J0IHsgTW9ja0RhdGFTZXJ2aWNlIH0gZnJvbSAnLi9tb2NrLWRhdGEuc2VydmljZSc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6IFtdLFxyXG4gIGRlY2xhcmF0aW9uczogW10sXHJcbiAgcHJvdmlkZXJzOiBbXHJcbiAgICBNb2NrRGF0YVNlcnZpY2UsXHJcbiAgICB7XHJcbiAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxyXG4gICAgICB1c2VDbGFzczogQXBwQ29yZUludGVyY2VwdG9yLFxyXG4gICAgICBtdWx0aTogdHJ1ZVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXHJcbiAgICAgIHVzZUNsYXNzOiBBcHBJbnRlcmNlcHRvck1vY2ssXHJcbiAgICAgIG11bHRpOiB0cnVlXHJcbiAgICB9XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTW9ja1V0aWxNb2R1bGUge1xyXG4gIGNvbnN0cnVjdG9yKCl7XHJcbiAgICBjb25zb2xlLmxvZygnZW50ZXIgbW9ja2trayBtb2R1bGUnKVxyXG4gIH1cclxufVxyXG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTXljb21wc0NvbXBvbmVudCB9IGZyb20gJy4vbXljb21wcy5jb21wb25lbnQnO1xuaW1wb3J0IHsgTXl0ZXN0Y29tcENvbXBvbmVudCB9IGZyb20gJy4vbXl0ZXN0Y29tcC9teXRlc3Rjb21wLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBNb2NrVXRpbE1vZHVsZSB9IGZyb20gJy4vbW9ja3V0aWwvbW9jay11dGlsLm1vZHVsZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgICBNb2NrVXRpbE1vZHVsZVxuICBdLFxuICBkZWNsYXJhdGlvbnM6IFtNeWNvbXBzQ29tcG9uZW50LCBNeXRlc3Rjb21wQ29tcG9uZW50XSxcbiAgZXhwb3J0czogW015Y29tcHNDb21wb25lbnQsIE15dGVzdGNvbXBDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIE15Y29tcHNNb2R1bGUgeyB9XG4iXSwibmFtZXMiOlsiSW5qZWN0aW9uVG9rZW4iLCJ0c2xpYl8xLl9fZXh0ZW5kcyIsIkh0dHBFcnJvclJlc3BvbnNlIiwidGhyb3dFcnJvciIsIm9mIiwiSHR0cEhlYWRlcnMiLCJIdHRwUmVzcG9uc2UiLCJJbmplY3RhYmxlIiwiSW5qZWN0IiwiQ29tcG9uZW50IiwidGFwIiwiY2F0Y2hFcnJvciIsIm1lcmdlTWFwIiwibWF0ZXJpYWxpemUiLCJkZWxheSIsImRlbWF0ZXJpYWxpemUiLCJOZ01vZHVsZSIsIkhUVFBfSU5URVJDRVBUT1JTIl0sIm1hcHBpbmdzIjoiOzs7Ozs7SUFBQTs7Ozs7Ozs7Ozs7Ozs7SUFjQTtJQUVBLElBQUksYUFBYSxHQUFHLE1BQU0sQ0FBQyxjQUFjO1NBQ3BDLEVBQUUsU0FBUyxFQUFFLEVBQUUsRUFBRSxZQUFZLEtBQUssSUFBSSxVQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzVFLFVBQVUsQ0FBQyxFQUFFLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUM7WUFBRSxJQUFJLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0FBRS9FLHVCQUEwQixDQUFDLEVBQUUsQ0FBQztRQUMxQixhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3BCLGdCQUFnQixJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxFQUFFO1FBQ3ZDLENBQUMsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxLQUFLLElBQUksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDekYsQ0FBQzs7Ozs7Ozs7O0FDdkJEOztRQUFBO1FBRUU7WUFDRSxJQUFJLENBQUMsR0FBRyxHQUFHO2dCQUNULElBQUk7Ozs4QkFBQyxHQUFHO29CQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQ2xCO2FBQ0YsQ0FBQztZQUNGLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxVQUFVLENBQUMsQ0FBQztTQUM5RDt1QkFWSDtRQVdDOzs7Ozs7eUJDSlksVUFBVSxHQUFHLElBQUlBLGlCQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7O1FBR3BCQyxtQ0FBUTtRQUUzQyx5QkFBd0MsUUFBZTtZQUF2RCxZQUNFLGlCQUFPLFNBQ1I7WUFGdUMsY0FBUSxHQUFSLFFBQVEsQ0FBTzs7U0FFdEQ7Ozs7O1FBR0Qsd0NBQWM7Ozs7WUFBZCxVQUFlLEdBQXFCO2dCQUFwQyxpQkFpQkM7Z0JBaEJDLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBZTs7b0JBRTlDLElBQ0UsR0FBRyxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsSUFBSTt3QkFDeEIsR0FBRyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQzt3QkFDMUIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDO3dCQUM1QixLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUM7d0JBQzNCLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FDMUIsRUFBRTt3QkFDQSxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7NEJBQ2IsS0FBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3lCQUNwRDt3QkFDRCxPQUFPLElBQUksQ0FBQztxQkFDYjtvQkFDRCxPQUFPLEtBQUssQ0FBQztpQkFDZCxDQUFDLENBQUM7YUFDSjs7Ozs7O1FBRUQsNENBQWtCOzs7OztZQUFsQixVQUFtQixHQUFxQixFQUFFLFFBQWU7Z0JBQXpELGlCQXFCQztnQkFwQkMsSUFBSSxDQUFDLFFBQVEsRUFBRTtvQkFDYixPQUFPLElBQUksQ0FBQztpQkFDYjtnQkFFRCxPQUFPLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBZTs7b0JBRXpDLElBQ0UsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDO3dCQUMzQixLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxHQUFHLENBQUM7d0JBQ3hCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQzt3QkFDNUIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDO3dCQUMzQixLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxHQUFHLENBQzFCLEVBQUU7d0JBQ0EsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFOzRCQUNiLEtBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt5QkFDcEQ7d0JBQ0QsT0FBTyxJQUFJLENBQUM7cUJBQ2I7b0JBQ0QsT0FBTyxLQUFLLENBQUM7aUJBQ2QsQ0FBQyxDQUFDO2FBQ0o7Ozs7OztRQUVPLHFDQUFXOzs7OztzQkFBQyxJQUFlLEVBQUUsR0FBcUI7Z0JBQ3hELElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDYixPQUFPLEdBQUcsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQztpQkFDakM7Z0JBQ0QsT0FBTyxJQUFJLENBQUM7Ozs7Ozs7UUFHTixrQ0FBUTs7Ozs7c0JBQUMsSUFBZSxFQUFFLEdBQXFCO2dCQUNyRCxJQUFJLElBQUksQ0FBQyxHQUFHLEVBQUU7b0JBQ1osT0FBTyxHQUFHLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQ25DO2dCQUNELE9BQU8sSUFBSSxDQUFDOzs7Ozs7O1FBR04scUNBQVc7Ozs7O3NCQUFDLElBQWUsRUFBRSxHQUFxQjtnQkFDeEQsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO29CQUNmLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQ3RCLFVBQUMsS0FBYSxJQUFLLE9BQUEsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssS0FBSyxDQUFDLEtBQUssS0FBSyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFBLENBQzFILENBQUM7aUJBQ0g7Z0JBQ0QsT0FBTyxJQUFJLENBQUM7Ozs7Ozs7UUFFTixzQ0FBWTs7Ozs7c0JBQUMsSUFBZSxFQUFFLEdBQXFCO2dCQUN6RCxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7b0JBQ3ZCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsVUFBQyxNQUFlLElBQUssT0FBQSxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssTUFBTSxDQUFDLEtBQUssR0FBQSxDQUFDLENBQUM7aUJBQ3JHO2dCQUNELE9BQU8sSUFBSSxDQUFDOzs7Ozs7O1FBR04sbUNBQVM7Ozs7O3NCQUFDLElBQWUsRUFBRSxHQUFxQjtnQkFDdEQsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO29CQUNiLHFCQUFNLEdBQUcsR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzlDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQUksSUFBSSxDQUFDLElBQUksc0JBQWlCLEdBQUcsa0JBQWEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFHLENBQUMsQ0FBQzs7b0JBRXhGLE9BQU8sR0FBRyxDQUFDO2lCQUNaO2dCQUNELE9BQU8sSUFBSSxDQUFDOzs7Ozs7UUFHZCx5Q0FBZTs7OztZQUFmLFVBQWdCLElBQWU7Z0JBQzdCLHFCQUFNLElBQUksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzNDLElBQUksSUFBSSxZQUFZQyxzQkFBaUIsRUFBRTtvQkFDckMsT0FBT0MsZUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUN6QjtxQkFBTTtvQkFDTCxPQUFPQyxPQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ2pCO2FBQ0Y7Ozs7O1FBRUQsNENBQWtCOzs7O1lBQWxCLFVBQW1CLElBQWU7Z0JBQ2hDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFO29CQUN2QixJQUFJLENBQUMsYUFBYSxHQUFHLEdBQUcsQ0FBQztpQkFDMUI7Z0JBQ0QscUJBQUksT0FBTyxHQUFHLElBQUlDLGdCQUFXLEVBQUUsQ0FBQztnQkFDaEMsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO29CQUN4QixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFBLE1BQU0sSUFBSSxRQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFDLENBQUMsQ0FBQztpQkFDOUY7Z0JBQ0QsSUFBSSxJQUFJLENBQUMsYUFBYSxLQUFLLEdBQUcsRUFBRTtvQkFDOUIsT0FBTyxJQUFJSCxzQkFBaUIsQ0FBQzt3QkFDM0IsTUFBTSxFQUFFLElBQUksQ0FBQyxhQUFhO3dCQUMxQixPQUFPLEVBQUUsT0FBTzt3QkFDaEIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO3dCQUNqQixVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVU7cUJBQzVCLENBQUMsQ0FBQztpQkFDSjtnQkFDRCxRQUNFLElBQUlJLGlCQUFZLENBQUM7b0JBQ2YsTUFBTSxFQUFFLElBQUksQ0FBQyxhQUFhO29CQUMxQixJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUk7b0JBQzFDLE9BQU8sRUFBRSxPQUFPO2lCQUNqQixDQUFDLEVBQ0Y7YUFDSDs7b0JBekhGQyxhQUFVOzs7Ozt3REFHSUMsU0FBTSxTQUFDLFVBQVU7Ozs4QkFaaEM7TUFVcUMsUUFBUTs7Ozs7O0lBMkg3QyxzQkFBc0IsQ0FBQyxFQUFFLENBQUM7O1FBRXhCLElBQUksQ0FBQyxZQUFZLFFBQVEsRUFBRTtZQUN6QixJQUFJLENBQUMsWUFBWSxRQUFRLEVBQUU7Z0JBQ3pCLE9BQU8sQ0FBQyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN0QztZQUNELE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFDRCxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLFNBQVMsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxTQUFTLEVBQUU7WUFDbEUsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2hCO1FBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7WUFDMUMsT0FBTyxJQUFJLENBQUM7U0FDYjs7UUFHRCxJQUFJLENBQUMsWUFBWSxJQUFJLEVBQUU7WUFDckIsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUNELElBQUksQ0FBQyxZQUFZLElBQUksRUFBRTtZQUNyQixPQUFPLEtBQUssQ0FBQztTQUNkOztRQUdELElBQUksRUFBRSxDQUFDLFlBQVksTUFBTSxDQUFDLEVBQUU7WUFDMUIsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUNELElBQUksRUFBRSxDQUFDLFlBQVksTUFBTSxDQUFDLEVBQUU7WUFDMUIsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUVELHFCQUFNLENBQUMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3pCLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFTLENBQUM7WUFDdkIsT0FBTyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2pDLENBQUMsQ0FBQzs7O0tBSUo7Ozs7OztBQzNLRDtRQU9FO1NBQWlCOztvQkFMbEJELGFBQVUsU0FBQzt3QkFDVixVQUFVLEVBQUUsTUFBTTtxQkFDbkI7Ozs7OzZCQUpEOzs7Ozs7O0FDQUE7UUFhRTtTQUFpQjs7OztRQUVqQixtQ0FBUTs7O1lBQVI7YUFDQzs7b0JBZEZFLFlBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsWUFBWTt3QkFDdEIsUUFBUSxFQUFFLHFEQUlUO3dCQUNFLE1BQU0sRUFBRSxDQUFDLG1CQUFtQixDQUFDO3FCQUNqQzs7OzsrQkFWRDs7Ozs7OztBQ0FBO1FBa0JFO1NBQWlCOzs7O1FBRWpCLHNDQUFROzs7WUFBUjthQUNDOztvQkFuQkZBLFlBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsZUFBZTt3QkFDekIsUUFBUSxFQUFFLGtGQVNYO3dCQUNDLE1BQU0sRUFBRSxDQUFDLHdCQUF3QixDQUFDO3FCQUNuQzs7OztrQ0FmRDs7Ozs7Ozs7UUNpQndDUixzQ0FBUTtRQUM5QzttQkFDRSxpQkFBTztTQUNSOzs7Ozs7UUFFRCxzQ0FBUzs7Ozs7WUFBVCxVQUFVLEdBQXFCLEVBQUUsSUFBaUI7Z0JBQWxELGlCQTZCTztnQkE1QkEscUJBQU0sVUFBVSxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUM7b0JBQzFCLE9BQU8sRUFBRSxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsZUFBZSxDQUFDO2lCQUN2RCxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQztnQkFDdkMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztxQkFDN0IsSUFBSSxDQUFDUyxhQUFHLENBQUMsVUFBQyxLQUFxQjtvQkFDOUIsSUFBSSxLQUFLLFlBQVlKLGlCQUFZLEVBQUU7d0JBQ2pDLHFCQUFNLENBQUMsSUFBRyxLQUEwQixDQUFBLENBQUM7d0JBQ3JDLEtBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQ3ZDLEtBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDN0MsS0FBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztxQkFDakU7aUJBQ0QsRUFBRSxVQUFDLEtBQXdCO29CQUMxQixLQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO29CQUN4RSxLQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUM5RCxLQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7aUJBQ3RELENBQUMsRUFDQUssb0JBQVUsQ0FBQyxVQUFDLEdBQUc7b0JBQ2YsS0FBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsR0FBRyxDQUFDLENBQUM7b0JBQzFDLE9BQU9SLGVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDeEIsQ0FBQyxDQUNILENBQUM7Ozs7O2FBT0Q7O29CQW5DUkksYUFBVTs7OztpQ0FoQlg7TUFpQndDLFFBQVE7Ozs7Ozs7UUNFUk4sc0NBQVE7UUFDOUMsNEJBQW9CLE9BQXdCO1lBQTVDLFlBQ0UsaUJBQU8sU0FDUjtZQUZtQixhQUFPLEdBQVAsT0FBTyxDQUFpQjs7U0FFM0M7Ozs7OztRQUVELHNDQUFTOzs7OztZQUFULFVBQVUsR0FBcUIsRUFBRSxJQUFpQjtnQkFBbEQsaUJBcUJDO2dCQXBCQSxxQkFBTSxJQUFJLEdBQWMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBRXpELElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLHVDQUF1QyxDQUFDLENBQUM7Z0JBRXZELE9BQU9HLE9BQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQ2pCUSxrQkFBUSxDQUFDO29CQUNQLElBQUksSUFBSSxFQUFFO3dCQUNSLE9BQU8sS0FBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQzNDO29CQUNELHFCQUFNLFVBQVUsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO3dCQUMzQixPQUFPLEVBQUUsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLGVBQWUsQ0FBQztxQkFDdkQsQ0FBQyxDQUFDO29CQUNILE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztpQkFDaEMsQ0FBQyxFQUNBQyxxQkFBVyxFQUFFLEVBQ2JDLGVBQUssQ0FBQyxHQUFHLENBQUMsRUFDVkMsdUJBQWEsRUFBRSxDQUNsQixDQUFDO2FBR0g7O29CQTNCRlIsYUFBVTs7Ozs7d0JBUEYsZUFBZTs7O2lDQVh4QjtNQW1Cd0MsUUFBUTs7Ozs7O0FDbkJoRDtRQXlCRTtZQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQTtTQUNwQzs7b0JBcEJGUyxXQUFRLFNBQUM7d0JBQ1IsT0FBTyxFQUFFLEVBQUU7d0JBQ1gsWUFBWSxFQUFFLEVBQUU7d0JBQ2hCLFNBQVMsRUFBRTs0QkFDVCxlQUFlOzRCQUNmO2dDQUNFLE9BQU8sRUFBRUMsc0JBQWlCO2dDQUMxQixRQUFRLEVBQUUsa0JBQWtCO2dDQUM1QixLQUFLLEVBQUUsSUFBSTs2QkFDWjs0QkFDRDtnQ0FDRSxPQUFPLEVBQUVBLHNCQUFpQjtnQ0FDMUIsUUFBUSxFQUFFLGtCQUFrQjtnQ0FDNUIsS0FBSyxFQUFFLElBQUk7NkJBQ1o7eUJBQ0Y7cUJBQ0Y7Ozs7NkJBdkJEOzs7Ozs7O0FDQUE7Ozs7b0JBS0NELFdBQVEsU0FBQzt3QkFDUixPQUFPLEVBQUU7NEJBQ1AsY0FBYzt5QkFDZjt3QkFDRCxZQUFZLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxtQkFBbUIsQ0FBQzt3QkFDckQsT0FBTyxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsbUJBQW1CLENBQUM7cUJBQ2pEOzs0QkFYRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7In0=