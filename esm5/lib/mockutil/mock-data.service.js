/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Inject, Injectable, InjectionToken } from '@angular/core';
import { of, throwError } from 'rxjs';
import { BaseUtil } from './base-util';
export var /** @type {?} */ TOKEN_MOCK = new InjectionToken('mockdata');
var MockDataService = /** @class */ (function (_super) {
    tslib_1.__extends(MockDataService, _super);
    function MockDataService(mockData) {
        var _this = _super.call(this) || this;
        _this.mockData = mockData;
        return _this;
    }
    /**
     * @param {?} req
     * @return {?}
     */
    MockDataService.prototype.getRequestMock = /**
     * @param {?} req
     * @return {?}
     */
    function (req) {
        var _this = this;
        return this.mockData.mocks.find(function (data) {
            // check if the method and url and params and match params
            if (req.method === data.type &&
                req.url.includes(data.url) &&
                _this.matchHeaders(data, req) &&
                _this.matchParams(data, req) &&
                _this.matchBody(data, req)) {
                if (data.name) {
                    _this.log.info('---mock data matched ' + data.name);
                }
                return true;
            }
            return false;
        });
    };
    /**
     * @param {?} req
     * @param {?} mockData
     * @return {?}
     */
    MockDataService.prototype.getRequestMockData = /**
     * @param {?} req
     * @param {?} mockData
     * @return {?}
     */
    function (req, mockData) {
        var _this = this;
        if (!mockData) {
            return null;
        }
        return mockData.mocks.find(function (data) {
            // check if the method and url and params and match params
            if (_this.matchMethod(data, req) &&
                _this.matchUrl(data, req) &&
                _this.matchHeaders(data, req) &&
                _this.matchParams(data, req) &&
                _this.matchBody(data, req)) {
                if (data.name) {
                    _this.log.info('---mock data matched ' + data.name);
                }
                return true;
            }
            return false;
        });
    };
    /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    MockDataService.prototype.matchMethod = /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    function (data, req) {
        if (data.type) {
            return req.method === data.type;
        }
        return true;
    };
    /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    MockDataService.prototype.matchUrl = /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    function (data, req) {
        if (data.url) {
            return req.url.includes(data.url);
        }
        return true;
    };
    /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    MockDataService.prototype.matchParams = /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    function (data, req) {
        if (data.params) {
            return data.params.every(function (param) { return req.params[param.key] === param.value || (req.params.get && req.params.get(param.key) === param.value); });
        }
        return true;
    };
    /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    MockDataService.prototype.matchHeaders = /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    function (data, req) {
        if (data.headersRequest) {
            return data.headersRequest.every(function (header) { return req.headers.get(header.key) === header.value; });
        }
        return true;
    };
    /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    MockDataService.prototype.matchBody = /**
     * @param {?} data
     * @param {?} req
     * @return {?}
     */
    function (data, req) {
        if (data.body) {
            var /** @type {?} */ res = objectEquals(data.body, req.body);
            this.log.info(" " + data.name + " -compare res=" + res + " body req " + JSON.stringify(req.body));
            // return JSON.stringify(data.body)==JSON.stringify(req.body);
            return res;
        }
        return true;
    };
    /**
     * @param {?} data
     * @return {?}
     */
    MockDataService.prototype.getResponseMock = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        var /** @type {?} */ resp = this.getResponseMockObj(data);
        if (resp instanceof HttpErrorResponse) {
            return throwError(resp);
        }
        else {
            return of(resp);
        }
    };
    /**
     * @param {?} data
     * @return {?}
     */
    MockDataService.prototype.getResponseMockObj = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        if (!data.resposeStatus) {
            data.resposeStatus = 200;
        }
        var /** @type {?} */ headers = new HttpHeaders();
        if (data.headersResponse) {
            data.headersResponse.forEach(function (header) { return (headers = headers.append(header.key, header.value)); });
        }
        if (data.resposeStatus !== 200) {
            return new HttpErrorResponse({
                status: data.resposeStatus,
                headers: headers,
                error: data.error,
                statusText: data.statusText
            });
        }
        return (new HttpResponse({
            status: data.resposeStatus,
            body: data.response ? data.response : null,
            headers: headers
        }));
    };
    MockDataService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    MockDataService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [TOKEN_MOCK,] }] }
    ]; };
    return MockDataService;
}(BaseUtil));
export { MockDataService };
function MockDataService_tsickle_Closure_declarations() {
    /** @type {?} */
    MockDataService.prototype.mockData;
}
/**
 * @param {?} x
 * @param {?} y
 * @return {?}
 */
function objectEquals(x, y) {
    // if both are function
    if (x instanceof Function) {
        if (y instanceof Function) {
            return x.toString() === y.toString();
        }
        return false;
    }
    if (x === null || x === undefined || y === null || y === undefined) {
        return x === y;
    }
    if (x === y || x.valueOf() === y.valueOf()) {
        return true;
    }
    // if one of them is date, they must had equal valueOf
    if (x instanceof Date) {
        return false;
    }
    if (y instanceof Date) {
        return false;
    }
    // if they are not function or strictly equal, they both need to be Objects
    if (!(x instanceof Object)) {
        return false;
    }
    if (!(y instanceof Object)) {
        return false;
    }
    var /** @type {?} */ p = Object.keys(x);
    return p.every(function (i) {
        return objectEquals(x[i], y[i]);
    });
    /* return Object.keys(y).every(function (i) { return p.indexOf(i) !== -1; }) ?
              p.every(function (i) { return objectEquals(x[i], y[i]); }) : false; */
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9jay1kYXRhLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teWNvbXBzLyIsInNvdXJjZXMiOlsibGliL21vY2t1dGlsL21vY2stZGF0YS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLGlCQUFpQixFQUFFLFdBQVcsRUFBZSxZQUFZLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRyxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxjQUFjLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkUsT0FBTyxFQUFFLEVBQUUsRUFBRSxVQUFVLEVBQWMsTUFBTSxNQUFNLENBQUM7QUFFbEQsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUd2QyxNQUFNLENBQUMscUJBQU0sVUFBVSxHQUFHLElBQUksY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDOztJQUdwQiwyQ0FBUTtJQUUzQyx5QkFBd0MsUUFBZTtRQUF2RCxZQUNFLGlCQUFPLFNBQ1I7UUFGdUMsY0FBUSxHQUFSLFFBQVEsQ0FBTzs7S0FFdEQ7Ozs7O0lBR0Qsd0NBQWM7Ozs7SUFBZCxVQUFlLEdBQXFCO1FBQXBDLGlCQWlCQztRQWhCQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBZTs7WUFFOUMsRUFBRSxDQUFDLENBQ0QsR0FBRyxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsSUFBSTtnQkFDeEIsR0FBRyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztnQkFDMUIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDO2dCQUM1QixLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUM7Z0JBQzNCLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FDMUIsQ0FBQyxDQUFDLENBQUM7Z0JBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ2QsS0FBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNwRDtnQkFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO2FBQ2I7WUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO1NBQ2QsQ0FBQyxDQUFDO0tBQ0o7Ozs7OztJQUVELDRDQUFrQjs7Ozs7SUFBbEIsVUFBbUIsR0FBcUIsRUFBRSxRQUFlO1FBQXpELGlCQXFCQztRQXBCQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDZCxNQUFNLENBQUMsSUFBSSxDQUFDO1NBQ2I7UUFFRCxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBQyxJQUFlOztZQUV6QyxFQUFFLENBQUMsQ0FDRCxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUM7Z0JBQzNCLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQztnQkFDeEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDO2dCQUM1QixLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUM7Z0JBQzNCLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FDMUIsQ0FBQyxDQUFDLENBQUM7Z0JBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ2QsS0FBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNwRDtnQkFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO2FBQ2I7WUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO1NBQ2QsQ0FBQyxDQUFDO0tBQ0o7Ozs7OztJQUVPLHFDQUFXOzs7OztjQUFDLElBQWUsRUFBRSxHQUFxQjtRQUN4RCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNkLE1BQU0sQ0FBQyxHQUFHLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDakM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDOzs7Ozs7O0lBR04sa0NBQVE7Ozs7O2NBQUMsSUFBZSxFQUFFLEdBQXFCO1FBQ3JELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ2IsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNuQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7Ozs7Ozs7SUFHTixxQ0FBVzs7Ozs7Y0FBQyxJQUFlLEVBQUUsR0FBcUI7UUFDeEQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDaEIsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUN0QixVQUFDLEtBQWEsSUFBSyxPQUFBLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEtBQUssQ0FBQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUF0RyxDQUFzRyxDQUMxSCxDQUFDO1NBQ0g7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDOzs7Ozs7O0lBRU4sc0NBQVk7Ozs7O2NBQUMsSUFBZSxFQUFFLEdBQXFCO1FBQ3pELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxVQUFDLE1BQWUsSUFBSyxPQUFBLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxNQUFNLENBQUMsS0FBSyxFQUE1QyxDQUE0QyxDQUFDLENBQUM7U0FDckc7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDOzs7Ozs7O0lBR04sbUNBQVM7Ozs7O2NBQUMsSUFBZSxFQUFFLEdBQXFCO1FBQ3RELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2QscUJBQU0sR0FBRyxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5QyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFJLElBQUksQ0FBQyxJQUFJLHNCQUFpQixHQUFHLGtCQUFhLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBRyxDQUFDLENBQUM7O1lBRXhGLE1BQU0sQ0FBQyxHQUFHLENBQUM7U0FDWjtRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7Ozs7OztJQUdkLHlDQUFlOzs7O0lBQWYsVUFBZ0IsSUFBZTtRQUM3QixxQkFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNDLEVBQUUsQ0FBQyxDQUFDLElBQUksWUFBWSxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7WUFDdEMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN6QjtRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sTUFBTSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNqQjtLQUNGOzs7OztJQUVELDRDQUFrQjs7OztJQUFsQixVQUFtQixJQUFlO1FBQ2hDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDeEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxHQUFHLENBQUM7U0FDMUI7UUFDRCxxQkFBSSxPQUFPLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQztRQUNoQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUN6QixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBcEQsQ0FBb0QsQ0FBQyxDQUFDO1NBQzlGO1FBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQy9CLE1BQU0sQ0FBQyxJQUFJLGlCQUFpQixDQUFDO2dCQUMzQixNQUFNLEVBQUUsSUFBSSxDQUFDLGFBQWE7Z0JBQzFCLE9BQU8sRUFBRSxPQUFPO2dCQUNoQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7Z0JBQ2pCLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVTthQUM1QixDQUFDLENBQUM7U0FDSjtRQUNELE1BQU0sQ0FBQyxDQUNMLElBQUksWUFBWSxDQUFDO1lBQ2YsTUFBTSxFQUFFLElBQUksQ0FBQyxhQUFhO1lBQzFCLElBQUksRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJO1lBQzFDLE9BQU8sRUFBRSxPQUFPO1NBQ2pCLENBQUMsQ0FDSCxDQUFDO0tBQ0g7O2dCQXpIRixVQUFVOzs7O2dEQUdJLE1BQU0sU0FBQyxVQUFVOzswQkFaaEM7RUFVcUMsUUFBUTtTQUFoQyxlQUFlOzs7Ozs7Ozs7O0FBMkg1QixzQkFBc0IsQ0FBQyxFQUFFLENBQUM7O0lBRXhCLEVBQUUsQ0FBQyxDQUFDLENBQUMsWUFBWSxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBQzFCLEVBQUUsQ0FBQyxDQUFDLENBQUMsWUFBWSxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQzFCLE1BQU0sQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQ3RDO1FBQ0QsTUFBTSxDQUFDLEtBQUssQ0FBQztLQUNkO0lBQ0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssU0FBUyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDbkUsTUFBTSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7S0FDaEI7SUFDRCxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzNDLE1BQU0sQ0FBQyxJQUFJLENBQUM7S0FDYjs7SUFHRCxFQUFFLENBQUMsQ0FBQyxDQUFDLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQztRQUN0QixNQUFNLENBQUMsS0FBSyxDQUFDO0tBQ2Q7SUFDRCxFQUFFLENBQUMsQ0FBQyxDQUFDLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQztRQUN0QixNQUFNLENBQUMsS0FBSyxDQUFDO0tBQ2Q7O0lBR0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDM0IsTUFBTSxDQUFDLEtBQUssQ0FBQztLQUNkO0lBQ0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDM0IsTUFBTSxDQUFDLEtBQUssQ0FBQztLQUNkO0lBRUQscUJBQU0sQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDekIsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBUyxDQUFDO1FBQ3ZCLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQ2pDLENBQUMsQ0FBQzs7O0NBSUoiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIdHRwRXJyb3JSZXNwb25zZSwgSHR0cEhlYWRlcnMsIEh0dHBSZXF1ZXN0LCBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSwgSW5qZWN0aW9uVG9rZW4gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgb2YsIHRocm93RXJyb3IsIE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IEJhc2VVdGlsIH0gZnJvbSAnLi9iYXNlLXV0aWwnO1xyXG5pbXBvcnQgeyBJSGVhZGVyLCBJTW9jaywgSU1vY2tEYXRhLCBJUGFyYW0gfSBmcm9tICcuL2ltb2NrLWRhdGEubW9kZWwnO1xyXG5cclxuZXhwb3J0IGNvbnN0IFRPS0VOX01PQ0sgPSBuZXcgSW5qZWN0aW9uVG9rZW4oJ21vY2tkYXRhJyk7XHJcbi8vIGltcG9ydCAncnhqcy9hZGQvb2JzZXJ2YWJsZS90aHJvdyc7XHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE1vY2tEYXRhU2VydmljZSBleHRlbmRzIEJhc2VVdGlsIHtcclxuXHJcbiAgY29uc3RydWN0b3IoQEluamVjdChUT0tFTl9NT0NLKSBwcml2YXRlIG1vY2tEYXRhOiBJTW9jaykge1xyXG4gICAgc3VwZXIoKTtcclxuICB9XHJcblxyXG5cclxuICBnZXRSZXF1ZXN0TW9jayhyZXE6IEh0dHBSZXF1ZXN0PGFueT4pOiBJTW9ja0RhdGEge1xyXG4gICAgcmV0dXJuIHRoaXMubW9ja0RhdGEubW9ja3MuZmluZCgoZGF0YTogSU1vY2tEYXRhKSA9PiB7XHJcbiAgICAgIC8vIGNoZWNrIGlmIHRoZSBtZXRob2QgYW5kIHVybCBhbmQgcGFyYW1zIGFuZCBtYXRjaCBwYXJhbXNcclxuICAgICAgaWYgKFxyXG4gICAgICAgIHJlcS5tZXRob2QgPT09IGRhdGEudHlwZSAmJlxyXG4gICAgICAgIHJlcS51cmwuaW5jbHVkZXMoZGF0YS51cmwpICYmXHJcbiAgICAgICAgdGhpcy5tYXRjaEhlYWRlcnMoZGF0YSwgcmVxKSAmJlxyXG4gICAgICAgIHRoaXMubWF0Y2hQYXJhbXMoZGF0YSwgcmVxKSAmJlxyXG4gICAgICAgIHRoaXMubWF0Y2hCb2R5KGRhdGEsIHJlcSlcclxuICAgICAgKSB7XHJcbiAgICAgICAgaWYgKGRhdGEubmFtZSkge1xyXG4gICAgICAgICAgdGhpcy5sb2cuaW5mbygnLS0tbW9jayBkYXRhIG1hdGNoZWQgJyArIGRhdGEubmFtZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0UmVxdWVzdE1vY2tEYXRhKHJlcTogSHR0cFJlcXVlc3Q8YW55PiwgbW9ja0RhdGE6IElNb2NrKTogSU1vY2tEYXRhIHtcclxuICAgIGlmICghbW9ja0RhdGEpIHtcclxuICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIG1vY2tEYXRhLm1vY2tzLmZpbmQoKGRhdGE6IElNb2NrRGF0YSkgPT4ge1xyXG4gICAgICAvLyBjaGVjayBpZiB0aGUgbWV0aG9kIGFuZCB1cmwgYW5kIHBhcmFtcyBhbmQgbWF0Y2ggcGFyYW1zXHJcbiAgICAgIGlmIChcclxuICAgICAgICB0aGlzLm1hdGNoTWV0aG9kKGRhdGEsIHJlcSkgJiZcclxuICAgICAgICB0aGlzLm1hdGNoVXJsKGRhdGEsIHJlcSkgJiZcclxuICAgICAgICB0aGlzLm1hdGNoSGVhZGVycyhkYXRhLCByZXEpICYmXHJcbiAgICAgICAgdGhpcy5tYXRjaFBhcmFtcyhkYXRhLCByZXEpICYmXHJcbiAgICAgICAgdGhpcy5tYXRjaEJvZHkoZGF0YSwgcmVxKVxyXG4gICAgICApIHtcclxuICAgICAgICBpZiAoZGF0YS5uYW1lKSB7XHJcbiAgICAgICAgICB0aGlzLmxvZy5pbmZvKCctLS1tb2NrIGRhdGEgbWF0Y2hlZCAnICsgZGF0YS5uYW1lKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIG1hdGNoTWV0aG9kKGRhdGE6IElNb2NrRGF0YSwgcmVxOiBIdHRwUmVxdWVzdDxhbnk+KTogYm9vbGVhbiB7XHJcbiAgICBpZiAoZGF0YS50eXBlKSB7XHJcbiAgICAgIHJldHVybiByZXEubWV0aG9kID09PSBkYXRhLnR5cGU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgbWF0Y2hVcmwoZGF0YTogSU1vY2tEYXRhLCByZXE6IEh0dHBSZXF1ZXN0PGFueT4pOiBib29sZWFuIHtcclxuICAgIGlmIChkYXRhLnVybCkge1xyXG4gICAgICByZXR1cm4gcmVxLnVybC5pbmNsdWRlcyhkYXRhLnVybCk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgbWF0Y2hQYXJhbXMoZGF0YTogSU1vY2tEYXRhLCByZXE6IEh0dHBSZXF1ZXN0PGFueT4pOiBib29sZWFuIHtcclxuICAgIGlmIChkYXRhLnBhcmFtcykge1xyXG4gICAgICByZXR1cm4gZGF0YS5wYXJhbXMuZXZlcnkoXHJcbiAgICAgICAgKHBhcmFtOiBJUGFyYW0pID0+IHJlcS5wYXJhbXNbcGFyYW0ua2V5XSA9PT0gcGFyYW0udmFsdWUgfHwgKHJlcS5wYXJhbXMuZ2V0ICYmIHJlcS5wYXJhbXMuZ2V0KHBhcmFtLmtleSkgPT09IHBhcmFtLnZhbHVlKVxyXG4gICAgICApO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG4gIHByaXZhdGUgbWF0Y2hIZWFkZXJzKGRhdGE6IElNb2NrRGF0YSwgcmVxOiBIdHRwUmVxdWVzdDxhbnk+KTogYm9vbGVhbiB7XHJcbiAgICBpZiAoZGF0YS5oZWFkZXJzUmVxdWVzdCkge1xyXG4gICAgICByZXR1cm4gZGF0YS5oZWFkZXJzUmVxdWVzdC5ldmVyeSgoaGVhZGVyOiBJSGVhZGVyKSA9PiByZXEuaGVhZGVycy5nZXQoaGVhZGVyLmtleSkgPT09IGhlYWRlci52YWx1ZSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgbWF0Y2hCb2R5KGRhdGE6IElNb2NrRGF0YSwgcmVxOiBIdHRwUmVxdWVzdDxhbnk+KTogYm9vbGVhbiB7XHJcbiAgICBpZiAoZGF0YS5ib2R5KSB7XHJcbiAgICAgIGNvbnN0IHJlcyA9IG9iamVjdEVxdWFscyhkYXRhLmJvZHksIHJlcS5ib2R5KTtcclxuICAgICAgdGhpcy5sb2cuaW5mbyhgICR7ZGF0YS5uYW1lfSAtY29tcGFyZSByZXM9JHtyZXN9IGJvZHkgcmVxICR7SlNPTi5zdHJpbmdpZnkocmVxLmJvZHkpfWApO1xyXG4gICAgICAvLyByZXR1cm4gSlNPTi5zdHJpbmdpZnkoZGF0YS5ib2R5KT09SlNPTi5zdHJpbmdpZnkocmVxLmJvZHkpO1xyXG4gICAgICByZXR1cm4gcmVzO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBnZXRSZXNwb25zZU1vY2soZGF0YTogSU1vY2tEYXRhKTpPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgY29uc3QgcmVzcCA9IHRoaXMuZ2V0UmVzcG9uc2VNb2NrT2JqKGRhdGEpO1xyXG4gICAgaWYgKHJlc3AgaW5zdGFuY2VvZiBIdHRwRXJyb3JSZXNwb25zZSkge1xyXG4gICAgICByZXR1cm4gdGhyb3dFcnJvcihyZXNwKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBvZihyZXNwKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldFJlc3BvbnNlTW9ja09iaihkYXRhOiBJTW9ja0RhdGEpOiBIdHRwRXJyb3JSZXNwb25zZSB8IEh0dHBSZXNwb25zZTxvYmplY3Q+IHtcclxuICAgIGlmICghZGF0YS5yZXNwb3NlU3RhdHVzKSB7XHJcbiAgICAgIGRhdGEucmVzcG9zZVN0YXR1cyA9IDIwMDtcclxuICAgIH1cclxuICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKCk7XHJcbiAgICBpZiAoZGF0YS5oZWFkZXJzUmVzcG9uc2UpIHtcclxuICAgICAgZGF0YS5oZWFkZXJzUmVzcG9uc2UuZm9yRWFjaChoZWFkZXIgPT4gKGhlYWRlcnMgPSBoZWFkZXJzLmFwcGVuZChoZWFkZXIua2V5LCBoZWFkZXIudmFsdWUpKSk7XHJcbiAgICB9XHJcbiAgICBpZiAoZGF0YS5yZXNwb3NlU3RhdHVzICE9PSAyMDApIHtcclxuICAgICAgcmV0dXJuIG5ldyBIdHRwRXJyb3JSZXNwb25zZSh7XHJcbiAgICAgICAgc3RhdHVzOiBkYXRhLnJlc3Bvc2VTdGF0dXMsXHJcbiAgICAgICAgaGVhZGVyczogaGVhZGVycyxcclxuICAgICAgICBlcnJvcjogZGF0YS5lcnJvcixcclxuICAgICAgICBzdGF0dXNUZXh0OiBkYXRhLnN0YXR1c1RleHRcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICBuZXcgSHR0cFJlc3BvbnNlKHtcclxuICAgICAgICBzdGF0dXM6IGRhdGEucmVzcG9zZVN0YXR1cyxcclxuICAgICAgICBib2R5OiBkYXRhLnJlc3BvbnNlID8gZGF0YS5yZXNwb25zZSA6IG51bGwsXHJcbiAgICAgICAgaGVhZGVyczogaGVhZGVyc1xyXG4gICAgICB9KVxyXG4gICAgKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIG9iamVjdEVxdWFscyh4LCB5KSB7XHJcbiAgLy8gaWYgYm90aCBhcmUgZnVuY3Rpb25cclxuICBpZiAoeCBpbnN0YW5jZW9mIEZ1bmN0aW9uKSB7XHJcbiAgICBpZiAoeSBpbnN0YW5jZW9mIEZ1bmN0aW9uKSB7XHJcbiAgICAgIHJldHVybiB4LnRvU3RyaW5nKCkgPT09IHkudG9TdHJpbmcoKTtcclxuICAgIH1cclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcbiAgaWYgKHggPT09IG51bGwgfHwgeCA9PT0gdW5kZWZpbmVkIHx8IHkgPT09IG51bGwgfHwgeSA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICByZXR1cm4geCA9PT0geTtcclxuICB9XHJcbiAgaWYgKHggPT09IHkgfHwgeC52YWx1ZU9mKCkgPT09IHkudmFsdWVPZigpKSB7XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIC8vIGlmIG9uZSBvZiB0aGVtIGlzIGRhdGUsIHRoZXkgbXVzdCBoYWQgZXF1YWwgdmFsdWVPZlxyXG4gIGlmICh4IGluc3RhbmNlb2YgRGF0ZSkge1xyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuICBpZiAoeSBpbnN0YW5jZW9mIERhdGUpIHtcclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcblxyXG4gIC8vIGlmIHRoZXkgYXJlIG5vdCBmdW5jdGlvbiBvciBzdHJpY3RseSBlcXVhbCwgdGhleSBib3RoIG5lZWQgdG8gYmUgT2JqZWN0c1xyXG4gIGlmICghKHggaW5zdGFuY2VvZiBPYmplY3QpKSB7XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG4gIGlmICghKHkgaW5zdGFuY2VvZiBPYmplY3QpKSB7XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBjb25zdCBwID0gT2JqZWN0LmtleXMoeCk7XHJcbiAgcmV0dXJuIHAuZXZlcnkoZnVuY3Rpb24oaSkge1xyXG4gICAgcmV0dXJuIG9iamVjdEVxdWFscyh4W2ldLCB5W2ldKTtcclxuICB9KTtcclxuXHJcbiAgLyogcmV0dXJuIE9iamVjdC5rZXlzKHkpLmV2ZXJ5KGZ1bmN0aW9uIChpKSB7IHJldHVybiBwLmluZGV4T2YoaSkgIT09IC0xOyB9KSA/XHJcbiAgICAgICAgICBwLmV2ZXJ5KGZ1bmN0aW9uIChpKSB7IHJldHVybiBvYmplY3RFcXVhbHMoeFtpXSwgeVtpXSk7IH0pIDogZmFsc2U7ICovXHJcbn1cclxuIl19