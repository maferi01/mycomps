/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
var /**
 * @abstract
 */
BaseUtil = /** @class */ (function () {
    function BaseUtil() {
        this.log = {
            info: /**
             * @param {?} msg
             * @return {?}
             */
            function (msg) {
                console.log(msg);
            }
        };
        this.log.info('Class ' + this.constructor.name + ' created');
    }
    return BaseUtil;
}());
/**
 * @abstract
 */
export { BaseUtil };
function BaseUtil_tsickle_Closure_declarations() {
    /** @type {?} */
    BaseUtil.prototype.log;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS11dGlsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXljb21wcy8iLCJzb3VyY2VzIjpbImxpYi9tb2NrdXRpbC9iYXNlLXV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUNBOzs7QUFBQTtJQUVFO1FBQ0UsSUFBSSxDQUFDLEdBQUcsR0FBRztZQUNULElBQUk7Ozs7c0JBQUMsR0FBRztnQkFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ2xCO1NBQ0YsQ0FBQztRQUNGLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxVQUFVLENBQUMsQ0FBQztLQUM5RDttQkFWSDtJQVdDLENBQUE7Ozs7QUFWRCxvQkFVQyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQmFzZVV0aWwge1xyXG4gIHByb3RlY3RlZCBsb2c7XHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICB0aGlzLmxvZyA9IHtcclxuICAgICAgaW5mbyhtc2cpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhtc2cpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG4gICAgdGhpcy5sb2cuaW5mbygnQ2xhc3MgJyArIHRoaXMuY29uc3RydWN0b3IubmFtZSArICcgY3JlYXRlZCcpO1xyXG4gIH1cclxufVxyXG4iXX0=